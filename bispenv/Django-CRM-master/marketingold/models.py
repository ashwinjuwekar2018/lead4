
from datetime import datetime, timedelta
from django.db import models
from django.utils.timesince import timesince
from django.utils.translation import ugettext_lazy as _
from django.core.validators import RegexValidator
from common.models import User


class Pdf(models.Model):
    title = models.CharField(max_length=255, blank=True)
    file = models.FileField(upload_to='contacts/pdfs/')
    uploaded_at = models.DateTimeField(auto_now_add=True)


class Contactlinkedin(models.Model):

    name = models.CharField(max_length=500)
    email = models.EmailField()
    contact_number = models.CharField(
      max_length=20, blank=True, null=True)

    company_name = models.CharField(max_length=500, null=True, blank=True)
    last_name = models.CharField(max_length=500, null=True, blank=True)
    city = models.CharField(max_length=500, null=True, blank=True)
    state = models.CharField(max_length=500, null=True, blank=True)
    country = models.CharField(max_length=500, null=True, blank=True)
    skills = models.CharField(max_length=500, null=True, blank=True)
    language=models.CharField(max_length=500, null=True, blank=True)
    certification =models.CharField(max_length=500, null=True, blank=True)


    position = models.CharField(max_length=500, null=True, blank=True)


    def __str__(self):
        return self.email

    class Meta:
        ordering = ['id', ]

class Tag(models.Model):
    name = models.CharField(max_length=500)
    color = models.CharField(max_length=20,
                             default="#999999", verbose_name=_("color"))
    created_by = models.ForeignKey(User,
                                   related_name="marketing_tags",
                                   null=True, on_delete=models.SET_NULL)
    created_on = models.DateTimeField(auto_now_add=True)

    @property
    def created_by_user(self):
        return self.created_by if self.created_by else None

class ContactList(models.Model):
    created_by = models.ForeignKey(
        User, related_name="marketing_contactlist",
        null=True, on_delete=models.SET_NULL)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=500)
    tags = models.ManyToManyField(Tag)
    visible_to = models.ManyToManyField(User, related_name="contact_lists_visible_to")

    class Meta:
        ordering = ('id',)

    @property
    def created_by_user(self):
        return self.created_by if self.created_by else None

    @property
    def created_on_format(self):
        return self.created_on.strftime('%b %d, %Y %I:%M %p')

    @property
    def created_on_since(self):
        now = datetime.now()
        difference = now.replace(tzinfo=None) - \
            self.created_on.replace(tzinfo=None)

        if difference <= timedelta(minutes=1):
            return 'just now'
        return '%(time)s ago' % {
            'time': timesince(self.created_on).split(', ')[0]}

    @property
    def tags_data(self):
        return self.tags.all()

    @property
    def no_of_contacts(self):
        return self.contacts.all().count()

    @property
    def no_of_campaigns(self):
        return self.campaigns.all().count()

    @property
    def unsubscribe_contacts(self):
        return self.contacts.filter(is_unsubscribed=True).count()

    @property
    def bounced_contacts(self):
        return self.contacts.filter(is_bounced=True).count()

class Contact(models.Model):
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Phone number must be entered in the format: '+999999999'. \
        Up to 20 digits allowed."
    )
    created_by = models.ForeignKey(
        User, related_name="marketing_contacts_created_by",
        null=True, on_delete=models.SET_NULL)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    date = models.DateTimeField(auto_now=True)
    contact_list = models.ManyToManyField(ContactList, related_name="contacts")
    name = models.CharField(max_length=500)
    email = models.EmailField()
    contact_number = models.CharField(
      max_length=20, blank=True, null=True)
    is_unsubscribed = models.BooleanField(default=False)
    is_bounced = models.BooleanField(default=False)
    company_name = models.CharField(max_length=500, null=True, blank=True)
    last_name = models.CharField(max_length=500, null=True, blank=True)
    city = models.CharField(max_length=500, null=True, blank=True)
    state = models.CharField(max_length=500, null=True, blank=True)
    country = models.CharField(max_length=500, null=True, blank=True)
    Course = models.CharField(max_length=500, null=True, blank=True)
    Lead_Source = models.CharField(max_length=500, null=True, blank=True)
    position = models.CharField(max_length=500, null=True, blank=True)
    pdf_file = models.FileField(upload_to='contacts/pdfs/',null=True,blank=True)

    def __str__(self):
        return self.email

    class Meta:
        ordering = ['id', ]

class Message(models.Model):
     sender = models.ForeignKey(User, related_name="sender",on_delete=models.CASCADE)
     reciever = models.ForeignKey(User, related_name="reciever",on_delete=models.CASCADE)
     linkedin_contact = models.ForeignKey(Contact, related_name="linkedin",on_delete=models.CASCADE,default=8)
     msg_content =models.TextField()
     created_at = models.DateTimeField(auto_now_add=True)
     schedule_date_time = models.DateTimeField(blank=True, null=True)


class linkMessage(models.Model):
    sender = models.ForeignKey(User, related_name="linksender", on_delete=models.CASCADE)
    reciever = models.ForeignKey(User, related_name="linkreciever", on_delete=models.CASCADE)
    linkedin_contact = models.ForeignKey(Contactlinkedin, related_name="contactlinkedin", on_delete=models.CASCADE)
    msg_content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    schedule_date_time = models.DateTimeField(blank=True, null=True)

from django.urls import reverse

class Event(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField()
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    user = models.ForeignKey(User, related_name="user", on_delete=models.CASCADE,default=1)

    @property
    def get_html_url(self):
        url = reverse('marketing:event_details', args=(self.id,))
        return f'<a href="{url}"> {self.title} </a>'

class FailedContact(models.Model):
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Phone number must be entered in the format: '+999999999'.\
        Up to 20 digits allowed."
    )
    created_by = models.ForeignKey(
        User, related_name="marketing_failed_contacts_created_by", null=True,
        on_delete=models.SET_NULL)
    created_on = models.DateTimeField(auto_now_add=True)
    contact_list = models.ManyToManyField(
        ContactList, related_name="failed_contacts")
    name = models.CharField(max_length=500, null=True, blank=True)
    email = models.EmailField(null=True, blank=True)
    contact_number = models.CharField(
        validators=[phone_regex], max_length=20, blank=True, null=True)
    company_name = models.CharField(max_length=500, null=True, blank=True)
    last_name = models.CharField(max_length=500, null=True, blank=True)
    city = models.CharField(max_length=500, null=True, blank=True)
    state = models.CharField(max_length=500, null=True, blank=True)
    contry = models.CharField(max_length=500, null=True, blank=True)

    def __str__(self):
        return self.email







