from django.urls import path, include
from django.conf.urls import url
from .views import (
    dashboard, contact_lists, contacts_list, contact_list_new, contacts_list_new, contact_list_detail, edit_contact,

    edit_contact_list, delete_contact_list, failed_contact_list_detail, failed_contact_list_download_delete,
  demo_file_download, delete_contact,  contact_detail,
    edit_failed_contact, delete_failed_contact,func_chat_detail,fun_calendar,fun_add_event,fun_remove,fun_update,BasicUploadView,clear_database,
link_contacts_list,link_contact_detail, func_link_chat_detail, fun_export_users_xls,fun_read_pdf,event_list_detail
)

app_name = 'marketing'

urlpatterns = [
    path('', dashboard, name='dashboard'),

    path('contact-list/', contact_lists, name='contact_lists'),
    path('contacts/<int:pk>/edit/', edit_contact, name='edit_contact'),
    path('contacts/<int:pk>/delete/', delete_contact, name='delete_contact'),

    path('contacts/', contacts_list, name='contacts_list'),
    path('contact-list/create/', contact_list_new, name='contact_list_new'),
    path('cl/list/cnew/', contacts_list_new, name='contacts_list_new'),
    path('contact-list/<int:pk>/detail/', contact_list_detail, name='contact_list_detail'),
    path('contact-list/<int:pk>/failed/', failed_contact_list_detail, name='failed_contact_list_detail'),
    path('contact-list/<int:pk>/failed/edit/', edit_failed_contact, name='edit_failed_contact'),
    path('contact-list/<int:pk>/failed/delete/', delete_failed_contact, name='delete_failed_contact'),
    path('contact-list/<int:pk>/failed/download/',
         failed_contact_list_download_delete, name='failed_contact_list_download_delete'),
    path('contact-list/<int:pk>/edit/', edit_contact_list, name='edit_contact_list'),
    path('contact-list/<int:pk>/delete/', delete_contact_list, name='delete_contact_list'),


    path('demo-file-download-for-contacts-list/', demo_file_download, name='demo_file_download'),

    path('contacts/<int:contact_id>/view/', contact_detail, name="contact_detail"),
    path('contacts/<str:user_id>/chatdetail/', func_chat_detail, name="func_chat_detail"),
    path('contacts/calendar/', fun_calendar, name='calendar'),
    path('contacts/add_event/', fun_add_event, name='fun_add_event'),
    path('contacts/update/', fun_update, name='fun_update'),
    path('contacts/remove/', fun_remove, name='fun_remove'),


    url(r'^basic-upload/$', BasicUploadView.as_view(), name='basic_upload'),
    url(r'^clear/$', clear_database, name='clear_database'),
    url(r'^pdf/$', fun_read_pdf, name='fun_read_pdf'),
    path('linkcontacts/', link_contacts_list, name='linkcontacts_list'),
    path('linkcontacts/<int:contact_id>/view/', link_contact_detail, name="link_contact_detail"),
    path('linkcontacts/<str:user_id>/chatdetail/',  func_link_chat_detail, name="func_link_chat_detail"),
    url(r'^export/xls/$', fun_export_users_xls, name='export_users_xls'),


    path('event-list/<int:pk>/detail/', event_list_detail, name='event_list_detail'),

]
