from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import csvdata,category,technology
from django_pandas.io import read_frame

import numpy as np
import matplotlib.pyplot as plt
import base64
import matplotlib
matplotlib.use('Agg')

import csv, io


@csrf_exempt
def predictive_analysis(request):
    Category = category.objects.all()


    return render(request, "myapp/linear.html",{'Category':Category})

@csrf_exempt
def regression(request,name):
    category_name = category.objects.get(Category=name)
    Category = category.objects.all()
    categoryname = str(category_name )
    category_all = csvdata.objects.all()

    df_category = read_frame(category_all)
    country = request.session['country']

    if  categoryname =='Java':


      #J2ME Trends

        data_frame1 = df_category.loc[(df_category['Technology'] == 'J2ME')  & (df_category['Country'] == country)]


        dfx1 = data_frame1['Exp']
        dfx1.to_numpy()

        dfy1 = data_frame1['Sal']
        dfy1.to_numpy()
        p11 = np.polyfit(dfx1, dfy1, 1)
        # p21 = np.polyfit(dfx1, dfy1, 2)
        p31 = np.polyfit(dfx1, dfy1, 3)

        fig1, ax1 = plt.subplots()
        data_frame1.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax1)
        if country == 'India':

            xp1 = np.linspace(20, 1, 100)
        else:
            xp1 = np.linspace(30, 1, 100)

        plt.plot(xp1, np.polyval(p11, xp1), 'r',label="Linear Pred")
        # plt.plot(xp1, np.polyval(p21, xp1), 'b--')
        plt.plot(xp1, np.polyval(p31, xp1), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.ylabel('Salary (In USD)')
            plt.xlim(2, 20)
            plt.ylim(0, 60000)

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('J2ME Prediction Based on Experience And Salary', fontsize=10)
        data_frame1 = plt.gca()
        data_frame1.spines['right'].set_color('none')
        data_frame1.spines['top'].set_color('none')

        buffer1 = io.BytesIO()

        plt.savefig(buffer1)

        buffer1.seek(0)

        image_png1 = buffer1.getvalue()
        buffer1.close()
        graphic = base64.b64encode(image_png1)
        graphic = graphic.decode('utf-8')

 #J2EE Trends

        data_frame2 = df_category.loc[(df_category['Technology'] == 'J2EE') & (df_category['Country'] == country)]

        dfx2 = data_frame2['Exp']
        dfx2.to_numpy()

        dfy2 = data_frame2['Sal']
        dfy2.to_numpy()
        p12 = np.polyfit(dfx2, dfy2, 1)
        # p22 = np.polyfit(dfx2, dfy2, 2)
        p32 = np.polyfit(dfx2, dfy2, 3)

        fig2, ax2 = plt.subplots()
        data_frame2.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax2)
        if country == 'India':

            xp2 = np.linspace(20, 1, 100)
        else:
            xp2 = np.linspace(30, 1, 100)
        plt.plot(xp2, np.polyval(p12, xp2), 'r', label="Linear Pred")
        # plt.plot(xp2, np.polyval(p22, xp2), 'b--')
        plt.plot(xp2, np.polyval(p32, xp2), 'm:' ,label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('J2EE Prediction Based on Experience And Salary', fontsize=10)
        data_frame2 = plt.gca()
        data_frame2.spines['right'].set_color('none')
        data_frame2.spines['top'].set_color('none')

        buffer2 = io.BytesIO()

        plt.savefig(buffer2)


        buffer2.seek(0)

        image_png2 = buffer2.getvalue()
        buffer2.close()
        graphic1 = base64.b64encode(image_png2)
        graphic1 = graphic1.decode('utf-8')

        return render(request, "myapp/regression.html",
                      {'graphic1': graphic1, 'graphic': graphic, 'category_name':category_name ,'Category':Category})

#Hadoop Analysis

    elif  categoryname =='Hadoop':

        data_frame = df_category.loc[(df_category['Technology'] == 'HDFS') & (df_category['Country'] == country)]

        dfx = data_frame['Exp']
        dfx.to_numpy()

        dfy = data_frame['Sal']
        dfy.to_numpy()
        p1 = np.polyfit(dfx, dfy, 1)
        # p2 = np.polyfit(dfx, dfy, 2)
        p3 = np.polyfit(dfx, dfy, 3)

        fig, ax = plt.subplots()
        data_frame.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax)
        if country == 'India':

            xp = np.linspace(20, 1, 100)
        else:
            xp = np.linspace(30, 1, 100)
        plt.plot(xp, np.polyval(p1, xp), 'r', label="Linear Pred")
        # plt.plot(xp, np.polyval(p2, xp), 'b--')
        plt.plot(xp, np.polyval(p3, xp), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('HDFS Prediction Based on Experience And Salary', fontsize=10)
        pivot_data = plt.gca()
        pivot_data.spines['right'].set_color('none')
        pivot_data.spines['top'].set_color('none')

        buffer = io.BytesIO()

        plt.savefig(buffer)


        buffer.seek(0)

        image_png = buffer.getvalue()
        buffer.close()
        graphic = base64.b64encode(image_png)
        graphic = graphic.decode('utf-8')

#Yarn prediction

        data_frame1 = df_category.loc[(df_category['Technology'] == 'YARN') & (df_category['Country'] == country)]

        dfx1 = data_frame1['Exp']
        dfx1.to_numpy()

        dfy1 = data_frame1['Sal']
        dfy1.to_numpy()
        p11 = np.polyfit(dfx1, dfy1, 1)
        # p21 = np.polyfit(dfx1, dfy1, 2)
        p31 = np.polyfit(dfx1, dfy1, 3)

        fig1, ax1 = plt.subplots()
        data_frame1.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax1)
        if country == 'India':

            xp1 = np.linspace(20, 1, 100)
        else:
            xp1 = np.linspace(30, 1, 100)
        plt.plot(xp1, np.polyval(p11, xp1), 'r', label="Linear Pred")
        # plt.plot(xp1, np.polyval(p21, xp1), 'b--')
        plt.plot(xp1, np.polyval(p31, xp1), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('YARN Prediction Based on Experience And Salary', fontsize=10)
        pivot_data1 = plt.gca()
        pivot_data1.spines['right'].set_color('none')
        pivot_data1.spines['top'].set_color('none')

        buffer1 = io.BytesIO()

        plt.savefig(buffer1)

        buffer1.seek(0)

        image_png1 = buffer1.getvalue()
        buffer1.close()
        graphic1 = base64.b64encode(image_png1)
        graphic1 = graphic1.decode('utf-8')

#Spark Analysis

        data_frame2 = df_category.loc[(df_category['Technology'] == ' Spark') & (df_category['Country'] == country)]

        dfx11 = data_frame2['Exp']
        dfx11.to_numpy()

        dfy11 = data_frame2['Sal']
        dfy11.to_numpy()
        p111 = np.polyfit(dfx11, dfy11, 1)
        # p211 = np.polyfit(dfx11, dfy11, 2)
        p311 = np.polyfit(dfx11, dfy11, 3)

        fig11, ax11 = plt.subplots()
        data_frame2.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax11)
        if country == 'India':

            xp11 = np.linspace(20, 1, 100)
        else:
            xp11 = np.linspace(30, 1, 100)
        plt.plot(xp11, np.polyval(p111, xp11), 'r', label="Linear Pred")
        # plt.plot(xp11, np.polyval(p211, xp11), 'b--')
        plt.plot(xp11, np.polyval(p311, xp11), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('SPARK Prediction Based on Experience And Salary', fontsize=10)
        pivot_data2 = plt.gca()
        pivot_data2.spines['right'].set_color('none')
        pivot_data2.spines['top'].set_color('none')

        buffer2 = io.BytesIO()

        plt.savefig(buffer2)

        buffer2.seek(0)

        image_png2 = buffer2.getvalue()
        buffer2.close()
        graphic2 = base64.b64encode(image_png2)
        graphic2 = graphic2.decode('utf-8')

#HBASE Analysis

        data_frame3 = df_category.loc[(df_category['Technology'] == 'HBase') & (df_category['Country'] == country)]

        dfx111 = data_frame3['Exp']
        dfx111.to_numpy()

        dfy111 = data_frame3['Sal']
        dfy111.to_numpy()
        p1111 = np.polyfit(dfx111, dfy111, 1)
        # p2111 = np.polyfit(dfx111, dfy111, 2)
        p3111 = np.polyfit(dfx111, dfy111, 3)

        fig111, ax111 = plt.subplots()
        data_frame3.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax111)
        if country == 'India':

            xp111 = np.linspace(30, 1, 50)
        else:
            xp111 = np.linspace(30, 1, 100)
        plt.plot(xp111, np.polyval(p1111, xp111), 'r', label="Linear Pred")
        # plt.plot(xp111, np.polyval(p2111, xp111), 'b--')
        plt.plot(xp111, np.polyval(p3111, xp111), 'm:',label="Polynomial Pred")
        plt.xlabel('Experience (In Year)')

        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('HBASE Prediction Based on Experience And Salary', fontsize=10)
        pivot_data3 = plt.gca()
        pivot_data3.spines['right'].set_color('none')
        pivot_data3.spines['top'].set_color('none')

        buffer3 = io.BytesIO()

        plt.savefig(buffer3)


        buffer3.seek(0)

        image_png3 = buffer3.getvalue()
        buffer3.close()
        graphic3 = base64.b64encode(image_png3)
        graphic3 = graphic3.decode('utf-8')

        return render(request, "myapp/regression.html",
                      {'graphic': graphic, 'graphic1': graphic1, 'graphic2': graphic2, 'graphic3': graphic3,
                       'category_name':category_name ,'Category':Category})


#ERP ANAlysis

    elif  categoryname =='ERP':

#People soft Analysis

        data_frame2 = df_category.loc[(df_category['Technology'] == 'Peoplesoft') & (df_category['Country'] == country)]

        dfx11 = data_frame2['Exp']
        dfx11.to_numpy()

        dfy11 = data_frame2['Sal']
        dfy11.to_numpy()
        p111 = np.polyfit(dfx11, dfy11, 1)
        # p211 = np.polyfit(dfx11, dfy11, 2)
        p311 = np.polyfit(dfx11, dfy11, 3)

        fig11, ax11 = plt.subplots()
        data_frame2.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax11)
        if country == 'India':

            xp11 = np.linspace(20, 1, 100)
        else:
            xp11 = np.linspace(30, 1, 100)
        plt.plot(xp11, np.polyval(p111, xp11), 'r', label="Linear Pred")
        # plt.plot(xp11, np.polyval(p211, xp11), 'b--')
        plt.plot(xp11, np.polyval(p311, xp11), 'm:',label="Ploynomial Pred")



        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('People Soft Prediction Based on Experience And Salary', fontsize=10)
        data_frame2 = plt.gca()
        data_frame2.spines['right'].set_color('none')
        data_frame2.spines['top'].set_color('none')

        buffer2 = io.BytesIO()

        plt.savefig(buffer2)

        buffer2.seek(0)

        image_png2 = buffer2.getvalue()
        buffer2.close()
        graphic2 = base64.b64encode(image_png2)
        graphic2 = graphic2.decode('utf-8')


#JDEdward Analysis


        data_frame3 = df_category.loc[(df_category['Technology'] == 'JD Edwards') & (df_category['Country'] == country)]

        dfx111 = data_frame3['Exp']
        dfx111.to_numpy()

        dfy111 = data_frame3['Sal']
        dfy111.to_numpy()
        p1111 = np.polyfit(dfx111, dfy111, 1)
        # p2111 = np.polyfit(dfx111, dfy111, 2)
        p3111 = np.polyfit(dfx111, dfy111, 3)

        fig111, ax111 = plt.subplots()
        data_frame3.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax111)
        if country == 'India':

            xp111 = np.linspace(20, 1, 100)
        else:
            xp111 = np.linspace(30, 1, 100)
        plt.plot(xp111, np.polyval(p1111, xp111), 'r', label="Linear Pred")
        # plt.plot(xp111, np.polyval(p2111, xp111), 'b--')
        plt.plot(xp111, np.polyval(p3111, xp111), 'm:',label="Polynomial Pred")
        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')



        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('JDEdward Prediction Based on Experience And Salary', fontsize=10)
        data_frame3 = plt.gca()
        data_frame3.spines['right'].set_color('none')
        data_frame3.spines['top'].set_color('none')

        buffer3 = io.BytesIO()

        plt.savefig(buffer3)


        buffer3.seek(0)

        image_png3 = buffer3.getvalue()
        buffer3.close()
        graphic3 = base64.b64encode(image_png3)
        graphic3 = graphic3.decode('utf-8')

#NetSuit Analysis


        data_frame4 = df_category.loc[(df_category['Technology'] == 'Netsuite') & (df_category['Country'] == country)]

        dfx1111 = data_frame4['Exp']
        dfx1111.to_numpy()

        dfy1111 = data_frame4['Sal']
        dfy1111.to_numpy()
        p11111 = np.polyfit(dfx1111, dfy1111, 1)
        # p21111 = np.polyfit(dfx1111, dfy1111, 2)
        p31111 = np.polyfit(dfx1111, dfy1111, 3)

        fig1111, ax1111 = plt.subplots()
        data_frame4.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax1111)
        if country == 'India':

            xp1111 = np.linspace(20, 1, 100)
        else:
            xp1111 = np.linspace(30, 1, 100)
        plt.plot(xp1111, np.polyval(p11111, xp1111), 'r', label="Linear Pred")
        # plt.plot(xp1111, np.polyval(p21111, xp1111), 'b--')
        plt.plot(xp1111, np.polyval(p31111, xp1111), 'm:',label="Polynomial Pred")
        plt.xlabel('Experience (In Year)')



        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('NetSuit Prediction Based on Experience And Salary', fontsize=10)
        data_frame4 = plt.gca()
        data_frame4.spines['right'].set_color('none')
        data_frame4.spines['top'].set_color('none')

        buffer4 = io.BytesIO()

        plt.savefig(buffer4)

        buffer4.seek(0)

        image_png4 = buffer4.getvalue()
        buffer4.close()
        graphic4 = base64.b64encode(image_png4)
        graphic4 = graphic4.decode('utf-8')

#Siebel Analysis

        data_frame5 = df_category.loc[(df_category['Technology'] == 'Siebel') & (df_category['Country'] == country)]

        dfx11111 = data_frame5['Exp']
        dfx11111.to_numpy()

        dfy11111 = data_frame5['Sal']
        dfy11111.to_numpy()
        p111111 = np.polyfit(dfx11111, dfy11111, 1)
        # p211111 = np.polyfit(dfx11111, dfy11111, 2)
        p311111 = np.polyfit(dfx11111, dfy11111, 3)

        fig11111, ax11111 = plt.subplots()
        data_frame5.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax11111)
        if country == 'India':

            xp11111 = np.linspace(20, 1, 100)
        else:
            xp11111 = np.linspace(30, 1, 100)
        plt.plot(xp11111, np.polyval(p111111, xp11111), 'r', label="Linear Pred")
        # plt.plot(xp11111, np.polyval(p211111, xp11111), 'b--')
        plt.plot(xp11111, np.polyval(p311111, xp11111), 'm:',label="Polynomial Pred")



        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Siebel Prediction Based on Experience And Salary', fontsize=10)
        data_frame5 = plt.gca()
        data_frame5.spines['right'].set_color('none')
        data_frame5.spines['top'].set_color('none')

        buffer5 = io.BytesIO()

        plt.savefig(buffer5)

        buffer5.seek(0)

        image_png5 = buffer5.getvalue()
        buffer5.close()
        graphic5 = base64.b64encode(image_png5)
        graphic5 = graphic5.decode('utf-8')

#Oracle EBS Analysis


        data_frame6 = df_category.loc[(df_category['Technology'] == 'Oracle EBS') & (df_category['Country'] == country)]

        dfx111111 = data_frame6['Exp']
        dfx111111.to_numpy()

        dfy111111 = data_frame6['Sal']
        dfy111111.to_numpy()
        p1111111 = np.polyfit(dfx111111, dfy111111, 1)
        # p2111111 = np.polyfit(dfx111111, dfy111111, 2)
        p3111111 = np.polyfit(dfx111111, dfy111111, 3)

        fig111111, ax111111 = plt.subplots()
        data_frame6.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax111111)
        if country == 'India':

            xp111111 = np.linspace(20, 1, 100)
        else:
            xp111111 = np.linspace(30, 1, 100)
        plt.plot(xp111111, np.polyval(p1111111, xp111111), 'r', label="Linear Pred")
        # plt.plot(xp111111, np.polyval(p2111111, xp111111), 'b--')
        plt.plot(xp111111, np.polyval(p3111111, xp111111), 'm:',label="Polynomial Pred")
        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')


        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Oracle EBS Prediction Based on Experience And Salary', fontsize=10)
        data_frame6 = plt.gca()
        data_frame6.spines['right'].set_color('none')
        data_frame6.spines['top'].set_color('none')

        buffer6 = io.BytesIO()

        plt.savefig(buffer6)


        buffer6.seek(0)

        image_png6 = buffer6.getvalue()
        buffer6.close()
        graphic6 = base64.b64encode(image_png6)
        graphic6 = graphic6.decode('utf-8')

        return render(request, "myapp/regression.html",{ 'category_name':category_name ,'Category':Category,'graphic2': graphic2,'graphic3': graphic3,'graphic4': graphic4,'graphic5': graphic5,'graphic6': graphic6})


#EPM Analysis

    elif  categoryname  == 'EPM':
        data_frame = df_category.loc[(df_category['Technology'] == 'Anaplan') & (df_category['Country'] == country)]

        dfx = data_frame['Exp']
        dfx.to_numpy()

        dfy = data_frame['Sal']
        dfy.to_numpy()
        p1 = np.polyfit(dfx, dfy, 1)
        # p2 = np.polyfit(dfx, dfy, 2)
        p3 = np.polyfit(dfx, dfy, 3)

        fig, ax = plt.subplots()
        data_frame.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax)
        if country == 'India':

            xp = np.linspace(20, 1, 100)
        else:
            xp = np.linspace(20, 1, 100)
        plt.plot(xp, np.polyval(p1, xp), 'r', label="Linear Pred")
        # plt.plot(xp, np.polyval(p2, xp), 'b--')
        plt.plot(xp, np.polyval(p3, xp), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 40000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Anaplan Prediction Based on Experience And Salary', fontsize=10)
        data_frame = plt.gca()
        data_frame.spines['right'].set_color('none')
        data_frame.spines['top'].set_color('none')

        buffer = io.BytesIO()

        plt.savefig(buffer)

        buffer.seek(0)

        image_png = buffer.getvalue()
        buffer.close()
        graphic = base64.b64encode(image_png)
        graphic = graphic.decode('utf-8')

#Host Analytics Analysis

        data_frame1 = df_category.loc[(df_category['Technology'] == 'Host Analytics') & (df_category['Country'] == country)]

        dfx1 = data_frame1['Exp']
        dfx1.to_numpy()

        dfy1 = data_frame1['Sal']
        dfy1.to_numpy()
        p11 = np.polyfit(dfx1, dfy1, 1)
        # p21 = np.polyfit(dfx1, dfy1, 2)
        p31 = np.polyfit(dfx1, dfy1, 3)

        fig1, ax1 = plt.subplots()
        data_frame1.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax1)
        if country == 'India':

            xp1 = np.linspace(20, 1, 100)
        else:
            xp1 = np.linspace(30, 1, 100)
        plt.plot(xp1, np.polyval(p11, xp1), 'r', label="Linear Pred")
        # plt.plot(xp1, np.polyval(p21, xp1), 'b--')
        plt.plot(xp1, np.polyval(p31, xp1), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Host Analytics Prediction Based on Experience And Salary', fontsize=10)
        data_frame1= plt.gca()
        data_frame1.spines['right'].set_color('none')
        data_frame1.spines['top'].set_color('none')

        buffer1 = io.BytesIO()

        plt.savefig(buffer1)


        buffer1.seek(0)

        image_png1 = buffer1.getvalue()
        buffer1.close()
        graphic1 = base64.b64encode(image_png1)
        graphic1 = graphic1.decode('utf-8')


    #BPC Analysis

        data_frame2 = df_category.loc[(df_category['Technology'] == 'BPC') & (df_category['Country'] == country)]

        dfx11 = data_frame2['Exp']
        dfx11.to_numpy()

        dfy11 = data_frame2['Sal']
        dfy11.to_numpy()
        p111 = np.polyfit(dfx11, dfy11, 1)
        # p211 = np.polyfit(dfx11, dfy11, 2)
        p311 = np.polyfit(dfx11, dfy11, 3)

        fig11, ax11 = plt.subplots()
        data_frame2.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax11)
        if country == 'India':

            xp11 = np.linspace(20, 1, 100)
        else:
            xp11 = np.linspace(30, 1, 100)
        plt.plot(xp11, np.polyval(p111, xp11), 'r', label="Linear Pred")
        # plt.plot(xp11, np.polyval(p211, xp11), 'b--')
        plt.plot(xp11, np.polyval(p311, xp11), 'm:',label="Polynomial Pred")
        plt.xlabel('Experience (In Year)')

        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('BPC Prediction Based on Experience And Salary', fontsize=10)
        data_frame2 = plt.gca()
        data_frame2.spines['right'].set_color('none')
        data_frame2.spines['top'].set_color('none')

        buffer2 = io.BytesIO()

        plt.savefig(buffer2)


        buffer2.seek(0)

        image_png2 = buffer2.getvalue()
        buffer2.close()
        graphic2 = base64.b64encode(image_png2)
        graphic2 = graphic2.decode('utf-8')


 #Hyperion Analysis

        data_frame3 = df_category.loc[(df_category['Technology'] == 'Hyperion') & (df_category['Country'] == country)]

        dfx111 = data_frame3['Exp']
        dfx111.to_numpy()

        dfy111 = data_frame3['Sal']
        dfy111.to_numpy()
        p1111 = np.polyfit(dfx111, dfy111, 1)
        # p2111 = np.polyfit(dfx111, dfy111, 2)
        p3111 = np.polyfit(dfx111, dfy111, 3)

        fig111, ax111 = plt.subplots()
        data_frame3.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax111)
        if country == 'India':

            xp111 = np.linspace(20, 1, 100)
        else:
            xp111 = np.linspace(30, 1, 100)
        plt.plot(xp111, np.polyval(p1111, xp111), 'r', label="Linear Pred")
        # plt.plot(xp111, np.polyval(p2111, xp111), 'b--')
        plt.plot(xp111, np.polyval(p3111, xp111), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Hyperion Prediction Based on Experience And Salary', fontsize=10)
        data_frame3 = plt.gca()
        data_frame3.spines['right'].set_color('none')
        data_frame3.spines['top'].set_color('none')

        buffer3 = io.BytesIO()

        plt.savefig(buffer3)


        buffer3.seek(0)

        image_png3 = buffer3.getvalue()
        buffer3.close()
        graphic3 = base64.b64encode(image_png3)
        graphic3 = graphic3.decode('utf-8')


        return render(request, "myapp/regression.html",{'graphic': graphic, 'graphic1': graphic1, 'graphic2': graphic2, 'graphic3': graphic3,  'category_name':category_name ,'Category':Category})


  #CRM Analysis

    elif  categoryname  == 'CRM':

        data_frame = df_category.loc[(df_category['Technology'] == 'Salesforce') & (df_category['Country'] == country)]

        dfx = data_frame['Exp']
        dfx.to_numpy()

        dfy = data_frame['Sal']
        dfy.to_numpy()
        p1 = np.polyfit(dfx, dfy, 1)
        # p2 = np.polyfit(dfx, dfy, 2)
        p3 = np.polyfit(dfx, dfy, 3)

        fig, ax = plt.subplots()
        data_frame.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax)
        if country == 'India':

            xp = np.linspace(20, 1, 100)
        else:
            xp = np.linspace(30, 1, 100)
        plt.plot(xp, np.polyval(p1, xp), 'r', label="Linear Pred")
        # plt.plot(xp, np.polyval(p2, xp), 'b--')
        plt.plot(xp, np.polyval(p3, xp), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Salesforce Prediction Based on Experience And Salary', fontsize=10)
        data_frame = plt.gca()
        data_frame.spines['right'].set_color('none')
        data_frame.spines['top'].set_color('none')

        buffer = io.BytesIO()

        plt.savefig(buffer)

        buffer.seek(0)

        image_png = buffer.getvalue()
        buffer.close()
        graphic = base64.b64encode(image_png)
        graphic = graphic.decode('utf-8')

   #Microsoft Analysis

        data_frame1 = df_category.loc[(df_category['Technology'] == 'Microsoft ') & (df_category['Country'] == country)]

        dfx1 = data_frame1['Exp']
        dfx1.to_numpy()

        dfy1 = data_frame1['Sal']
        dfy1.to_numpy()
        p11 = np.polyfit(dfx1, dfy1, 1)
        # p21 = np.polyfit(dfx1, dfy1, 2)
        p31 = np.polyfit(dfx1, dfy1, 3)

        fig1, ax1 = plt.subplots()
        data_frame1.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax1)
        if country == 'India':

            xp1 = np.linspace(20, 1, 100)
        else:
            xp1 = np.linspace(30, 1, 100)
        plt.plot(xp1, np.polyval(p11, xp1), 'r', label="Linear Pred")
        # plt.plot(xp1, np.polyval(p21, xp1), 'b--')
        plt.plot(xp1, np.polyval(p31, xp1), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')
        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Microsoft Prediction Based on Experience And Salary', fontsize=10)
        data_frame1 = plt.gca()
        data_frame1.spines['right'].set_color('none')
        data_frame1.spines['top'].set_color('none')

        buffer1 = io.BytesIO()

        plt.savefig(buffer1)


        buffer1.seek(0)

        image_png1 = buffer1.getvalue()
        buffer1.close()
        graphic1 = base64.b64encode(image_png1)
        graphic1 = graphic1.decode('utf-8')

#SAS Analysis

        data_frame2 = df_category.loc[(df_category['Technology'] == 'SAS') & (df_category['Country'] == country)]

        dfx11 = data_frame2['Exp']
        dfx11.to_numpy()

        dfy11 = data_frame2['Sal']
        dfy11.to_numpy()
        p111 = np.polyfit(dfx11, dfy11, 1)
        # p211 = np.polyfit(dfx11, dfy11, 2)
        p311 = np.polyfit(dfx11, dfy11, 3)

        fig11, ax11 = plt.subplots()
        data_frame2.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax11)
        if country == 'India':

            xp11 = np.linspace(20, 1, 100)
        else:
            xp11 = np.linspace(30, 1, 100)
        plt.plot(xp11, np.polyval(p111, xp11), 'r', label="Linear Pred")
        # plt.plot(xp11, np.polyval(p211, xp11), 'b--')
        plt.plot(xp11, np.polyval(p311, xp11), 'm:',label="Polynomial Pred")
        plt.xlabel('Experience (In Year)')

        if country == 'India':
            plt.xlim(2, 20)
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('SAS Prediction Based on Experience And Salary', fontsize=10)
        data_frame2 = plt.gca()
        data_frame2.spines['right'].set_color('none')
        data_frame2.spines['top'].set_color('none')

        buffer2 = io.BytesIO()

        plt.savefig(buffer2)


        buffer2.seek(0)

        image_png2 = buffer2.getvalue()
        buffer2.close()
        graphic2 = base64.b64encode(image_png2)
        graphic2 = graphic2.decode('utf-8')


        return render(request, "myapp/regression.html",
                      {'graphic': graphic, 'graphic1': graphic1, 'graphic2': graphic2,
                       'category_name':category_name ,'Category':Category})




    elif  categoryname  == 'Visualization':

        data_frame = df_category.loc[(df_category['Technology'] == 'Spotfire') & (df_category['Country'] == country)]

        dfx = data_frame['Exp']
        dfx.to_numpy()

        dfy = data_frame['Sal']
        dfy.to_numpy()
        p1 = np.polyfit(dfx, dfy, 1)
        # p2 = np.polyfit(dfx, dfy, 2)
        p3 = np.polyfit(dfx, dfy, 3)

        fig, ax = plt.subplots()
        data_frame.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax)
        if country == 'India':

            xp = np.linspace(20, 1, 150)
        else:
            xp = np.linspace(30, 1, 100)
        plt.plot(xp, np.polyval(p1, xp), 'r', label="Linear Pred")
        # plt.plot(xp, np.polyval(p2, xp), 'b--')
        plt.plot(xp, np.polyval(p3, xp), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.ylabel('Salary (In Lac-INR)')

        else:
            plt.xlim(2, 20)
            plt.ylim(0, 60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Spotfire Prediction Based on Experience And Salary', fontsize=10)
        data_frame = plt.gca()
        data_frame.spines['right'].set_color('none')
        data_frame.spines['top'].set_color('none')

        buffer = io.BytesIO()

        plt.savefig(buffer)

        buffer.seek(0)

        image_png = buffer.getvalue()
        buffer.close()
        graphic = base64.b64encode(image_png)
        graphic = graphic.decode('utf-8')

#Tableau Analysis

        data_frame1 = df_category.loc[(df_category['Technology'] == 'Tableau') & (df_category['Country'] == country)]

        dfx1 = data_frame1['Exp']
        dfx1.to_numpy()

        dfy1 = data_frame1['Sal']
        dfy1.to_numpy()
        p11 = np.polyfit(dfx1, dfy1, 1)
        # p21 = np.polyfit(dfx1, dfy1, 2)
        p31 = np.polyfit(dfx1, dfy1, 3)

        fig1, ax1 = plt.subplots()
        data_frame1.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax1)
        if country == 'India':

            xp1 = np.linspace(20, 1, 100)
        else:
            xp1 = np.linspace(30, 1, 100)
        plt.plot(xp1, np.polyval(p11, xp1), 'r', label="Linear Pred")
        # plt.plot(xp1, np.polyval(p21, xp1), 'b--')
        plt.plot(xp1, np.polyval(p31, xp1), 'm:',label="Polynomial Pred")

        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.ylim(0, 60000)
            plt.xlim(2,20)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Tableau Prediction Based on Experience And Salary', fontsize=10)
        data_frame1 = plt.gca()
        data_frame1.spines['right'].set_color('none')
        data_frame1.spines['top'].set_color('none')

        buffer1 = io.BytesIO()

        plt.savefig(buffer1)


        buffer1.seek(0)

        image_png1 = buffer1.getvalue()
        buffer1.close()
        graphic1 = base64.b64encode(image_png1)
        graphic1 = graphic1.decode('utf-8')

 #Qlikview Analysis

        data_frame2 = df_category.loc[(df_category['Technology'] == 'Qlikview') & (df_category['Country'] == country)]

        dfx11 = data_frame2['Exp']
        dfx11.to_numpy()

        dfy11 = data_frame2['Sal']
        dfy11.to_numpy()
        p111 = np.polyfit(dfx11, dfy11, 1)
        # p211 = np.polyfit(dfx11, dfy11, 2)
        p311 = np.polyfit(dfx11, dfy11, 3)

        fig11, ax11 = plt.subplots()
        data_frame2.plot(kind='scatter', x='Exp', y='Sal', color='blue', marker='o', label="Actual", ax=ax11)
        if country == 'India':

            xp11 = np.linspace(20, 1, 100)
        else:
            xp11 = np.linspace(30, 1, 100)
        plt.plot(xp11, np.polyval(p111, xp11), 'r', label="Linear Pred")
        # plt.plot(xp1, np.polyval(p211, xp11), 'b--')
        plt.plot(xp1, np.polyval(p311, xp11), 'm:',label="Polynomial Pred")
        plt.xlim(2, 20)
        plt.xlabel('Experience (In Year)')
        if country == 'India':
            plt.ylabel('Salary (In Lac-INR)')
        else:
            plt.xlim(2,20)
            plt.ylim(0,60000)
            plt.ylabel('Salary (In USD)')

        plt.legend()
        plt.tight_layout()
        plt.subplots_adjust(left=0.15, right=0.94, top=0.90, bottom=0.30)
        plt.title('Qlikview Prediction Based on Experience And Salary', fontsize=10)
        data_frame2 = plt.gca()
        data_frame2.spines['right'].set_color('none')
        data_frame2.spines['top'].set_color('none')

        buffer2 = io.BytesIO()

        plt.savefig(buffer2)


        buffer2.seek(0)

        image_png2 = buffer2.getvalue()
        buffer2.close()
        graphic2 = base64.b64encode(image_png2)
        graphic2 = graphic2.decode('utf-8')


        return render(request, "myapp/regression.html",
                      {'graphic': graphic, 'graphic1': graphic1, 'graphic2': graphic2,
                        'category_name':category_name ,'Category':Category})



    return render(request, "myapp/regression.html",{'category_name':category_name ,'Category':Category})


