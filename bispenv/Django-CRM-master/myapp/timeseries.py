from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from .models import csvdata,category,technology
from django_pandas.io import read_frame

import numpy as np
import matplotlib.pyplot as plt
import base64
import csv, io
import matplotlib
matplotlib.use('Agg')

@csrf_exempt
def time_series_analysis(request):
    Category = category.objects.all()


    return render(request, "myapp/series.html", {'Category':Category})

@csrf_exempt
def timeanalysis(request,name):
    category_name = category.objects.get(Category=name)
    Category = category.objects.all()
    categoryname = str(category_name)
    # cname = category.objects.all()
    # context = {'catagery_id': cname}
    category1 = csvdata.objects.all()
    df_category = read_frame(category1)
    country = request.session['country']
    data_frame = df_category.loc[(df_category['Category'] == categoryname) & (df_category['Country'] == country)]
    df = data_frame.drop(data_frame.columns[0], axis=1)
    pivot_data = data_frame.pivot_table(index='Month',columns='Technology', values='Opening', aggfunc=np.sum, fill_value=0)


    ma_val=pivot_data.max()
    max_val=ma_val.max()


    pivot_data = pivot_data.reindex(['Jan', 'Feb', 'Mar', 'Apr','May','June','July','August','September','Oct','November'])
    pivot_data.plot(kind='line',marker='o')
    pivot_data = plt.gca()
    pivot_data.spines['right'].set_color('none')
    pivot_data.spines['top'].set_color('none')
    plt.tight_layout()
    plt.subplots_adjust(left=0.09, right=0.94, top=0.90, bottom=0.30)
    plt.title(categoryname + ' ' + 'Opening In different Month According To Technologies', fontsize=10)
    plt.ylabel('No Of Opening')
    plt.yticks(np.arange(0, max_val, 5))
    plt.ylim(0, max_val)
    plt.xlabel('Months')
    plt.legend(loc=9, bbox_to_anchor=(0.5, -0.4), ncol=8, columnspacing=0.5, borderaxespad=0, fancybox=True,
               framealpha=0.5, )
    buffer = io.BytesIO()

    plt.savefig(buffer)


    buffer.seek(0)

    image_png = buffer.getvalue()
    buffer.close()
    graphic = base64.b64encode(image_png)
    graphic = graphic.decode('utf-8')

    # Variable For Lcation
    data_frame1 = data_frame[
        (data_frame.Location == 'Pune') | (data_frame.Location == 'Chennai') | (data_frame.Location == 'Bengaluru') | (
                data_frame.Location == 'Delhi ') | (data_frame.Location == 'Hyderabad')]
    pivot_data1 = data_frame.pivot_table(index='Month', columns='Location', values='Opening', aggfunc=np.sum,
                                          fill_value=0)
    pivot_data1 = pivot_data1.reindex(['Jan', 'Feb', 'Mar', 'Apr','May','June','July','August','September','Oct','November'])
    ma_val1 = pivot_data1.max()
    max_val1 = ma_val1.max()

    pivot_data1.plot(kind='line',marker='o')
    pivot_data1 = plt.gca()
    pivot_data1.spines['right'].set_color('none')
    pivot_data1.spines['top'].set_color('none')


    plt.tight_layout()
    plt.subplots_adjust(left=0.11, right=0.94, top=0.90, bottom=0.30)
    plt.title(categoryname + ' ' + 'Opening In different Month According To Location', fontsize=10)

    plt.ylabel('No Of Opening')
    plt.yticks(np.arange(0, max_val1, 5))
    plt.ylim(0, max_val1)
    # plt.yticks(np.arange(0, 1, step=0.5))
    plt.xlabel('Months', fontsize=10)
    plt.legend(loc=9, bbox_to_anchor=(0.5, -0.4), ncol=8, columnspacing=0.5, borderaxespad=0, fancybox=True,
               framealpha=0.5, )
    buffer1 = io.BytesIO()
    plt.savefig(buffer1)



    buffer1.seek(0)
    image_png1 = buffer1.getvalue()
    buffer1.close()
    graphic1 = base64.b64encode(image_png1)
    graphic1 = graphic1.decode('utf-8')

    # Variable for Experience

    pivot_data2 = data_frame.pivot_table(index='Month', columns='Experience', values='Opening', aggfunc=np.sum,
                                         fill_value=0)
    pivot_data2 = pivot_data2.reindex(['Jan', 'Feb', 'Mar', 'Apr','May','June','July','August','September','Oct','November'])
    ma_val2 = pivot_data2.max()
    max_val2 = ma_val2.max()

    pivot_data2.plot(kind='line',marker='o')
    pivot_data2 = plt.gca()
    pivot_data2.spines['right'].set_color('none')
    pivot_data2.spines['top'].set_color('none')
    plt.tight_layout()
    plt.subplots_adjust(left=0.11, right=0.94, top=0.90, bottom=0.30)
    plt.title(categoryname + ' ' + 'Opening different month According To Experience', fontsize=10)
    plt.ylabel('No Of Opening')
    plt.yticks(np.arange(0, max_val2, 5))
    plt.ylim(0, max_val2)

    plt.xlabel('Months')
    plt.legend(loc=9, bbox_to_anchor=(0.5, -0.4), ncol=8, columnspacing=0.5, borderaxespad=0, fancybox=True,
               framealpha=0.5, )
    buffer2 = io.BytesIO()
    plt.savefig(buffer2, format='png')

    plt.figure(num=None, figsize=(6, 3), dpi=50)
    # plt.rcParams["figure.figsize"] = [16, 9]
    buffer2.seek(0)
    image_png2 = buffer2.getvalue()
    buffer2.close()
    graphic2 = base64.b64encode(image_png2)
    graphic2 = graphic2.decode('utf-8')

    # Variable for company

    pivot_data3 = data_frame.pivot_table(index='Month', columns='Company', values='Opening', aggfunc=np.sum,
                                         fill_value=0)
    pivot_data3 = pivot_data3.reindex(['Jan', 'Feb', 'Mar', 'Apr','May','June','July','August','September'])
    ma_val3 = pivot_data3.max()
    max_val3 = ma_val3.max()


    pivot_data3.plot(kind='line',marker='o')
    pivot_data3 = plt.gca()
    pivot_data3.spines['right'].set_color('none')
    pivot_data3.spines['top'].set_color('none')


    plt.tight_layout()
    plt.subplots_adjust(left=0.09, right=0.94, top=0.90, bottom=0.30)
    plt.title(categoryname + ' ' + 'Opening In different month According To Company', fontsize=10)
    plt.ylabel('No Of Opening')
    plt.yticks(np.arange(0, max_val3, 5))
    plt.ylim(0,  max_val3)
    plt.xlabel('Months')
    plt.legend(loc=9, bbox_to_anchor=(0.5, -0.4), ncol=8, columnspacing=0.5, borderaxespad=0, fancybox=True,
               framealpha=0.5, )
    buffer3 = io.BytesIO()
    plt.savefig(buffer3, format='png')

    plt.figure(num=None, figsize=(6, 3), dpi=50)
    buffer3.seek(0)
    image_png3 = buffer3.getvalue()
    buffer3.close()
    graphic3 = base64.b64encode(image_png3)
    graphic3 = graphic3.decode('utf-8')

    # Variable for salary

    data_frame1 = data_frame[data_frame.Salary != '0']


    # salary=data_frame1['1 To 4 L', '4 To 8 L', '8 To 12 L', '12 To 20 L']
    pivot_data4 = data_frame1.pivot_table(index='Month', columns='Salary', values='Opening', aggfunc=np.sum,
                                          fill_value=0)


    pivot_data4 = pivot_data4.reindex(['Jan', 'Feb', 'Mar', 'Apr','May','June','July','August','September','Oct','November'])
    ma_val4 = pivot_data4.max()
    max_val4 = ma_val4.max()



    # pivot_data.plot(kind=chart_type)
    pivot_data4.plot(kind='line',marker='o', label='Technology')
    pivot_data4 = plt.gca()
    pivot_data4.spines['right'].set_color('none')
    pivot_data4.spines['top'].set_color('none')



    plt.tight_layout()
    plt.subplots_adjust(left=0.09, right=0.94, top=0.90, bottom=0.30)
    plt.title(categoryname + ' ' + 'Opening In different month According To Salary', fontsize=10)
    plt.ylabel('No Of Opening')
    plt.yticks(np.arange(0, max_val4, 5))
    plt.ylim(0, max_val4)
    plt.xlabel('Months')
    plt.legend(loc=9, bbox_to_anchor=(0.5, -0.4), ncol=8, columnspacing=0.5, borderaxespad=0, fancybox=True,
               framealpha=0.5, )
    buffer4 = io.BytesIO()
    plt.savefig(buffer4, format='png')

    plt.figure(num=None, figsize=(6, 3), dpi=50)
    # plt.rcParams["figure.figsize"] = [16, 9]
    buffer4.seek(0)
    image_png4 = buffer4.getvalue()
    buffer4.close()
    graphic4 = base64.b64encode(image_png4)
    graphic4 = graphic4.decode('utf-8')


    return render(request, "myapp/timeseries.html",{'graphic': graphic,'graphic1': graphic1,'graphic2': graphic2,'graphic3': graphic3,'graphic4': graphic4,'category_name': category_name,'Category':Category})

