
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render,render_to_response
from .models import csvdata,category,technology
from django_pandas.io import read_frame


import pandas as pd



import itertools
from pandas.plotting import register_matplotlib_converters
import statsmodels.api as sm
register_matplotlib_converters()
import numpy as np
import matplotlib.pyplot as plt
import base64
import csv, io
import matplotlib
matplotlib.use('Agg')

@csrf_exempt
def forecasting(request):
    Category = category.objects.all()


    return render(request, "myapp/casting.html", {'Category':Category})

@csrf_exempt
def Prediction(request,name):
    category_name = category.objects.get(Category=name)
    Category = category.objects.all()
    categoryname = str(category_name)


    category_all = csvdata.objects.all()

    df_category = read_frame(category_all)
    country = request.session['country']



    data_frame = df_category.loc[(df_category['Category'] ==  categoryname) & (df_category['Country'] == country)]

    cols = ['id','Country', 'Category', 'Technology', 'Location', 'Experience', 'Company', 'Salary', 'Opening', 'Exp',
            'Month']

    data_frame.drop(cols, axis=1, inplace=True)
    data_frame['Timestamp'] = pd.to_datetime(data_frame.Date)
    data_frame.index = data_frame.Timestamp
    data_frame = data_frame.resample('D').mean()
    data_frame['Sal'] = data_frame['Sal'].fillna((data_frame['Sal'].mean()))
    # for seasonal data

    decomposition = sm.tsa.seasonal_decompose(data_frame, model='additive')
    p = d = q = range(0, 2)
    pdq = list(itertools.product(p, d, q))
    # print(pdq)
    seasonal_pdq = [(x[0], x[1], x[2], 12) for x in list(itertools.product(p, d, q))]
    # print('Examples of parameter combinations for Seasonal ARIMA...')
    # print('SARIMAX: {} x {}'.format(pdq[1], seasonal_pdq[1]))
    # print('SARIMAX: {} x {}'.format(pdq[1], seasonal_pdq[2]))
    # print('SARIMAX: {} x {}'.format(pdq[2], seasonal_pdq[3]))
    # print('SARIMAX: {} x {}'.format(pdq[2], seasonal_pdq[4]))

    # for histogram
    for param in pdq:
        for param_seasonal in seasonal_pdq:
            mod = sm.tsa.statespace.SARIMAX(data_frame, order=param, seasonal_order=param_seasonal,
                                            enforce_stationarity=False, enforce_invertibility=False)

    results = mod.fit()

    mod = sm.tsa.statespace.SARIMAX(data_frame,
                                    order=(1, 1, 1),
                                    seasonal_order=(1, 1, 0, 12),
                                    enforce_stationarity=False,
                                    enforce_invertibility=False)
    results = mod.fit(disp=0)

    # Prediction of actual data
    pred = results.get_prediction(start=pd.to_datetime('2019/04/01'), dynamic=False)


    # How accurate our prediction

    y_forecasted = pred.predicted_mean
    y_truth = data_frame['2019/01/01':]


    # forecast

    pred_uc = results.get_forecast(steps=100)
    pred_ci = pred_uc.conf_int()
    # print(pred_ci)
    ax = data_frame.plot(label='Actual_Salary', figsize=(14, 7))
    pred_uc.predicted_mean.plot(ax=ax, label='Forecast_Salary')
    ax.fill_between(pred_ci.index,
                    pred_ci.iloc[:, 0],
                    pred_ci.iloc[:, 1], color='k', alpha=.25)
    ax.set_xlabel('Months',fontsize=15)
    ax.set_ylabel('Salary',fontsize=15)
    if country == 'US':
        plt.ylim(6000, 60000)


    pivot_data4 = plt.gca()
    pivot_data4.spines['right'].set_color('none')
    pivot_data4.spines['top'].set_color('none')
    plt.subplots_adjust(left=0.09, right=0.94, top=0.90, bottom=0.30)
    plt.title('4 Month Actual data Of '+ categoryname + ' ' +'Remaining is Prediction of Salary' , fontsize=15)
    plt.legend(loc=2)




    buffer = io.BytesIO()

    plt.savefig(buffer)


    buffer.seek(0)

    image_png = buffer.getvalue()
    buffer.close()
    graphic = base64.b64encode(image_png)
    graphic = graphic.decode('utf-8')

    return render(request, "myapp/forecasting.html", {'graphic': graphic, 'category_name': category_name,'Category':Category})








