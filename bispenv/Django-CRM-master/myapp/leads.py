from django.shortcuts import  render_to_response

import pandas as pd
import numpy as np
from bokeh.models import  LabelSet
from matplotlib.pyplot import figure
from bokeh.layouts import  column
from bokeh.plotting import figure
from bokeh.embed import components

from bokeh.models import HoverTool
from bokeh.models import ColumnDataSource
from bokeh.core.properties import value

from collections import Counter
from bokeh.palettes import  Category20b, Category20c

from math import pi
from bokeh.transform import cumsum


def leadanalytics(request):
    # this is country wise
    grouped = pd.read_csv('F:/django/django-env/myproject/media/salesforce.csv')
    pp = grouped.groupby('Country__c').count()
    pivotedcsv = pp.to_csv('salesforce_pivoted.csv')
    csvdata = pd.read_csv('salesforce_pivoted.csv')
    source = ColumnDataSource(pd.DataFrame(csvdata))
    Country = source.data['Country__c'].tolist()
    hover = HoverTool()
    hover.tooltips = [("Leads", "@Phone")]
    hover.mode = 'vline'
    p = figure(x_range=Country, plot_width=530, plot_height=400)
    # ,tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'], toolbar_location="right")
    # print(p)
    labels = LabelSet(x='Country__c', y='Phone', text='Phone', level='glyph',
                      x_offset=-13.5, y_offset=0, source=source, render_mode='canvas', text_color="black")
    p.add_layout(labels)
    p.title.text = "Leads on the basis of country"
    p.title.align = "center"
    p.title.text_color = "black"
    p.title.text_font_size = "15px"
    p.outline_line_width = 7
    p.toolbar.logo = None

    # p.toolbar_location = None
    # p.outline_line_alpha = 0.3
    # p.outline_line_color = "navy"
    p.xaxis.axis_label = 'Countries'

    p.xaxis.axis_label_text_font_size = "15pt"

    p.xaxis.axis_label_text_font = "times"
    p.xaxis.axis_label_text_color = "black"
    p.yaxis.axis_label = 'No of Leads'
    p.yaxis.axis_label_text_font_size = "15pt"

    p.yaxis.axis_label_text_font = "times"
    p.yaxis.axis_label_text_color = "black"

    p.add_tools(hover)
    vbar = p.vbar(x='Country__c', top='Phone', source=source, color='#756bb1', width=0.5, )
    layout = column(p)
    script, div = components(layout)

    # now for course category
    pp1 = grouped.groupby('CourseCategory__c').count()
    pivotedcsv_course = pp1.to_csv('salesforce_pivoted_course.csv')
    csvdata_course = pd.read_csv('salesforce_pivoted_course.csv')
    sourcedata_course = ColumnDataSource(pd.DataFrame(csvdata_course))
    Course = sourcedata_course.data['CourseCategory__c'].tolist()
    hover = HoverTool()
    hover.tooltips = [("Leads", "@Phone")]
    hover.mode = 'vline'
    p1 = figure(x_range=Course, plot_width=530, plot_height=400)
    labels1 = LabelSet(x='CourseCategory__c', y='Phone', text='Phone', level='glyph',
                       x_offset=-13.5, y_offset=0, source=sourcedata_course, render_mode='canvas', text_color="black")
    p1.add_layout(labels1)
    p1.title.text = "Leads on the basis of Technology"
    p1.title.align = "center"
    p1.title.text_color = "black"
    p1.title.text_font_size = "15px"
    p1.outline_line_width = 7
    p1.toolbar.logo = None

    # p.toolbar_location = None
    # p.outline_line_alpha = 0.3
    # p.outline_line_color = "navy"
    p1.xaxis.axis_label = 'Technologies'

    p1.xaxis.axis_label_text_font_size = "15pt"

    p1.xaxis.axis_label_text_font = "times"
    p1.xaxis.axis_label_text_color = "black"
    p1.yaxis.axis_label = 'No of Leads'
    p1.yaxis.axis_label_text_font_size = "15pt"

    p1.yaxis.axis_label_text_font = "times"
    p1.yaxis.axis_label_text_color = "black"

    p1.add_tools(hover)
    vbar = p1.vbar(x='CourseCategory__c', top='Phone', source=sourcedata_course, color='#756bb1', width=0.5, )
    layout1 = column(p1)
    script1, div1 = components(layout1)

    # now for date wise we are getting the data
    pp2 = grouped.groupby('CreatedDate').count()
    pivotedcsv_date = pp2.to_csv('salesforce_pivoted_datewise.csv')  # saving the csv
    csvdata_date = pd.read_csv('salesforce_pivoted_datewise.csv')  # reading the saved csv
    sourcedata_date = ColumnDataSource(pd.DataFrame(csvdata_date))
    Datewise = sourcedata_date.data['CreatedDate'].tolist()
    hover = HoverTool()
    hover.tooltips = [("Leads", "@Phone")]
    hover.mode = 'vline'
    p2 = figure(x_range=Datewise, plot_width=530, plot_height=400)
    labels2 = LabelSet(x='CreatedDate', y='Phone', text='Phone', level='glyph',
                       x_offset=-13.5, y_offset=0, source=sourcedata_date, render_mode='canvas', text_color="black")
    p2.add_layout(labels2)
    p2.title.text = "Leads on the basis of Date"
    p2.title.align = "center"
    p2.title.text_color = "black"
    p2.title.text_font_size = "15px"
    p2.outline_line_width = 7
    p2.toolbar.logo = None

    p2.xaxis.axis_label = 'Date'

    p2.xaxis.axis_label_text_font_size = "15pt"

    p2.xaxis.axis_label_text_font = "times"
    p2.xaxis.axis_label_text_color = "black"
    p2.yaxis.axis_label = 'No of Leads'
    p2.yaxis.axis_label_text_font_size = "15pt"

    p2.yaxis.axis_label_text_font = "times"
    p2.yaxis.axis_label_text_color = "black"

    p2.add_tools(hover)
    vbar = p2.vbar(x='CreatedDate', top='Phone', source=sourcedata_date, color='#756bb1', width=0.5, )
    layout2 = column(p2)
    script2, div2 = components(layout2)






    # Country Wise Technology leads
    df = pd.read_csv('F:/django/django-env/myproject/media/salesforce.csv')
    # get the pivoted table
    table = df.pivot_table(index='Country__c', columns='CourseCategory__c', values='Lead', aggfunc=np.sum, fill_value=0)
    # now get the list of the column heads i.e.courselist headings
    headerList = list(table.head(2))
    print(headerList)
    # here the id of each columndatasource is created
    source1 = ColumnDataSource(pd.DataFrame(table))
    # here the list of countries are received now we can write only column head
    countrylist = source1.data['Country__c'].tolist()

    # figure is created here
    graphFigure = figure(x_range=countrylist, plot_width=530, plot_height=400,
                         tools=['save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                         toolbar_location="below")
    colorList = Category20b[len(headerList)]  # color list Category20b is fixed keyword
    # here the stacked bar is created
    stacked = graphFigure.vbar_stack(stackers=headerList, x='Country__c', color=colorList, source=source1,
                                     legend=[value(x) for x in headerList],
                                     name=headerList,
                                     width=0.5, )
    # whatever you want to show on bar hover this is the fixed for loop format it cant be changed
    for r in stacked:
        tec = r.name
        hover = HoverTool(tooltips=[
            ("%s " % tec, "@%s" % tec),
        ], renderers=[r])
        graphFigure.add_tools(hover)
    # this is the final step
    layout7 = column(graphFigure)
    script3, div3 = components(layout7)

    #################now month wise

    df2 = pd.read_csv('F:/django/django-env/myproject/media/salesforce.csv')
    # get the pivoted table
    table2 = df2.pivot_table(index='Month', columns='CourseCategory__c', values='Lead', aggfunc=np.sum, fill_value=0)
    # mm = pd.Series.unique(df2['Month'])
    # print(mm)
    # now get the list of the column heads i.e.courselist headings
    headerList2 = list(table2.head(2))
    # here the id of each columndatasource is created
    source2 = ColumnDataSource(pd.DataFrame(table2))
    # here the list of countries are received now we can write only column head
    countrylist = source2.data['Month'].tolist()

    # figure is created here
    monthwise = figure(x_range=countrylist, plot_width=530, plot_height=400,
                       tools=['save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                       toolbar_location="below")
    colorList = Category20b[len(headerList2)]  # color list Category20b is fixed keyword
    # here the stacked bar is created
    stacked = monthwise.vbar_stack(stackers=headerList2, x='Month', color=colorList, source=source2,
                                   legend=[value(x) for x in headerList2],
                                   name=headerList2,
                                   width=0.5, )
    # whatever you want to show on bar hover this is the fixed for loop format it cant be changed
    for r in stacked:
        tec = r.name
        hover = HoverTool(tooltips=[
            ("%s " % tec, "@%s" % tec),
        ], renderers=[r])
        monthwise.add_tools(hover)
    # this is the final step
    layout7 = column(monthwise)
    script4, div4 = components(layout7)

    ##now donut chart in bokeh

    x = Counter({
        'United States': 157, 'United Kingdom': 93, 'Japan': 89, 'China': 63,
        'Germany': 44, 'India': 42, 'Italy': 40, 'Australia': 35,
        'Brazil': 32, 'France': 31, 'Taiwan': 31, 'Spain': 29
    })
    # print(x)
    data = pd.DataFrame.from_dict(dict(x), orient='index').reset_index().rename(index=str, columns={0: 'value',
                                                                                                    'index': 'country'})
    data['angle'] = data['value'] / sum(x.values()) * 2 * pi
    data['color'] = Category20c[len(x)]

    # Plotting code

    p = figure(plot_height=350, title="Donut Chart", toolbar_location=None,
               tools="hover", tooltips=[("Country", "@country"), ("Value", "@value")])

    p.annular_wedge(x=0, y=1, inner_radius=0.2, outer_radius=0.4,
                    start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
                    line_color="white", fill_color='color', legend='country', source=data)

    p.axis.axis_label = None
    p.axis.visible = False
    p.grid.grid_line_color = None
    layout7 = column(p)
    script5, div5 = components(layout7)

    return render_to_response('myapp/leads.html',
                              {'script': script, 'div': div, 'script1': script1, 'div1': div1, 'script2': script2,
                               'div2': div2, 'script3': script3, 'div3': div3, 'script4': script4, 'div4': div4,
                               'script5': script5, 'div5': div5})
    # return render( request,'myapp/leadanalytics.html')


