from django.urls import path
from django.conf.urls import include, url
from myapp import views,regression,timeseries,forecasting,leads

app_name = "myapp"

urlpatterns = [
    # path('handler404',views.handler404,name='handler404'),

    path('', views.login1,name='login'),
    path('index/', views.index, name='index'),

    path('home/', views.Home, name='home'),
    path('userhome/', views.userhome, name='userhome'),
    path('userlogin/', views.userlogin, name='userlogin'),
    path('upload/', views.file_upload, name='upload'),
    path('technology/', views.technology, name='technology'),
    path('visual/', views.visual, name='visual'),
    path('dashboard/<str:name>', views.dashboard, name='dashboard'),
    path('Technology/', views.Technology, name='Technology'),
    path('Location/', views.Location, name='Location'),
    path('Experience/', views.Experience, name='Experience'),
    path('Company/', views.Company, name='Company'),
    path('Salary/', views.Salary, name='Salary'),
    url(r'^pdf/$', views.GeneratePdf.as_view(), name='pdf'),


#REgression Urls
   path('regression/<str:name>', regression.regression, name='regression'),
   path('predictive_analysis/', regression.predictive_analysis, name='predictive_analysis'),




#TimeSeries Urls
   path('time_series_analysis/', timeseries.time_series_analysis, name='time_series_analysis'),
# url(r'^timeanalysis/(?P<name>[\w|\W]+)'.replace('%20','-'), timeseries.timeanalysis, name='timeanalysis'),

#(r’^operationstatus/(?P<status>.*)$’, ‘myapp.backbone_view’)
   path('timeanalysis/<str:name>', timeseries.timeanalysis, name='timeanalysis'),
    # path('post/<slug>', timeseries.post, name='post'),

#forecasting Urls

    path('forecasting/', forecasting.forecasting, name='forecasting'),
    path('Prediction/<str:name>', forecasting.Prediction, name='Prediction'),


#Lead url
    path('leadanalytics/', leads.leadanalytics, name='leadanalytics'),
]
# urlpatterns += staticfiles_urlpatterns()
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
