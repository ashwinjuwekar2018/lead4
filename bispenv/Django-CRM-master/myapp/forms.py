from django import forms
from myapp.models import csvdata,category
class csvdataForm(forms.ModelForm):
    class Meta:
        model = csvdata
        fields = "__all__"


class categoryForm(forms.ModelForm):
    class Meta:
        model = category
        fields = "__all__"