#Salesforce Integration
from simple_salesforce import Salesforce
import pandas as pd
import datetime

def fetchall():
    sf = Salesforce(username='support@bisptrainings.com', password='testdrive$81',
                    security_token='Avlel3pAD0giztTrBUn3G9PrI')

    query_result = sf.query(
        'SELECT Name,Email,Description ,Country__c, CourseCategory__c, Phone,CreatedDate from Contact')

    items = {
        val: dict(query_result["records"][val])
        for val in range(query_result["totalSize"])
    }
    df = pd.DataFrame.from_dict(items, orient="index").drop(["attributes"], axis=1)
    df['CreatedDate'] = df['CreatedDate'].map(lambda x: str(x)[:10])
    csv_data = df.to_csv('salesforce.csv')

    last_time_executed = datetime.datetime.now()
    print(last_time_executed)

