from django.contrib import admin


# Register your models here.
from .models import csvdata,category,technology,country
# Register your models here.
class csvdataAdmin(admin.ModelAdmin):
   list_display = ('Country','Category','Technology','Location','Experience','Company','Salary','Exp','Sal','Month','Date')
   list_filter = ('Country','Category','Technology','Date','Month')
   search_fields = ('Country','Category','Technology')

class DataFileAdmin(admin.ModelAdmin):
   list_display = ('data')


admin.site.register(csvdata,csvdataAdmin)
admin.site.register(category)

admin.site.register(technology)
admin.site.register(country)



