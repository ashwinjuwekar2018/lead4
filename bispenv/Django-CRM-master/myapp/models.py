from django.db import models



#table for uplaod csv file
class csvdata(models.Model):
    Country=models.CharField(max_length=30,null=True)
    Category = models.CharField(max_length=30)
    Technology = models.CharField(max_length=30)
    Location = models.CharField(max_length=30)
    Experience = models.CharField(max_length=100)
    Company = models.CharField(max_length=30)
    Salary = models.CharField(max_length=30)
    Opening = models.IntegerField()
    Exp = models.IntegerField()
    Sal = models.IntegerField()

    Month=models.CharField(max_length=30,null=True)
    Date = models.CharField(max_length=30, null=True)


    def __str__(self):
        return self.Category + '' + self.Technology + '' + self.Location + '' + self.Experience + '' + self.Company + '' + self.Salary



class category(models.Model):
    Category = models.CharField(max_length=50,unique=True)
    slug = models.SlugField(max_length=50, null=True,unique=True,blank=True)

    def __str__(self):
        return self.Category





class technology(models.Model):
    Technology = models.CharField(max_length=50)
    Category=models.ForeignKey(category, on_delete=models.CASCADE,null=True)

    def __str__(self):
        return self.Technology





class country(models.Model):
    Country = models.CharField(max_length=50)

    def __str__(self):
        return self.Country



