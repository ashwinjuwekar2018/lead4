import random

from bokeh.io import export_png
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render
from django.contrib import messages
from django.template.loader import render_to_string
import csv, io

from bokeh.transform import dodge
from bokeh.core.properties import value
from bokeh.palettes import Spectral6

import numpy as np
import datetime
from bokeh.layouts import row, column
from bokeh.embed import components
from bokeh.models import ColumnDataSource, LabelSet, CustomJS, RadioButtonGroup

from bokeh.models.tools import HoverTool
from .util import render_to_pdf
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseRedirect
from matplotlib.pyplot import figure
from bokeh.plotting import figure

import matplotlib

matplotlib.use('Agg')

from django.views.generic import View

import pandas as pd
from django_pandas.io import read_frame
from .models import csvdata, category


# def handler404(request):
#     return render(request, 'myapp/404.html', status=404)
#
# def handler500(request):
#     return render(request, 'myapp/500.html', status=500)


def login1(request):
    return render(request, "myapp/login.html")


@csrf_exempt
def index(request):
    return render(request, "myapp/index.html")


@csrf_exempt
def userlogin(request):
    username = request.POST.get('user')
    password = request.POST.get('pass')

    if username == 'admin' and password == 'admin123':

        return HttpResponseRedirect('/index/')
    elif username == 'guest' and password == 'guest123':
        return HttpResponseRedirect('/index/')
    else:
        messages.success(request, 'Please enter valid name or password')
        return render(request, "myapp/login.html",)

    return request


@csrf_exempt
def Home(request):
    country = request.POST.get('country')
    request.session['country'] = country
    # country = request.session['country']

    return render(request, "myapp/home.html")


@csrf_exempt
def userhome(request):
    country = request.POST.get('country')
    p = request.session['country'] = country
    return render(request, "myapp/userhome.html")

def check_admin(user):
   return user.is_superuser

# Function for file upload


# Function for file upload

def file_upload(request):
    template = "myapp/upload.html"

    if request.method == "GET":
        return render(request, template)
    csv_file = request.FILES['file']

    if not csv_file.name.endswith('.csv'):
        messages.error(request, 'this is not a csv file')
        print('hello world')
    data_set = csv_file.read().decode('UTF-8')
    io_string = io.StringIO(data_set)
    next(io_string)
    for column in csv.reader(io_string, delimiter=','):
        _created = csvdata.objects.update_or_create(
            Country=column[0],
            Category=column[1],
            Technology=column[2],
            Location=column[3],
            Experience=column[4],
            Company=column[5],
            Salary=column[6],
            Opening=column[7],
            Exp=column[8],
            Sal=column[9],

            Month=column[10],
            Date=column[11]
        )
    context = {}

    cnt = csvdata.objects.all().count()

    context = {'count': cnt}
    messages.success(request, 'File upload successful')
    return render(request, template, context)


# Function for dropdown
def technology(request):
     Category = category.objects.all()

     return render(request, "myapp/visual.html",{'Category':Category})


# Function for images

def visual(request):
    return render(request, 'myapp/visual.html')


# Function for image
def Technology(request):
    technology = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Technology.csv')
    technology_table = technology.to_html(classes="table table-striped")

    technology_detail = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\bispenv\Django-CRM-master\media\\opportunity-data\\detailview.csv')
    technology_detail.drop(technology_detail.columns[technology_detail.columns.str.contains('unnamed', case=False)], axis=1, inplace=True)
    technology_html=technology_detail.drop(['Exp', 'Sal','Month','Opening'], axis=1)

    technologydetail = technology_html.to_html(classes="table table-striped")

    return render(request, 'myapp/technology.html', {'technology_table':technology_table,'technologydetail':technologydetail})


# Function for image
def Location(request):
    location = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Location.csv')
    location_table = location.to_html(classes="table table-striped")
    location_detail = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\detailview.csv')
    location_detail.drop(location_detail.columns[location_detail.columns.str.contains('unnamed', case=False)], axis=1, inplace=True)
    location_html = location_detail.drop(['Exp', 'Sal', 'Month', 'Opening'], axis=1)

    locationdetail = location_html.to_html(classes="table table-striped")


    return render(request, 'myapp/location.html', {'location_table': location_table,'locationdetail': locationdetail})


# Function for image
def Experience(request):
    experience = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Experience.csv')
    experience_table = experience.to_html(classes="table table-striped")

    experience_detail = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\detailview.csv')
    experience_detail.drop(experience_detail.columns[experience_detail.columns.str.contains('unnamed', case=False)], axis=1, inplace=True)
    experience_html = experience_detail.drop(['Exp', 'Sal', 'Month', 'Opening'], axis=1)

    experiencedetail = experience_html.to_html(classes="table table-striped")

    return render(request, 'myapp/experience.html', {'experience_table': experience_table,'experiencedetail':experiencedetail})


# Function for image
def Company(request):
    company = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Company.csv')
    company_table =  company.to_html(classes="table table-striped")

    company_detail = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\detailview.csv')
    company_detail.drop(company_detail.columns[company_detail.columns.str.contains('unnamed', case=False)], axis=1, inplace=True)
    company_html = company_detail.drop(['Exp', 'Sal', 'Month', 'Opening'], axis=1)

    companydetail = company_html.to_html(classes="table table-striped")

    return render(request, 'myapp/company.html', {'company_table': company_table,'companydetail':companydetail})


# Function for image
def Salary(request):
    salary = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Salary.csv')
    salary_table = salary.to_html(classes="table table-striped")

    salary_detail = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\detailview.csv')
    salary_detail.drop(salary_detail.columns[salary_detail.columns.str.contains('unnamed', case=False)], axis=1, inplace=True)
    salary_html = salary_detail.drop(['Exp', 'Sal', 'Month', 'Opening'], axis=1)

    salarydetail = salary_html.to_html(classes="table table-striped")

    return render(request, 'myapp/salary.html', {'salary_table': salary_table,'salarydetail':salarydetail})


# Function for charts
def Categorylist():
    category_list = category.objects.all()
    category_list = read_frame(category_list)
    category_list = category_list['Category'].to_list()
    print('category_list')
    return category_list



@csrf_exempt
def dashboard(request, name):
    chartcontext = {}
    selected_option = request.POST['opportunity']
    selected_category = request.POST['category']
    country = request.session['country']
    current_month = datetime.now().strftime('%b')
    current_month = "Jan"
    chartcontext['selected_option'] = selected_option
    chartcontext['selected_category'] = selected_category
    chartcontext['current_month'] = current_month
    category_list = Categorylist()
    chartcontext['category_list'] = category_list
    print(chartcontext)



    if selected_option == 'opportunitys':
        complete_data = csvdata.objects.all()
        complete_data_df = read_frame(complete_data)
        #demandwise
        demandwise = complete_data_df.loc[(complete_data_df['Category'] == selected_category) & (complete_data_df['Country'] == country) & (complete_data_df['Month'] == current_month)]
        numberofopening = demandwise['Category'].count()
        chartcontext['numberofopening'] = numberofopening

        #Locationywise openings
        locationwise =  pd.pivot_table(complete_data_df.loc[(complete_data_df['Category'] == selected_category) & (complete_data_df['Country'] == country) & (complete_data_df['Month'] == current_month)], index='Location', values='Opening', aggfunc=np.sum, fill_value=0)
        locationwise = locationwise.reset_index()
        chartcontext['locationlist'] = locationwise['Location'].to_list()
        locationwiseopening = locationwise['Opening'].to_list()
        for i in range(0, len(locationwiseopening)):
            locationwiseopening[i] = int(locationwiseopening[i])
        chartcontext['locationopening'] = locationwiseopening
        number_of_colors = len(locationwiseopening)
        color1 = ['#'+''.join([random.choice('0123456789ABCDEF') for j in range(6)])
                  for i in range(number_of_colors)]
        colorslisting = color1

        chartcontext['colorlocation'] = colorslisting
        print(locationwiseopening)


    return render(request, 'myapp/home.html', chartcontext)





















@csrf_exempt
def dashboard__old(request,name):
    today = datetime.date.today()
    # months = ['zero', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
    #           'November', 'December']
    #
    # current_month = months[today.month-1]

    # months = [ 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October',
    #           'November', 'December']

    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'August', 'September', 'Oct','November', 'December']

    current_month = months[today.month - 2]

    category_name = category.objects.get(Category=name)
    Category = category.objects.all()
    categoryname = str(category_name)
    cname = category.objects.all()
    context = {'catagery_id': cname}
    category1 = csvdata.objects.all()
    df_category = read_frame(category1)
    country = request.session['country']

    data_frame = df_category.loc[(df_category['Category'] == categoryname) & (df_category['Country'] == country)  & (df_category['Month'] == current_month)]

    df = data_frame.drop(data_frame.columns[0], axis=1)

    csv_data = df.to_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\opportunity-data\\detailview.csv')

    # context = {'count': data_frame}
    # print(context)

    pivot_data = data_frame.pivot_table(index='Technology', values='Opening', aggfunc=np.sum, fill_value=0)
    table = pivot_data.to_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\opportunity-data\\Technology.csv')
    grouped = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Technology.csv')


    source = ColumnDataSource(pd.DataFrame(grouped))
    Technologies = source.data['Technology'].tolist()
    # colorList = Blues[len(Technologies)]
    p = figure(x_range=Technologies,plot_width=530, plot_height=400,
               tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'], toolbar_location="below")

    hover = HoverTool()

    hover.tooltips = [("Openings", "@Opening")]
    hover.mode = 'vline'

    p.add_tools(hover)
    labels = LabelSet(x='Technology', y='Opening', text='Opening', level='glyph',
                      x_offset=-13.5, y_offset=0, source=source, render_mode='canvas',  text_color="black")
    p.add_layout(labels)
    vbar = p.vbar(x='Technology', top='Opening', source=source,color='#756bb1', width=0.5,)

    line = p.line(x='Technology', y='Opening', source=source, color='#756bb1', alpha=1,line_width=4)

    line.visible = False
    # curdoc().theme = 'dark_minimal'
    # p.legend.click_policy = "hide"
    p.title.text =  categoryname+' '+"opening in"+' '+current_month+" on the basis of Technology"
    p.title.align = "center"
    p.title.text_color = "black"
    p.title.text_font_size = "15px"
    p.outline_line_width = 7
    p.toolbar.logo = None

    # p.toolbar_location = None
    # p.outline_line_alpha = 0.3
    # p.outline_line_color = "navy"
    p.xaxis.axis_label = 'Technology'

    p.xaxis.axis_label_text_font_size = "15pt"

    p.xaxis.axis_label_text_font = "times"
    p.xaxis.axis_label_text_color = "black"
    p.yaxis.axis_label = 'No of opening'
    p.yaxis.axis_label_text_font_size = "15pt"

    p.yaxis.axis_label_text_font = "times"
    p.yaxis.axis_label_text_color = "black"





    radiogroup = RadioButtonGroup(labels=["Bar", "Line"], active=0)


    radiogroup.callback = CustomJS(args=dict(vbar=vbar, line=line), code="""


        vbar.visible = false;

        line.visible = false;



        if (cb_obj.active == 0)

            vbar.visible = true;

        else if (cb_obj.active == 1)
            line.visible = true;




             """)

    # export_png(p, filename="media/opportunity-data/Technology.png")
    layout = column(p, radiogroup,width=300, )
    script, div = components(layout)

    # Location

    pivot_data1 = data_frame.pivot_table(index='Location', columns='Technology', values='Opening', aggfunc=np.sum,
                                         fill_value=0)

    table1 = pivot_data1.to_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Location.csv')
    dataframe1 = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Location.csv')
    # print(dataframe1)
    # loc = dataframe1['Location']


    print(dataframe1)
    print("reddy")
    # list(v_pivotedTable.head(2))
    print("ROCKY")


    ys1 = list(dataframe1.keys())
    ys1.remove('Location')

    courseCount = len(ys1)
    number_of_colors = courseCount
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    colorList = color
    TOOLTIPS = [("Location", "@Location")]
    source1 = ColumnDataSource(pd.DataFrame(dataframe1))

    locations = source1.data['Location'].tolist()

    p1 = figure(x_range=locations, plot_width=530, plot_height=400, tools=['save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                tooltips=TOOLTIPS, toolbar_location="below")

    # colorList = Spectral6[len(ys1)]

    stacked = p1.vbar_stack(stackers=ys1, x='Location', color=colorList, source=source1, legend=[value(x) for x in ys1],
                            name=ys1,
                            width=0.5, )

    for r in stacked:
        tec = r.name
        hover = HoverTool(tooltips=[
            ("%s " % tec, "@%s" % tec),

        ], renderers=[r])
        p1.add_tools(hover)

    labels = []
    bars = []
    for y, offset, color in zip(ys1, [-0.25, 0, 0.25], colorList):
        # labels = LabelSet(x=dodge('Location', offset, range=p1.x_range), y=y, text=y, source=source1,
        #                   text_align='center')
        bar = p1.vbar(x=dodge('Location', offset, range=p1.x_range), top=y, width=0.2, source=source1, color=color)
        bar.visible = False
        # p1.add_layout(labels)
        bars.append(bar)

        TOOLTIPS.append((y, "@" + y))

    radiogroup = RadioButtonGroup(labels=["StackedBar", "Bar"], active=0, )
    radiogroup.callback = CustomJS(args=dict(stacked=stacked, bars=bars), code="""
        for (i in stacked)
            stacked[i].visible = false;

        for (i in bars)
            bars[i].visible = false;

        if (cb_obj.active == 0)
            for (i in stacked)
                stacked[i].visible = true;
        else if (cb_obj.active == 1)
            for (i in bars)
                bars[i].visible = true; """)

    p1.legend.orientation = "horizontal"
    p1.title.text = categoryname+' '+"opening in"+' '+current_month+" on the basis of Location"
    p1.title.align = "center"
    p1.title.text_color = "black"
    p1.title.text_font_size = "15px"
    p1.outline_line_width = 7
    p1.toolbar.logo = None

    # p.toolbar_location = None
    # p.outline_line_alpha = 0.3
    # p.outline_line_color = "navy"
    p1.xaxis.axis_label = 'Location'
    p1.xaxis.axis_label_text_font_size = "15pt"

    p1.xaxis.axis_label_text_font = "times"
    p1.xaxis.axis_label_text_color = "black"
    p1.yaxis.axis_label = 'No of opening'
    p1.yaxis.axis_label_text_font_size = "15pt"

    p1.yaxis.axis_label_text_font = "times"
    p1.yaxis.axis_label_text_color = "black"


    layout1 = column(p1, radiogroup, width=300, )

    script1, div1 = components(layout1)

    # Experience

    pivot_data2 = data_frame.pivot_table(index='Experience', columns='Technology', values='Opening', aggfunc=np.sum,
                                         fill_value=0)
    pd_f = pivot_data2.reindex(['1 to 3', '3 to 8', '8 to 12', '12 to 18'])
    table3 = pd_f.to_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Experience.csv')
    dataframe3 = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Experience.csv')

    ys3 = list(dataframe3.keys())
    ys3.remove('Experience')
    courseCount = len(ys3)
    number_of_colors = courseCount
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    colorList = color
    TOOLTIPS = [("Experience", "@Experience")]
    source3 = ColumnDataSource(pd.DataFrame(dataframe3))

    exp = source3.data['Experience'].tolist()

    p3 = figure(x_range=exp, plot_width=530, plot_height=400,
                tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                tooltips=TOOLTIPS, toolbar_location="below")

    # colorList =Spectral6[len(ys3)]

    stacked = p3.vbar_stack(stackers=ys3, x='Experience', color=colorList, source=source3,
                            legend=[value(x) for x in ys3],
                            name=ys3,
                            width=0.5, )

    for r in stacked:
        tec = r.name
        hover = HoverTool(tooltips=[
            ("%s " % tec, "@%s" % tec),

        ], renderers=[r])
        p3.add_tools(hover)

    labels = []
    bars = []
    for y, offset, color in zip(ys3, [-0.25, 0, 0.25], colorList):
        bar = p3.vbar(x=dodge('Experience', offset, range=p3.x_range), top=y, width=0.2, source=source3, color=color)
        bar.visible = False
        bars.append(bar)
        TOOLTIPS.append((y, "@" + y))

    radiogroup = RadioButtonGroup(labels=["StackedBar", "Bar"], active=0, )
    radiogroup.callback = CustomJS(args=dict(stacked=stacked, bars=bars), code="""
            for (i in stacked)
                stacked[i].visible = false;

            for (i in bars)
                bars[i].visible = false;

            if (cb_obj.active == 0)
                for (i in stacked)
                    stacked[i].visible = true;
            else if (cb_obj.active == 1)
                for (i in bars)
                    bars[i].visible = true; """)

    p3.legend.orientation = "horizontal"
    p3.title.text = categoryname+' '+"opening in"+' '+current_month+" on the basis of Experience"
    p3.title.align = "center"
    p3.title.text_color = "black"
    p3.title.text_font_size = "15px"
    p3.outline_line_width = 7
    p3.toolbar.logo = None

    # p.toolbar_location = None
    # p.outline_line_alpha = 0.3
    # p.outline_line_color = "navy"
    p3.xaxis.axis_label = 'Experience In year'
    p3.xaxis.axis_label_text_font_size = "15pt"

    p3.xaxis.axis_label_text_font = "times"
    p3.xaxis.axis_label_text_color = "black"
    p3.yaxis.axis_label = 'No of opening'

    p3.yaxis.axis_label_text_font_size = "15pt"
    p3.yaxis.axis_label_text_font = "times"
    p3.yaxis.axis_label_text_color = "black"


    layout3 = column(p3, radiogroup, width=300, )

    script3, div3 = components(layout3)

    # Company

    pivot_data4 = data_frame.pivot_table(index='Company', columns='Technology', values='Opening', aggfunc=np.sum,
                                         fill_value=0)
    table4 = pivot_data4.to_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Company.csv')
    dataframe4 = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Company.csv')

    com = dataframe4['Company']
    ys4 = list(dataframe4.keys())
    ys4.remove('Company')
    courseCount = len(ys4)
    number_of_colors = courseCount
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    colorList = color
    TOOLTIPS = [("Company", "@Company")]
    source4 = ColumnDataSource(pd.DataFrame(dataframe4))

    company = source4.data['Company'].tolist()

    p4 = figure(x_range=company, plot_width=530, plot_height=400,
                tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                tooltips=TOOLTIPS, toolbar_location="below")

    # colorList =Spectral6[len(ys4)]

    stacked = p4.vbar_stack(stackers=ys4, x='Company', color=colorList, source=source4, legend=[value(x) for x in ys4],
                            name=ys4,
                            width=0.5, )

    for r in stacked:
        tec = r.name
        hover = HoverTool(tooltips=[
            ("%s " % tec, "@%s" % tec),

        ], renderers=[r])
        p4.add_tools(hover)

    labels = []
    bars = []
    for y, offset, color in zip(ys4, [-0.25, 0, 0.25], colorList):
        bar = p4.vbar(x=dodge('Company', offset, range=p4.x_range), top=y, width=0.2, source=source4, color=color)
        bar.visible = False
        bars.append(bar)
        TOOLTIPS.append((y, "@" + y))

    radiogroup = RadioButtonGroup(labels=["StackedBar", "Bar"], active=0, )
    radiogroup.callback = CustomJS(args=dict(stacked=stacked, bars=bars), code="""
                for (i in stacked)
                    stacked[i].visible = false;

                for (i in bars)
                    bars[i].visible = false;

                if (cb_obj.active == 0)
                    for (i in stacked)
                        stacked[i].visible = true;
                else if (cb_obj.active == 1)
                    for (i in bars)
                        bars[i].visible = true; """)

    p4.legend.orientation = "horizontal"
    p4.title.text = categoryname+' '+"opening in"+' '+current_month+" on the basis of Company"
    p4.title.align = "center"
    p4.title.text_color = "black"
    p4.title.text_font_size = "15px"
    p4.outline_line_width = 7
    p4.toolbar.logo = None

    # p.toolbar_location = None
    # p.outline_line_alpha = 0.3
    # p.outline_line_color = "navy"
    p4.xaxis.axis_label = 'Company'
    p4.xaxis.axis_label_text_font_size = "15pt"

    p4.xaxis.axis_label_text_font = "times"
    p4.xaxis.axis_label_text_color = "black"
    p4.yaxis.axis_label = 'No of opening'

    p4.yaxis.axis_label_text_font_size = "15pt"
    p4.yaxis.axis_label_text_font = "times"
    p4.yaxis.axis_label_text_color = "black"


    layout4 = column(p4, radiogroup, width=300, )

    script4, div4 = components(layout4)

    # Salary

    pivot_data5 = data_frame.pivot_table(index='Salary', columns='Technology', values='Opening', aggfunc=np.sum,
                                          fill_value=0)
    if country == 'India':
        pivot_data5 = pivot_data5.reindex(['1 to 4 L', '4 to 8 L', '8 to 12 L', '12 to 20 L'])
    else:
        pivot_data5 = pivot_data5.reindex(
            ['6k to 10k $', '10k to 16k $', '20k to 30k $', '35k to 50k $'])

    table5 = pivot_data5.to_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Salary.csv')
    dataframe5 = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\\opportunity-data\\Salary.csv')

    source5 = ColumnDataSource(dataframe5)
    sal = dataframe5['Salary']

    ys5 = list(dataframe5.keys())
    ys5.remove('Salary')
    courseCount = len(ys5)
    number_of_colors = courseCount
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    colorList = color

    TOOLTIPS = [("Salary", "@Salary")]
    source5 = ColumnDataSource(pd.DataFrame(dataframe5))

    salary = source5.data['Salary'].tolist()

    p5 = figure(x_range=salary, plot_width=530, plot_height=400,
                tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                tooltips=TOOLTIPS, toolbar_location="below")

    # colorList = Spectral6[len(ys5)]

    stacked = p5.vbar_stack(stackers=ys5, x='Salary', color=colorList, source=source5, legend=[value(x) for x in ys5],
                            name=ys5,
                            width=0.5, )

    for r in stacked:
        tec = r.name
        hover = HoverTool(tooltips=[
            ("%s " % tec, "@%s" % tec),

        ], renderers=[r])
        p5.add_tools(hover)

    labels = []
    bars = []
    for y, offset, color in zip(ys5, [-0.25, 0, 0.25], colorList):
        bar = p5.vbar(x=dodge('Salary', offset, range=p5.x_range), top=y, width=0.2, source=source5, color=color)
        bar.visible = False
        bars.append(bar)
        TOOLTIPS.append((y, "@" + y))

    radiogroup = RadioButtonGroup(labels=["StackedBar", "Bar"], active=0, )
    radiogroup.callback = CustomJS(args=dict(stacked=stacked, bars=bars), code="""
                    for (i in stacked)
                        stacked[i].visible = false;

                    for (i in bars)
                        bars[i].visible = false;

                    if (cb_obj.active == 0)
                        for (i in stacked)
                            stacked[i].visible = true;
                    else if (cb_obj.active == 1)
                        for (i in bars)
                            bars[i].visible = true; """)

    p5.legend.orientation = "horizontal"
    p5.title.text =categoryname+' '+"opening in"+' '+current_month+" on the basis of Salary"
    p5.title.align = "center"
    p5.title.text_color = "black"
    p5.title.text_font_size = "15px"
    p5.outline_line_width = 7
    p5.toolbar.logo = None

    # p.toolbar_location = None
    # p.outline_line_alpha = 0.3
    # p.outline_line_color = "navy"
    if country == 'India':
        p5.xaxis.axis_label = 'Salary In Lac'
    else:
         p5.xaxis.axis_label = 'Salary In USD'

    p5.xaxis.axis_label_text_font_size = "15pt"

    p5.xaxis.axis_label_text_font = "times"
    p5.xaxis.axis_label_text_color = "black"
    p5.yaxis.axis_label = 'No of opening'
    p5.yaxis.axis_label_text_font_size = "15pt"

    p5.yaxis.axis_label_text_font = "times"
    p5.yaxis.axis_label_text_color = "black"

    layout5 = column(p5, radiogroup, width=300, )
    script5, div5 = components(layout5)

    html = render_to_string("myapp/dropdown.html",
                            {'script': script, 'div': div, 'script1': script1, 'div1': div1, 'script3': script3,
                             'div3': div3, 'script4': script4, 'div4': div4, 'script5': script5, 'div5': div5,'category_name':category_name,'Category':Category })
    return HttpResponse(html)




# class For GeneratePdf
class GeneratePdf(View):
    def get(self, request, *args, **kwargs):
        template = get_template('myapp/pdf.html')
        pdf = render_to_pdf('myapp/pdf.html')
        if pdf:
            response = HttpResponse(pdf, content_type='application/pdf')
            filename = "Opening In May_%s.pdf" % ("12341231")
            content = "inline; filename='%s'" % (filename)
            download = request.GET.get("download")

            if download:
                content = "attachment; filename='%s'" % (filename)
            response['Content-Disposition'] = content
            return response
        return HttpResponse("Not found")


