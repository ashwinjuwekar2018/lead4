from celery.schedules import crontab
from celery.task import task, periodic_task
from django.core.mail import EmailMessage
from django.shortcuts import reverse
from django.template.loader import render_to_string

from common.models import Comment, User


@task
def send_email_to_new_user(user_email,username,password, created_by, domain='localhost:8000', protocol='http'):
    """ Send Mail To Users When their account is created """
    if user_email:
        context = {}
        context["user_email"] = user_email
        context["username"] = username
        context["password"] = password
        context["created_by"] = created_by
        context["url"] = protocol + '://' + domain + '/'
        recipients = []
        recipients.append(user_email)
        subject = 'Created account in LMS'
        html_content = render_to_string('user_status_in.html', context=context)
        if recipients:
            msg = EmailMessage(
                subject,
                html_content,
                to=recipients
            )
            msg.content_subtype = "html"
            msg.send()


@task
def send_email_user_mentions(comment_id, called_from, domain='localhost:8000', protocol='http'):
    """ Send Mail To Mentioned Users In The Comment """
    comment = Comment.objects.filter(id=comment_id).first()
    if comment:
        comment_text = comment.comment
        comment_text_list = comment_text.split()
        recipients = []
        for comment_text in comment_text_list:
            if comment_text.startswith('@'):
                if comment_text.strip('@').strip(',') not in recipients:
                    if User.objects.filter(username=comment_text.strip('@').strip(',')).exists():
                        email = User.objects.filter(
                            username=comment_text.strip('@').strip(',')).first().email
                        recipients.append(email)

        context = {}
        context["name"] = comment.commented_by
        context["comment_description"] = comment.comment


        html_content = render_to_string('comment_email.html', context=context)
        subject = 'BISP LMS : comment '
        if recipients:
            msg = EmailMessage(
                subject,
                html_content,
                from_email=comment.commented_by.email,
                to=recipients
            )
            msg.content_subtype = "html"
            msg.send()


@task
def send_email_user_status(user_id, domain='localhost:8000', protocol='http'):
    """ Send Mail To Users Regarding their status i.e active or inactive """
    user = User.objects.filter(id=user_id).first()
    if user:
        context = {}
        context["message"] = 'deactivated'
        if user.is_active:
            context["url"] = protocol + '://' + domain + '/'
            context["message"] = 'activated'
        html_content = render_to_string('user_status.html', context=context)
        subject = 'LMS : User Status '
        recipients = []
        recipients.append(user.email)
        if recipients:
            msg = EmailMessage(
                subject,
                html_content,
                to=recipients
            )
            msg.content_subtype = "html"
            msg.send()

@task
def send_email_user_delete(user_email, domain='localhost:8000', protocol='http'):
    """ Send Mail To Users When their account is deleted """
    if user_email:
        context = {}
        context["message"] = 'deleted'
        recipients = []
        recipients.append(user_email)
        subject = 'LMS : User Deleted. '
        html_content = render_to_string('user_status.html', context=context)
        if recipients:
            msg = EmailMessage(
                subject,
                html_content,
                to=recipients
            )
            msg.content_subtype = "html"
            msg.send()
