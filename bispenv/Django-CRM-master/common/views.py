import base64
import os
import json
import requests
import datetime
import pandas as pd
import numpy as np
from colour import Color
from datetime import date, timedelta
import datetime as dt
import random
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, authenticate, login
from django.db.models import Q
from django.core.mail import EmailMessage
from django.contrib.auth.hashers import check_password
from django.contrib.auth.mixins import LoginRequiredMixin, AccessMixin
from django.http import (HttpResponseRedirect,
                         JsonResponse, HttpResponse,
                         Http404, request)
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import (
    CreateView, UpdateView, DetailView, TemplateView, View, DeleteView)
from common.models import (User, Document, Attachments,Comment, APISettings,Google)
from common.forms import (
    UserForm, LoginForm,
    ChangePasswordForm, PasswordResetEmailForm,
    DocumentForm, UserCommentForm,
    APISettingsForm
)
from django.contrib.auth.views import PasswordResetView
from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse_lazy, reverse
from django.conf import settings

from django.template.loader import render_to_string
from django.core.exceptions import PermissionDenied
import boto3
import botocore

from common.utils import ROLES
from common.tasks import send_email_user_status, send_email_user_delete, send_email_to_new_user
from marketing.models import  Contact,ContactList,Event,Contactlinkedin
import json
import ast

from pandas import Series


def handler404(request, exception):
    return render(request, '404.html', status=404)


def handler500(request):
    return render(request, '500.html', status=500)


class AdminRequiredMixin(AccessMixin):

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        self.raise_exception = True
        if not request.user.role == "ADMIN":
            if not request.user.is_superuser:
                return self.handle_no_permission()
        return super(AdminRequiredMixin, self).dispatch(
            request, *args, **kwargs)

@login_required(login_url='/login')
@csrf_exempt
def HomeView(request):
    #Massaging the data
    # var_readcsv = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\csv_of_leads_final.csv')
    # var_readcsv.dropna(how="all", inplace=True)
    # var_readcsv['Year'] = pd.DatetimeIndex(var_readcsv['Date']).year
    # var_readcsv['MonthNumber'] = pd.DatetimeIndex(var_readcsv['Date']).month
    # var_readcsv['Month'] = pd.DatetimeIndex(var_readcsv['Date']).month
    # var_readcsv['Weeknumber'] = pd.DatetimeIndex(var_readcsv['Date']).week
    # var_readcsv['Datenumber'] = pd.DatetimeIndex(var_readcsv['Date']).day
    # var_readcsv['Lead'] = 1
    # look_up2 = {1: 'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct',11: 'Nov', 12: 'Dec'}
    # var_readcsv['Month'] = var_readcsv['Month'].apply(lambda x: look_up2[x])
    # updated_csv = var_readcsv.dropna(axis=0, how='any')
    # updated_csv.to_csv('media/csv_of_leads_final.csv', mode='w', index=False)



    #here is the last n days leads are being fetched
    if request.method == 'POST':
        tabname = request.POST['tabname']
        criteria = request.POST['criteria']
        vc=request.POST['varience_check2']
        if criteria=='MTD':
            previous_date = dt.datetime.today().strftime("%Y-%m-01")
            ttext = 'MTD'
        if criteria == 'YTD':
            previous_date = dt.datetime.today().strftime("%Y-01-01")
            ttext = 'YTD'
        if criteria == 'QTD':
            currentdate = dt.datetime.today()
            quarter_start = pd.to_datetime(currentdate - pd.tseries.offsets.QuarterBegin(startingMonth=1)).date()
            previous_date = quarter_start.strftime('%Y-%m-%d')           #this is for getting the current quarter current date
            ttext = 'QTD'
        if criteria == 'HTD':
            previous_date = '2015-01-01'
            ttext = 'HTD'
        if criteria == '90':
            s_todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
            backdate = date.today() - timedelta(90)
            previous_date = backdate.strftime("%Y-%m-%d")
            ttext = 'Weekwise'
        if criteria == '60':
            s_todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
            backdate = date.today() - timedelta(60)
            previous_date = backdate.strftime("%Y-%m-%d")
            ttext = 'Weekwise'
        if criteria == '30':
            s_todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
            backdate = date.today() - timedelta(30)
            previous_date = backdate.strftime("%Y-%m-%d")
            ttext = 'Weekwise'
        if criteria == '15':
            s_todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
            backdate = date.today() - timedelta(15)
            previous_date = backdate.strftime("%Y-%m-%d")
            ttext = 'Weekwise'
    else:
        criteria = 'YTD'
        previous_date = dt.datetime.today().strftime("%Y-01-01")
        ttext = 'YTD'
        vc='off'
        tabname='gener'

    criterialist = ['HTD','YTD','QTD','MTD']
    criterialist2 = ['90','60','30','15']
    contextfirst = {}
    contextfirst['criterialist'] = criterialist
    contextfirst['criterialist2'] = criterialist2
    contextfirst['criteria'] = criteria
    contextfirst['tabsname'] = tabname
    print(contextfirst['tabsname'])

    if (vc=='on'):
        contextfirst['varience_checked']='on'
        a = getComperison(criteria,contextfirst)

        # return render(request, 'LeadApplication/homepage_old.html', a)
    if (vc == 'off'):
        contextfirst['varience_checked']='off'

    var_readcsv = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv',usecols = ["Technology","Date","Status","Lead","Weeknumber","Year","Month"])
    totalcontacts = var_readcsv.sum()['Lead']
    totalconverted = var_readcsv[var_readcsv["Status"]=='Converted'].count()["Lead"]
    convertedpercent = (totalconverted*100)/totalcontacts
    convertedpercent = "{:.2f}".format(convertedpercent)
    contextfirst['totalconverted'] = totalconverted
    contextfirst['convertedpercent'] = convertedpercent

    #Now first of all we need to find out top course with its enquiry count this current month we already have the month name
    currentmonthname = dt.datetime.today().strftime("%b")
    currentyear = pd.datetime.now().year                      #now we have the current year also
    # var_readcsv = var_readcsv[(var_readcsv['Month'] == 'Jan') & (var_readcsv['Year'] == currentyear)]
    var_readcsv = var_readcsv[(var_readcsv['Year'] == currentyear)]
    pivoted_df = pd.pivot_table(var_readcsv,index='Technology',values='Lead',aggfunc=np.sum,fill_value=0)
    pivoted_df = pivoted_df.reset_index()
    pivoted_df = pivoted_df.nlargest(1, 'Lead')
    topsubject = pivoted_df['Technology'].to_list()
    topsubjectcount = pivoted_df['Lead'].to_list()

    contextfirst['topsubject'] = topsubject[0]
    contextfirst['topsubjectcount'] = topsubjectcount[0]





    # last_x_days = 90
    todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
    # backdate = date.today() - timedelta(last_x_days)
    # previous_date = backdate.strftime("%Y-%m-%d")
    var_readcsv =pd.pivot_table(var_readcsv,index=['Date','Weeknumber','Year','Month'],values='Lead',aggfunc=np.sum,fill_value=0)
    var_readcsv = var_readcsv.reset_index()
    var_readcsv = var_readcsv[(var_readcsv['Date'] >= previous_date) & (var_readcsv['Date'] < todaysdate)]


    #calculate this month leads
    month_var_readcsv = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv',usecols=["Date", "Lead", "Weeknumber", "Year", "Month"])
    r_todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
    rprevious_date = dt.datetime.today().strftime("%Y-%m-01")
    month_var_readcsv=pd.pivot_table(month_var_readcsv,index=['Date','Weeknumber','Year','Month'],values='Lead',aggfunc=np.sum,fill_value=0)
    month_var_readcsv = month_var_readcsv.reset_index()
    month_var_readcsv = month_var_readcsv[(month_var_readcsv['Date'] >= rprevious_date) & (month_var_readcsv['Date'] <= r_todaysdate)]
    weektotallistmonth = month_var_readcsv.groupby("Month").Lead.sum().reset_index().sort_values("Month", ascending=True)

    thismonthleads = weektotallistmonth.sum()['Lead']


    #=============================================================

    if criteria=='HTD':
        weektotallist = var_readcsv.groupby("Year").Lead.sum().reset_index().sort_values("Year", ascending=True)
        labels = weektotallist['Year'].tolist()
        labeltitle = "Year "

    if criteria=='QTD':
        weektotallist = var_readcsv.groupby("Weeknumber").Lead.sum().reset_index().sort_values("Weeknumber",ascending=False)
        labels = weektotallist['Weeknumber'].tolist()
        labeltitle = "Week #"

    if criteria=='MTD':
        weektotallist = var_readcsv.groupby("Month").Lead.sum().reset_index().sort_values("Month",ascending=True)
        labels = weektotallist['Month'].tolist()
        labeltitle = "Month "

    if criteria=='YTD':
        weektotallist = var_readcsv.groupby("Month").Lead.sum().reset_index().sort_values("Month",ascending=False)
        labels = weektotallist['Month'].tolist()
        labeltitle = " "

    if criteria== '90':
        weektotallist = var_readcsv.groupby("Weeknumber").Lead.sum().reset_index().sort_values("Weeknumber",ascending=False)
        labels = weektotallist['Weeknumber'].tolist()
        labeltitle = "Week #"

    if criteria== '60':
        weektotallist = var_readcsv.groupby("Weeknumber").Lead.sum().reset_index().sort_values("Weeknumber",ascending=False)
        labels = weektotallist['Weeknumber'].tolist()
        labeltitle = "Week #"

    if criteria== '30':
        weektotallist = var_readcsv.groupby("Weeknumber").Lead.sum().reset_index().sort_values("Weeknumber",ascending=False)
        labels = weektotallist['Weeknumber'].tolist()
        labeltitle = "Week #"

    if criteria== '15':
        weektotallist = var_readcsv.groupby("Weeknumber").Lead.sum().reset_index().sort_values("Weeknumber",ascending=False)
        labels = weektotallist['Weeknumber'].tolist()
        labeltitle = "Week #"


    data = weektotallist['Lead'].tolist()
    colorlistlength = len(labels)
    number_of_colors = colorlistlength
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    colorList = color
    begindate = pd.to_datetime(previous_date)
    enddate = pd.to_datetime(todaysdate)
    diffdays = enddate - begindate
    last_x_days = diffdays.days

    contextfirst['labeltitle'] = labeltitle

    contextfirst['labels11'] = labels
    contextfirst['data11'] = data
    contextfirst['colors11'] = colorList
    contextfirst['totalleads'] = totalcontacts
    contextfirst['currentmonthname'] = currentmonthname
    contextfirst['thismonthleads'] = thismonthleads
    contextfirst['last_x_days'] = last_x_days
    contextfirst['ttext'] = ttext
    contextfirst['firstview'] = 'firstview'


    #############################Now we want leads by sources
    var_readcsv_source = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv',usecols = ["Source","Date", "Lead","Weeknumber"])
    s_todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
    # backdate = date.today() - timedelta(last_s_days)
    # s_previous_date = backdate.strftime("%Y-%m-%d")
    s_previous_date = previous_date
    begindate = pd.to_datetime(s_previous_date)
    enddate = pd.to_datetime(s_todaysdate)
    diffdays= enddate-begindate
    last_x_days = diffdays.days
    var_readcsv_source = pd.pivot_table(var_readcsv_source, index=['Date', 'Source', 'Weeknumber'], values='Lead', aggfunc=np.sum, fill_value=0)
    var_readcsv_source = var_readcsv_source.reset_index()
    var_readcsv_source = var_readcsv_source[(var_readcsv_source['Date'] >= s_previous_date) & (var_readcsv_source['Date'] < s_todaysdate)]
    # weektotallist2 = var_readcsv_source.groupby(["Weeknumber"]).Lead.sum().reset_index().sort_values("Weeknumber", ascending=False)
    sourcetotallist2 = var_readcsv_source.groupby(["Source"]).Lead.sum().reset_index().sort_values("Source",ascending=False)
    sourcetotallist2.reset_index()
    # weektotallist2.reset_index()
    labels2 = sourcetotallist2['Source'].unique().tolist()
    data2 = sourcetotallist2['Lead'].tolist()
    contextfirst['labels22'] = labels2
    contextfirst['data22'] = data2
    contextfirst['last_s_days'] = last_x_days

    #/////////////////Now we need top 5 Cities with top courses
    fun_readcsv2 = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv',usecols = ["Location","Source","Date", "Lead","Weeknumber"])
    s_todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
    # last_x_days = n_days2
    # last_x_days = int(last_x_days)
    # backdate = date.today() - timedelta(last_x_days)
    # s_previous_date = backdate.strftime("%Y-%m-%d")
    s_previous_date = previous_date
    var_readcsv_cities = pd.pivot_table(fun_readcsv2, index=['Date', 'Location', 'Weeknumber'], values='Lead',aggfunc=np.sum, fill_value=0)
    var_readcsv_cities = var_readcsv_cities.reset_index()
    var_readcsv_cities = var_readcsv_cities[(var_readcsv_cities['Date'] >= s_previous_date) & (var_readcsv_cities['Date'] < s_todaysdate)]
    var_readcsv_cities = pd.pivot_table(var_readcsv_cities, index=['Location'], values='Lead', aggfunc=np.sum,fill_value=0)
    var_largest = var_readcsv_cities.nlargest(5,'Lead')
    # fun_readcsv2 = fun_readcsv2.pivot_table(index='Location', values='Lead', aggfunc=np.sum, fill_value=0)

    var_largest = var_largest.reset_index()
    labels1 = var_largest['Location'].tolist()
    labels1 = list(labels1)
    data1 = var_largest['Lead'].tolist()
    colorlistlength1 = len(labels1)
    number_of_colors1 = colorlistlength1
    color1 = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
              for i in range(number_of_colors1)]
    colorList1 = color1
    contextfirst['labels_city'] = labels1
    contextfirst['data_city'] = data1
    contextfirst['colors_city'] = colorList1

    # /////////////////Now we need top 5 Countries with top courses
    fun_readcsv2 = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
    s_todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
    # last_x_days = n_days2
    # last_x_days = int(last_x_days)
    # backdate = date.today() - timedelta(last_x_days)
    # s_previous_date = backdate.strftime("%Y-%m-%d")
    s_previous_date = previous_date
    var_readcsv_cities = pd.pivot_table(fun_readcsv2, index=['Date', 'Country', 'Weeknumber'], values='Lead',aggfunc=np.sum, fill_value=0)
    var_readcsv_cities = var_readcsv_cities.reset_index()
    var_readcsv_cities = var_readcsv_cities[(var_readcsv_cities['Date'] >= s_previous_date) & (var_readcsv_cities['Date'] < s_todaysdate)]
    var_readcsv_cities = pd.pivot_table(var_readcsv_cities, index=['Country'], values='Lead', aggfunc=np.sum,fill_value=0)
    var_largest = var_readcsv_cities.nlargest(5, 'Lead')
    # fun_readcsv2 = fun_readcsv2.pivot_table(index='Location', values='Lead', aggfunc=np.sum, fill_value=0)
    # var_largest = fun_readcsv2.nlargest(5, 'Lead')

    var_largest = var_largest.reset_index()
    labels1 = var_largest['Country'].tolist()
    labels1 = list(labels1)
    data1 = var_largest['Lead'].tolist()
    colorlistlength1 = len(labels1)
    number_of_colors1 = colorlistlength1
    color1 = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
              for i in range(number_of_colors1)]
    colorList1 = color1
    contextfirst['labels_country'] = labels1
    contextfirst['data_country'] = data1
    contextfirst['colors_country'] = colorList1

    #///////////////////// for the internet Stacked bar very important

    var_readcsv_country = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv',usecols=["Country", "Date", "Lead", "Weeknumber","Month","Year"])
    s_todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
    s_previous_date = previous_date
    begindate = pd.to_datetime(s_previous_date)
    enddate = pd.to_datetime(s_todaysdate)
    diffdays = enddate - begindate
    last_x_days = diffdays.days
    var_readcsv_country = pd.pivot_table(var_readcsv_country, index=['Date', 'Country', 'Weeknumber','Month','Year'], values='Lead',aggfunc=np.sum, fill_value=0)
    var_readcsv_country = var_readcsv_country.reset_index()
    var_readcsv_country = var_readcsv_country[(var_readcsv_country['Date'] >= s_previous_date) & (var_readcsv_country['Date'] <= s_todaysdate)]

    if criteria == 'MTD':
        weektotallist = var_readcsv_country.groupby(["Month", "Country"]).Lead.sum().reset_index().sort_values("Month",ascending=True)
        weeklisting = weektotallist['Month'].unique().tolist()

    if criteria == 'YTD':
        weektotallist = var_readcsv_country.groupby(["Month", "Country"]).Lead.sum().reset_index().sort_values("Month", ascending=False)
        weeklisting = weektotallist['Month'].unique().tolist()
    if criteria == 'HTD':
        weektotallist = var_readcsv_country.groupby(["Year", "Country"]).Lead.sum().reset_index().sort_values("Year", ascending=True)
        weeklisting = weektotallist['Year'].unique().tolist()
    if criteria == 'QTD':
        weektotallist = var_readcsv_country.groupby(["Weeknumber", "Country"]).Lead.sum().reset_index().sort_values("Weeknumber", ascending=True)
        weeklisting = weektotallist['Weeknumber'].unique().tolist()
    if criteria == '90':
        weektotallist = var_readcsv_country.groupby(["Weeknumber", "Country"]).Lead.sum().reset_index().sort_values("Weeknumber", ascending=False)
        weeklisting = weektotallist['Weeknumber'].unique().tolist()
    if criteria == '60':
        weektotallist = var_readcsv_country.groupby(["Weeknumber", "Country"]).Lead.sum().reset_index().sort_values("Weeknumber", ascending=False)
        weeklisting = weektotallist['Weeknumber'].unique().tolist()
    if criteria == '30':
        weektotallist = var_readcsv_country.groupby(["Weeknumber", "Country"]).Lead.sum().reset_index().sort_values("Weeknumber", ascending=False)
        weeklisting = weektotallist['Weeknumber'].unique().tolist()
    if criteria == '15':
        weektotallist = var_readcsv_country.groupby(["Weeknumber", "Country"]).Lead.sum().reset_index().sort_values("Weeknumber", ascending=False)
        weeklisting = weektotallist['Weeknumber'].unique().tolist()


    # now below is the procedure of creating the list of list of all the countries corresponding to that year month week
    countrylist = weektotallist['Country'].unique().tolist()
    datalistingarray = []
    for c in countrylist:
        listings = []
        for w in weeklisting:
            if criteria == 'MTD':
                new_df = var_readcsv_country[((var_readcsv_country['Country'] == c) & (var_readcsv_country['Month'] == w))]
            if criteria == 'YTD':
                new_df = var_readcsv_country[((var_readcsv_country['Country'] == c) & (var_readcsv_country['Month'] == w))]
            if criteria == 'HTD':
                new_df = var_readcsv_country[((var_readcsv_country['Country'] == c) & (var_readcsv_country['Year'] == w))]
            if criteria == 'QTD':
                new_df = var_readcsv_country[((var_readcsv_country['Country'] == c) & (var_readcsv_country['Weeknumber'] == w))]
            if criteria == '90':
                new_df = var_readcsv_country[((var_readcsv_country['Country'] == c) & (var_readcsv_country['Weeknumber'] == w))]
            if criteria == '60':
                new_df = var_readcsv_country[((var_readcsv_country['Country'] == c) & (var_readcsv_country['Weeknumber'] == w))]
            if criteria == '30':
                new_df = var_readcsv_country[((var_readcsv_country['Country'] == c) & (var_readcsv_country['Weeknumber'] == w))]
            if criteria == '15':
                new_df = var_readcsv_country[((var_readcsv_country['Country'] == c) & (var_readcsv_country['Weeknumber'] == w))]


            sumoflead = new_df['Lead'].sum()

            if sumoflead == '':
                a = 0
            else:
                a = sumoflead

            listings.append(a)

        #here we have applied the cumsum formula
        listings = pd.Series(listings)
        listings = listings.cumsum()
        listings = list(listings)

        datalistingarray.append(listings)

    listlength = len(countrylist)
    mainlabels = weeklisting
    contextfirst['mainlabel'] = mainlabels     #this line is important
    contextfirst['countrylist'] = countrylist    #this line is also important
    contextsecond = []
    mystring = ''
    fromcolor = Color("#a3f1ff")
    colorslisting = list(fromcolor.range_to(Color("#ff2a79"), listlength))
    for i in range (0,listlength):
        ilabel = countrylist[i]
        idata = datalistingarray[i]
        # color1 = "#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
        color1 = colorslisting[i]
        # contextfirst[countrylist[i]] = {'type':'bar','label':ilabel,'data':idata,'backgroundColor':color1,'borderwidth':1}
        contextsecond.append({'type':'bar','label':ilabel,'data':idata,'backgroundColor':color1,'borderwidth':1})
    contextfirst['contextsecond'] = contextsecond           #this line is most important
    contextfirst['last_csx_days'] = last_x_days


    # return render(request,'LeadApplication/dashboard2.html',contextfirst)
    return render(request,'LeadApplication/homepage.html',contextfirst)
    # return render(request,'LeadApplication/homepage_old.html',contextfirst)


@csrf_exempt
def getComperison(criteria,contextfirst):
    fun_readcsv_multiline = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv',usecols=["Country", "Date", "Source", "Lead", "Weeknumber", "Month", "Year"])
    monthseriallist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    currentyear = pd.datetime.now().year
    lastyear = currentyear - 1
    contextfirst['current_year'] = currentyear
    contextfirst['last_year'] = lastyear
    contextfirst['x_axis_label'] = criteria
    if criteria == 'YTD':
        currentyeardata = pd.pivot_table(fun_readcsv_multiline.loc[fun_readcsv_multiline['Year'] == currentyear],columns=['Month'], values='Lead', aggfunc=np.sum)
        # here we get the monthwise
        monthlist = list(currentyeardata.columns)
        unorderedmonthlist = monthlist
        orderedmonthlist = sorted(unorderedmonthlist, key=lambda x: monthseriallist.index(x))
        currentyeardata = currentyeardata[orderedmonthlist]
        currentyearleadtotallist = currentyeardata.iloc[0].tolist()
        monthlists = monthlist
        monthlist = [value for value in monthseriallist if value in monthlists]
        current_monthnotavailable = set(monthseriallist).difference(monthlist)
        for ctr in current_monthnotavailable:
            if (ctr == 'Jan'):
                monthlist.insert(0, 'Jan')
                currentyearleadtotallist.insert(0, 0)
            elif (ctr == 'Feb'):
                monthlist.insert(1, 'Feb')
                currentyearleadtotallist.insert(1, 0)
            elif (ctr == 'Mar'):
                monthlist.insert(2, 'Mar')
                currentyearleadtotallist.insert(2, 0)
            elif (ctr == 'Apr'):
                monthlist.insert(3, 'Apr')
                currentyearleadtotallist.insert(3, 0)
            elif (ctr == 'May'):
                monthlist.insert(4, 'May')
                currentyearleadtotallist.insert(4, 0)
            elif (ctr == 'Jun'):
                monthlist.insert(5, 'Jun')
                currentyearleadtotallist.insert(5, 0)
            elif (ctr == 'Jul'):
                monthlist.insert(6, 'Jul')
                currentyearleadtotallist.insert(6, 0)
            elif (ctr == 'Aug'):
                monthlist.insert(7, 'Aug')
                currentyearleadtotallist.insert(7, 0)
            elif (ctr == 'Sep'):
                monthlist.insert(8, 'Sep')
                currentyearleadtotallist.insert(8, 0)
            elif (ctr == 'Oct'):
                monthlist.insert(9, 'Oct')
                currentyearleadtotallist.insert(9, 0)
            elif (ctr == 'Nov'):
                monthlist.insert(10, 'Nov')
                currentyearleadtotallist.insert(10, 0)
            elif (ctr == 'Dec'):
                monthlist.insert(11, 'Dec')
                currentyearleadtotallist.insert(11, 0)
            else:
                j = 0
        # here we need cumulative sum that means last value adds in the current value in this way the line chart is always ascending
        currentyearleadtotallist = np.trim_zeros(currentyearleadtotallist)
        cumuletive_sum_current = pd.Series(currentyearleadtotallist)
        currentyearleadtotallist = cumuletive_sum_current.cumsum()
        currentyearleadtotallist = list(currentyearleadtotallist)
        ####now for the previouse year also we need to read the csv
        lastyeardata = pd.pivot_table(fun_readcsv_multiline.loc[fun_readcsv_multiline['Year'] == lastyear],columns=['Month'], values='Lead', aggfunc=np.sum, fill_value=0)

        # here we get the monthwise
        monthlist = list(lastyeardata.columns)
        unorderedmonthlist = monthlist
        orderedmonthlist = sorted(unorderedmonthlist, key=lambda x: monthseriallist.index(x))
        lastyeardata = lastyeardata[orderedmonthlist]
        lastyearleadtotallist = lastyeardata.iloc[0].tolist()
        monthlists = monthlist
        monthlist = [value for value in monthseriallist if value in monthlists]
        current_monthnotavailable = set(monthseriallist).difference(monthlist)
        for ctr in current_monthnotavailable:
            if (ctr == 'Jan'):
                monthlist.insert(0, 'Jan')
                lastyearleadtotallist.insert(0, 0)
            elif (ctr == 'Feb'):
                monthlist.insert(1, 'Feb')
                lastyearleadtotallist.insert(1, 0)
            elif (ctr == 'Mar'):
                monthlist.insert(2, 'Mar')
                lastyearleadtotallist.insert(2, 0)
            elif (ctr == 'Apr'):
                monthlist.insert(3, 'Apr')
                lastyearleadtotallist.insert(3, 0)
            elif (ctr == 'May'):
                monthlist.insert(4, 'May')
                lastyearleadtotallist.insert(4, 0)
            elif (ctr == 'Jun'):
                monthlist.insert(5, 'Jun')
                lastyearleadtotallist.insert(5, 0)
            elif (ctr == 'Jul'):
                monthlist.insert(6, 'Jul')
                lastyearleadtotallist.insert(6, 0)
            elif (ctr == 'Aug'):
                monthlist.insert(7, 'Aug')
                lastyearleadtotallist.insert(7, 0)
            elif (ctr == 'Sep'):
                monthlist.insert(8, 'Sep')
                lastyearleadtotallist.insert(8, 0)
            elif (ctr == 'Oct'):
                monthlist.insert(9, 'Oct')
                lastyearleadtotallist.insert(9, 0)
            elif (ctr == 'Nov'):
                monthlist.insert(10, 'Nov')
                lastyearleadtotallist.insert(10, 0)
            elif (ctr == 'Dec'):
                monthlist.insert(11, 'Dec')
                lastyearleadtotallist.insert(11, 0)
            else:
                j = 0

        #here we need cumulative sum that means last value adds in the current value in this way the line chart is always ascending
        cumuletive_sum_current = pd.Series(lastyearleadtotallist)
        lastyearleadtotallist = cumuletive_sum_current.cumsum()
        lastyearleadtotallist = list(lastyearleadtotallist)
        contextfirst['previousy'] = lastyearleadtotallist
        contextfirst['currenty'] = currentyearleadtotallist
        contextfirst['v_labels'] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

        #from here the lead by source code starts
        both_year_data = fun_readcsv_multiline[(fun_readcsv_multiline['Year'] >= lastyear) & (fun_readcsv_multiline['Year'] <= currentyear)]
        lastyeardata_s = both_year_data.loc[both_year_data['Year'] == lastyear]
        lastyeardata_s = lastyeardata_s.reset_index()
        lastyeardata_s = pd.pivot_table(lastyeardata_s, index='Source', values='Lead', aggfunc=np.sum, fill_value=0)
        lastyeardata_s = lastyeardata_s.groupby("Source").Lead.sum().reset_index().sort_values("Source", ascending=True)
        lastyearsourcelead_s = lastyeardata_s['Lead'].to_list() # second importat thing
        for i in range(0, len(lastyearsourcelead_s)):
            lastyearsourcelead_s[i] = int(lastyearsourcelead_s[i])
        lastyearsourcelead_s = pd.Series(lastyearsourcelead_s)
        lastyearsourcelead_s = lastyearsourcelead_s.cumsum()
        lastyearsourcelead_s = list(lastyearsourcelead_s)

        x_axis_source_labels = lastyeardata_s['Source'].tolist()
        # now currentyear data
        currentyeardata_s = both_year_data.loc[both_year_data['Year'] == currentyear]
        currentyeardata_s = currentyeardata_s.reset_index()
        currentyeardata_s = pd.pivot_table(currentyeardata_s, index='Source', values='Lead', aggfunc=np.sum,fill_value=0)
        currentyeardata_s = currentyeardata_s.groupby("Source").Lead.sum().reset_index().sort_values("Source",ascending=True)
        currentyearsourcelead_s = currentyeardata_s['Lead'].to_list()  # third important thing
        for i in range(0, len(currentyearsourcelead_s)):
            currentyearsourcelead_s[i] = int(currentyearsourcelead_s[i])
        currentyearsourcelead_s = pd.Series(currentyearsourcelead_s)
        currentyearsourcelead_s = currentyearsourcelead_s.cumsum()
        currentyearsourcelead_s = list(currentyearsourcelead_s)


        contextfirst['x_axis_source_labels'] = x_axis_source_labels
        contextfirst['lastyearsourcelead'] = lastyearsourcelead_s
        contextfirst['currentyearsourcelead'] = currentyearsourcelead_s
        #source lead code ends



    if criteria == 'QTD':
        currentdate = dt.datetime.today()
        todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
        quarter_start = pd.to_datetime(currentdate - pd.tseries.offsets.QuarterBegin(startingMonth=1)).date()
        previous_date = quarter_start.strftime('%Y-%m-%d')
        fun_readcsv_multiline2 = fun_readcsv_multiline[(fun_readcsv_multiline['Date'] >= previous_date) & (fun_readcsv_multiline['Date'] <= todaysdate) & (fun_readcsv_multiline['Year']==currentyear)]
        weektotallist = fun_readcsv_multiline2.groupby(["Weeknumber"]).Lead.sum().reset_index().sort_values("Weeknumber",ascending=True)

        uniqueweeks = weektotallist['Weeknumber'].tolist()
        firstdateofweek = []
        for uw in uniqueweeks:
            fd = getDateRangeFromWeek(currentyear, uw)
            firstdateofweek.append(fd)
        week_date =  [ "week# "+str(j)+"("+str(i)+")" for i, j in zip(firstdateofweek, uniqueweeks)]
        currentyearqtrleadtotallist = weektotallist['Lead'].tolist()
        currentyearqtrleadtotallist = pd.Series(currentyearqtrleadtotallist)
        currentyearqtrleadtotallist = currentyearqtrleadtotallist.cumsum()
        currentyearqtrleadtotallist = list(currentyearqtrleadtotallist)
        contextfirst['currenty'] = currentyearqtrleadtotallist
        #data of last year needs to be derived
        lastyearqtr = fun_readcsv_multiline[(fun_readcsv_multiline['Year']==lastyear) & (fun_readcsv_multiline.Weeknumber.isin(uniqueweeks))]
        lastyearweektotallist = lastyearqtr.groupby("Weeknumber").Lead.sum().reset_index().sort_values("Weeknumber",ascending=True)
        uniquelsweeks = lastyearweektotallist['Weeknumber'].tolist()         #this is not needed for now
        lastyearqtrleadtotallist = lastyearweektotallist['Lead'].tolist()
        lastyearqtrleadtotallist = pd.Series(lastyearqtrleadtotallist)
        lastyearqtrleadtotallist = lastyearqtrleadtotallist.cumsum()
        lastyearqtrleadtotallist = list(lastyearqtrleadtotallist)
        contextfirst['previousy'] = lastyearqtrleadtotallist
        contextfirst['v_labels'] = week_date

        # from here the lead by source code starts QTD
        fun_readcsv_source_qtr = fun_readcsv_multiline[(fun_readcsv_multiline['Date'] >= previous_date) & (fun_readcsv_multiline['Date'] <= todaysdate) & (fun_readcsv_multiline['Year']==currentyear)]
        source_grouping_qtr = fun_readcsv_source_qtr.groupby(["Source"]).Lead.sum().reset_index().sort_values("Source",ascending=True)
        x_axis_source_labels = source_grouping_qtr['Source'].tolist()
        lastyearsourcelead_qtr= lastyearqtr.groupby("Source").Lead.sum().reset_index().sort_values("Source",ascending=True)
        df_outer = pd.merge(source_grouping_qtr, lastyearsourcelead_qtr, on='Source', how='outer').fillna('0')
        currentyearsourcelead_q = df_outer['Lead_x'].tolist()
        for i in range(0, len(currentyearsourcelead_q)):
            currentyearsourcelead_q[i] = int(currentyearsourcelead_q[i])
        currentyearsourcelead_q = pd.Series(currentyearsourcelead_q)
        currentyearsourcelead_q = currentyearsourcelead_q.cumsum()
        currentyearsourcelead_q = list(currentyearsourcelead_q)


        lastyearsourcelead_q = df_outer['Lead_y'].tolist()
        for i in range(0, len(lastyearsourcelead_q)):
            lastyearsourcelead_q[i] = int(lastyearsourcelead_q[i])
        lastyearsourcelead_q = pd.Series(lastyearsourcelead_q)
        lastyearsourcelead_q = lastyearsourcelead_q.cumsum()
        lastyearsourcelead_q = list(lastyearsourcelead_q)

        contextfirst['x_axis_source_labels'] = x_axis_source_labels
        contextfirst['lastyearsourcelead'] = lastyearsourcelead_q
        contextfirst['currentyearsourcelead'] = currentyearsourcelead_q


       #now we have to get the data monthwise comparison
    if criteria == 'MTD':
        previous_date = dt.datetime.today().strftime("%Y-%m-01")
        todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
        begindate = pd.to_datetime(previous_date)
        enddate = pd.to_datetime(todaysdate)
        diffdays = (enddate - begindate).days
        if diffdays<14:
            fun_readcsv_multiline2 = fun_readcsv_multiline[(fun_readcsv_multiline['Date'] >= previous_date) & (fun_readcsv_multiline['Date'] <= todaysdate) & (fun_readcsv_multiline['Year'] == currentyear)]
            weektotallist = fun_readcsv_multiline2.groupby(["Weeknumber"]).Lead.sum().reset_index().sort_values("Weeknumber", ascending=True)

            uniqueweeks = weektotallist['Weeknumber'].tolist()

            firstdateofweek = []
            for uw in uniqueweeks:
                fd = getDateRangeFromWeek(currentyear, uw)
                firstdateofweek.append(fd)
            week_date = res = ["week# " + str(j) + "(" + str(i) + ")" for i, j in zip(firstdateofweek, uniqueweeks)]
            currentyearqtrleadtotallist = weektotallist['Lead'].tolist()
            currentyearqtrleadtotallist = pd.Series(currentyearqtrleadtotallist)
            currentyearqtrleadtotallist = currentyearqtrleadtotallist.cumsum()
            currentyearqtrleadtotallist = list(currentyearqtrleadtotallist)
            contextfirst['currenty'] = currentyearqtrleadtotallist
            # data of last year needs to be derived
            lastyearqtr = fun_readcsv_multiline[(fun_readcsv_multiline['Year'] == lastyear) & (fun_readcsv_multiline.Weeknumber.isin(uniqueweeks))]
            lastyearweektotallist = lastyearqtr.groupby("Weeknumber").Lead.sum().reset_index().sort_values("Weeknumber",ascending=True)
            uniquelsweeks = lastyearweektotallist['Weeknumber'].tolist()  # this is not needed for now
            lastyearqtrleadtotallist = lastyearweektotallist['Lead'].tolist()
            lastyearqtrleadtotallist = pd.Series(lastyearqtrleadtotallist)
            lastyearqtrleadtotallist = lastyearqtrleadtotallist.cumsum()
            lastyearqtrleadtotallist = list(lastyearqtrleadtotallist)
            contextfirst['previousy'] = lastyearqtrleadtotallist
            contextfirst['v_labels'] = week_date

            #show the data datewise
        else:
            fun_readcsv_multiline2 = fun_readcsv_multiline[(fun_readcsv_multiline['Date'] >= previous_date) & (fun_readcsv_multiline['Date'] <= todaysdate) & (fun_readcsv_multiline['Year'] == currentyear)]
            datetotallist = fun_readcsv_multiline2.groupby(["Date"]).Lead.sum().reset_index().sort_values("Date", ascending=True)
            uniquedates = datetotallist['Date'].tolist()
            currentmonthleadtotallist = datetotallist['Lead'].tolist()
            currentmonthleadtotallist = pd.Series(currentmonthleadtotallist)
            currentmonthleadtotallist = currentmonthleadtotallist.cumsum()
            currentmonthleadtotallist = list(currentmonthleadtotallist)
            contextfirst['currenty'] = currentmonthleadtotallist
            # data of last year needs to be derived
            uniquelastyeardates = [w.replace('2020', '2019') for w in uniquedates]
            lastyearsamemonth = fun_readcsv_multiline[(fun_readcsv_multiline['Year'] == lastyear) & (fun_readcsv_multiline.Date.isin(uniquelastyeardates))]
            lastyearsamemonthtotallist = lastyearsamemonth.groupby("Date").Lead.sum().reset_index().sort_values("Date",ascending=True)
            lastyearsamemonthleadtotallist = lastyearsamemonthtotallist['Lead'].tolist()
            lastyearsamemonthleadtotallist = pd.Series(lastyearsamemonthleadtotallist)
            lastyearsamemonthleadtotallist = lastyearsamemonthleadtotallist.cumsum()
            lastyearsamemonthleadtotallist = list(lastyearsamemonthleadtotallist)
            contextfirst['previousy'] = lastyearsamemonthleadtotallist
            contextfirst['v_labels'] = uniquedates

        #Now we need to get the MTD For Source also
        var_read_csv_source_m = fun_readcsv_multiline[(fun_readcsv_multiline['Date'] >= previous_date) & (fun_readcsv_multiline['Date'] <= todaysdate) & (fun_readcsv_multiline['Year'] == currentyear)]
        uniquedatelist = var_read_csv_source_m['Date'].unique().tolist()
        var_read_csv_source_current = var_read_csv_source_m.groupby(["Source"]).Lead.sum().reset_index().sort_values("Source", ascending=True)
        uniquedatelist = [w.replace(str(currentyear), str(lastyear)) for w in uniquedatelist]
        var_read_csv_source_last= fun_readcsv_multiline[(fun_readcsv_multiline['Year'] == lastyear) & (fun_readcsv_multiline.Date.isin(uniquedatelist))]
        var_read_csv_source_last=var_read_csv_source_last.groupby(["Source"]).Lead.sum().reset_index().sort_values("Source", ascending=True)
        df_outer_join = pd.merge(var_read_csv_source_current, var_read_csv_source_last, on='Source', how='outer').fillna(0)
        x_axis_source_labels_m = df_outer_join['Source'].tolist()
        lastyearsourcelead_m = df_outer_join['Lead_y'].tolist()
        lastyearsourcelead_m = pd.Series(lastyearsourcelead_m)
        lastyearsourcelead_m = lastyearsourcelead_m.cumsum()
        lastyearsourcelead_m = list(lastyearsourcelead_m)
        for i in range(0, len(lastyearsourcelead_m)):
            lastyearsourcelead_m[i] = int(lastyearsourcelead_m[i])
        currentyearsourcelead_m = df_outer_join['Lead_x'].tolist()
        currentyearsourcelead_m = pd.Series(currentyearsourcelead_m)
        currentyearsourcelead_m = currentyearsourcelead_m.cumsum()
        currentyearsourcelead_m = list(currentyearsourcelead_m)
        for i in range(0, len(currentyearsourcelead_m)):
            currentyearsourcelead_m[i] = int(currentyearsourcelead_m[i])
        contextfirst['lastyearsourcelead'] = lastyearsourcelead_m
        contextfirst['currentyearsourcelead'] = currentyearsourcelead_m
        contextfirst['x_axis_source_labels'] = x_axis_source_labels_m
        #now get the unique date list
    #now we want source wise comperison with line and bar and use cumsum function in it

    return contextfirst

def Senddata(request):
    return render(request,'LeadApplication/genericview.html')





def getDateRangeFromWeek(p_year,p_week):
    firstdayofweek = datetime.datetime.strptime(f'{p_year}-W{int(p_week )- 1}-1', "%Y-W%W-%w").date()
    # lastdayofweek = firstdayofweek + datetime.timedelta(days=6.9)
    a = firstdayofweek.strftime('%Y-%m-%d')
    return a




class ChangePasswordView(LoginRequiredMixin, TemplateView):
    template_name = "change_password.html"

    def get_context_data(self, **kwargs):
        context = super(ChangePasswordView, self).get_context_data(**kwargs)
        context["change_password_form"] = ChangePasswordForm()
        return context

    def post(self, request, *args, **kwargs):
        error, errors = "", ""
        form = ChangePasswordForm(request.POST, user=request.user)
        if form.is_valid():
            user = request.user
            # if not check_password(request.POST.get('CurrentPassword'),
            #                       user.password):
            #     error = "Invalid old password"
            # else:
            user.set_password(request.POST.get('Newpassword'))
            user.is_active = True
            user.save()
            return HttpResponseRedirect('/')
        else:
            errors = form.errors
        return render(request, "change_password.html",
                      {'error': error, 'errors': errors,
                       'change_password_form': form})


class ProfileView(LoginRequiredMixin, TemplateView):
    template_name = "profile.html"

    def get_context_data(self, **kwargs):

        context = super(ProfileView, self).get_context_data(**kwargs)
        context["user_obj"] = self.request.user
        return context


class LoginView(TemplateView):
    template_name = "login.html"

    def get_context_data(self, **kwargs):
        context = super(LoginView, self).get_context_data(**kwargs)
        context["ENABLE_GOOGLE_LOGIN"] = settings.ENABLE_GOOGLE_LOGIN
        context["GP_CLIENT_SECRET"] = settings.GP_CLIENT_SECRET
        context["GP_CLIENT_ID"] = settings.GP_CLIENT_ID
        return context

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect('/')
        return super(LoginView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = LoginForm(request.POST, request=request)
        if form.is_valid():

            user = User.objects.filter(email=request.POST.get('email')).first()
            # user = authenticate(username=request.POST.get('email'), password=request.POST.get('password'))
            if user is not None:
                if user.is_active:
                    user = authenticate(username=request.POST.get(
                        'email'), password=request.POST.get('password'))

                    if user is not None:
                        login(request, user)
                        return HttpResponseRedirect('/')
                    return render(request, "login.html", {
                        "ENABLE_GOOGLE_LOGIN": settings.ENABLE_GOOGLE_LOGIN,
                        "GP_CLIENT_SECRET": settings.GP_CLIENT_SECRET,
                        "GP_CLIENT_ID": settings.GP_CLIENT_ID,
                        "error": True,
                        "message":
                        "Your username and password didn't match. \
                        Please try again."
                    })
                return render(request, "login.html", {
                    "ENABLE_GOOGLE_LOGIN": settings.ENABLE_GOOGLE_LOGIN,
                    "GP_CLIENT_SECRET": settings.GP_CLIENT_SECRET,
                    "GP_CLIENT_ID": settings.GP_CLIENT_ID,
                    "error": True,
                    "message":
                    "Your Account is inactive. Please Contact Administrator"
                })
            return render(request, "login.html", {
                "ENABLE_GOOGLE_LOGIN": settings.ENABLE_GOOGLE_LOGIN,
                "GP_CLIENT_SECRET": settings.GP_CLIENT_SECRET,
                "GP_CLIENT_ID": settings.GP_CLIENT_ID,
                "error": True,
                "message":
                "Your Account is not Found. Please Contact Administrator"
            })

        return render(request, "login.html", {
            "ENABLE_GOOGLE_LOGIN": settings.ENABLE_GOOGLE_LOGIN,
            "GP_CLIENT_SECRET": settings.GP_CLIENT_SECRET,
            "GP_CLIENT_ID": settings.GP_CLIENT_ID,
            # "error": True,
            # "message": "Your username and password didn't match. Please try again."
            "form": form
        })


class ForgotPasswordView(TemplateView):
    template_name = "forgot_password.html"


class LogoutView(LoginRequiredMixin, View):

    def get(self, request, *args, **kwargs):
        logout(request)
        request.session.flush()
        return redirect("common:login")


class UsersListView(AdminRequiredMixin, TemplateView):
    model = User
    context_object_name = "users"
    template_name = "list.html"

    def get_queryset(self):
        queryset = self.model.objects.all()

        request_post = self.request.POST
        if request_post:
            if request_post.get('username'):
                queryset = queryset.filter(
                    username__icontains=request_post.get('username'))
            if request_post.get('email'):
                queryset = queryset.filter(
                    email=request_post.get('email'))
            if request_post.get('role'):
                queryset = queryset.filter(
                    role=request_post.get('role'))

        return queryset.order_by('username')

    def get_context_data(self, **kwargs):
        context = super(UsersListView, self).get_context_data(**kwargs)
        active_users = self.get_queryset().filter(is_active=True)
        inactive_users = self.get_queryset().filter(is_active=False)
        context["active_users"] = active_users
        context["inactive_users"] = inactive_users
        context["per_page"] = self.request.POST.get('per_page')
        context['admin_email'] = settings.ADMIN_EMAIL
        context['roles'] = ROLES
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class CreateUserView(AdminRequiredMixin, CreateView):
    model = User
    form_class = UserForm
    template_name = "create.html"

    def form_valid(self, form):
        user = form.save(commit=False)
        if form.cleaned_data.get("password"):
            user.set_password(form.cleaned_data.get("password"))
        user.save()

        current_site = self.request.get_host()
        protocol = self.request.scheme
        send_email_to_new_user(user.email,user.username,user.password ,self.request.user.email,
                                     domain=current_site, protocol=protocol,)
        mail_subject = 'Created account in LMS'
        message = render_to_string('new_user.html', {
            'user': user,
            'created_by': self.request.user

        })
        email = EmailMessage(mail_subject, message, to=[user.email])
        email.content_subtype = "html"
        email.send()

        if self.request.is_ajax():
            data = {'success_url': reverse_lazy(
                'common:users_list'), 'error': False}
            return JsonResponse(data)
        return super(CreateUserView, self).form_valid(form)

    def form_invalid(self, form):
        response = super(CreateUserView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse({'error': True, 'errors': form.errors})
        return response

    def get_context_data(self, **kwargs):
        context = super(CreateUserView, self).get_context_data(**kwargs)
        context["user_form"] = context["form"]
        if "errors" in kwargs:
            context["errors"] = kwargs["errors"]
        return context


class UserDetailView(AdminRequiredMixin, DetailView):
    model = User
    context_object_name = "users"
    template_name = "user_detail.html"

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        user_obj = self.object
        users_data = []
        for each in User.objects.all():
            assigned_dict = {}
            assigned_dict['id'] = each.id
            assigned_dict['name'] = each.username
            users_data.append(assigned_dict)
        context.update({
            "user_obj": user_obj,

            # "accounts": Account.objects.filter(assigned_to=user_obj.id),
            "assigned_data": json.dumps(users_data),
            "comments": user_obj.user_comments.all(),
        })
        return context


class UpdateUserView(LoginRequiredMixin, UpdateView):
    model = User
    form_class = UserForm
    template_name = "create.html"

    def form_valid(self, form):
        user = form.save(commit=False)
        if self.request.is_ajax():
            if (self.request.user.role != "ADMIN" and not
                    self.request.user.is_superuser):
                if self.request.user.id != self.object.id:
                    data = {'error_403': True, 'error': True}
                    return JsonResponse(data)
        if user.role == "USER":
            user.is_superuser = False
        user.save()
        if (self.request.user.role == "ADMIN" and
                self.request.user.is_superuser):
            if self.request.is_ajax():
                data = {'success_url': reverse_lazy(
                    'common:users_list'), 'error': False}
                return JsonResponse(data)
        if self.request.is_ajax():
            data = {'success_url': reverse_lazy(
                'common:profile'), 'error': False}
            return JsonResponse(data)
        return super(UpdateUserView, self).form_valid(form)

    def form_invalid(self, form):
        response = super(UpdateUserView, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse({'error': True, 'errors': form.errors})
        return response

    def get_context_data(self, **kwargs):
        context = super(UpdateUserView, self).get_context_data(**kwargs)
        context["user_obj"] = self.object
        user_profile_name = str(context["user_obj"].profile_pic).split("/")
        user_profile_name = user_profile_name[-1]
        context["user_profile_name"] = user_profile_name
        context["user_form"] = context["form"]
        if "errors" in kwargs:
            context["errors"] = kwargs["errors"]
        return context


class UserDeleteView(AdminRequiredMixin, DeleteView):
    model = User

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        current_site = request.get_host()
        send_email_user_delete.delay(
            self.object.email, domain=current_site, protocol=request.scheme)
        self.object.delete()
        return redirect("common:users_list")


class PasswordResetView(PasswordResetView):
    template_name = 'registration/password_reset_form.html'
    form_class = PasswordResetEmailForm
    email_template_name = 'registration/password_reset_email.html'


def document_create(request):
    template_name = "doc_create.html"
    users = []
    if request.user.role == 'ADMIN' or request.user.is_superuser:
        users = User.objects.filter(is_active=True).order_by('email')
    elif request.user.google.all():
        users = []
    else:
        users = User.objects.filter(role='ADMIN').order_by('email')
    form = DocumentForm(users=users)
    if request.POST:
        form = DocumentForm(request.POST, request.FILES, users=users)
        if form.is_valid():
            doc = form.save(commit=False)
            doc.created_by = request.user
            doc.save()
            if request.POST.getlist('shared_to'):
                doc.shared_to.add(*request.POST.getlist('shared_to'))
            data = {'success_url': reverse_lazy(
                'common:doc_list'), 'error': False}
            return JsonResponse(data)
        return JsonResponse({'error': True, 'errors': form.errors})
    context = {}
    context["doc_form"] = form
    context["users"] = users
    context["sharedto_list"] = [
        int(i) for i in request.POST.getlist('assigned_to', []) if i]
    context["errors"] = form.errors
    return render(request, template_name, context)


class DocumentListView(LoginRequiredMixin, TemplateView):
    model = Document
    context_object_name = "documents"
    template_name = "doc_list.html"

    def get_queryset(self):
        queryset = self.model.objects.all()
        if self.request.user.is_superuser or self.request.user.role == "ADMIN":
            queryset = queryset
        else:
            if self.request.user.documents():
                doc_ids = self.request.user.documents().values_list('id',
                                                                    flat=True)
                shared_ids = queryset.filter(
                    Q(status='active') &
                    Q(shared_to__id__in=[self.request.user.id])).values_list(
                    'id', flat=True)
                queryset = queryset.filter(
                    Q(id__in=doc_ids) | Q(id__in=shared_ids))
            else:
                queryset = queryset.filter(Q(status='active') & Q(
                    shared_to__id__in=[self.request.user.id]))

        request_post = self.request.POST
        if request_post:
            if request_post.get('doc_name'):
                queryset = queryset.filter(
                    title__icontains=request_post.get('doc_name'))
            if request_post.get('status'):
                queryset = queryset.filter(status=request_post.get('status'))

            if request_post.getlist('shared_to'):
                queryset = queryset.filter(
                    shared_to__id__in=request_post.getlist('shared_to'))
        return queryset

    def get_context_data(self, **kwargs):
        context = super(DocumentListView, self).get_context_data(**kwargs)
        context["users"] = User.objects.filter(
            is_active=True).order_by('email')
        context["documents"] = self.get_queryset()
        context["status_choices"] = Document.DOCUMENT_STATUS_CHOICE
        context["sharedto_list"] = [
            int(i) for i in self.request.POST.getlist('shared_to', []) if i]
        context["per_page"] = self.request.POST.get('per_page')

        search = False
        if (
            self.request.POST.get('doc_name') or
            self.request.POST.get('status') or
            self.request.POST.get('shared_to')
        ):
            search = True

        context["search"] = search
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


class DocumentDeleteView(LoginRequiredMixin, DeleteView):
    model = Document

    def get(self, request, *args, **kwargs):
        if not request.user.role == 'ADMIN':
            if not request.user == Document.objects.get(id=kwargs['pk']).created_by:
                raise PermissionDenied
        self.object = self.get_object()
        self.object.delete()
        return redirect("common:doc_list")


def document_update(request, pk):
    template_name = "doc_create.html"
    users = []
    if request.user.role == 'ADMIN' or request.user.is_superuser:
        users = User.objects.filter(is_active=True).order_by('email')
    elif request.user.google.all():
        users = []
    else:
        users = User.objects.filter(role='ADMIN').order_by('email')
    document = Document.objects.filter(id=pk).first()
    form = DocumentForm(users=users, instance=document)

    if request.POST:
        form = DocumentForm(request.POST, request.FILES,
                            instance=document, users=users)
        if form.is_valid():
            doc = form.save(commit=False)
            doc.save()

            doc.shared_to.clear()
            if request.POST.getlist('shared_to'):
                doc.shared_to.add(*request.POST.getlist('shared_to'))

            data = {'success_url': reverse_lazy(
                'common:doc_list'), 'error': False}
            return JsonResponse(data)
        return JsonResponse({'error': True, 'errors': form.errors})
    context = {}
    context["doc_obj"] = document
    context["doc_form"] = form
    context["doc_file_name"] = context["doc_obj"].document_file.name.split(
        "/")[-1]
    context["users"] = users
    context["sharedto_list"] = [
        int(i) for i in request.POST.getlist('shared_to', []) if i]
    context["errors"] = form.errors
    return render(request, template_name, context)


class DocumentDetailView(LoginRequiredMixin, DetailView):
    model = Document
    template_name = "doc_detail.html"

    def dispatch(self, request, *args, **kwargs):
        if not request.user.role == 'ADMIN':
            if (not request.user ==
                    Document.objects.get(id=kwargs['pk']).created_by):
                raise PermissionDenied

        return super(DocumentDetailView, self).dispatch(
            request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DocumentDetailView, self).get_context_data(**kwargs)
        # documents = Document.objects.all()
        context.update({
            "file_type_code": self.object.file_type()[1],
            "doc_obj": self.object,
        })
        return context


def download_document(request, pk):
    doc_obj = Document.objects.filter(id=pk).last()
    if doc_obj:
        if not request.user.role == 'ADMIN':
            if (not request.user == doc_obj.created_by and
                    request.user not in doc_obj.shared_to.all()):
                raise PermissionDenied
        if settings.STORAGE_TYPE == "normal":
            # print('no no no no')
            path = doc_obj.document_file.path
            file_path = os.path.join(settings.MEDIA_ROOT, path)
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    response = HttpResponse(
                        fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + \
                        os.path.basename(file_path)
                    return response
        else:
            file_path = doc_obj.document_file
            file_name = doc_obj.title
            # print(file_path)
            # print(file_name)
            BUCKET_NAME = "django-crm-demo"
            KEY = str(file_path)
            s3 = boto3.resource('s3')
            try:
                s3.Bucket(BUCKET_NAME).download_file(KEY, file_name)
                # print('got it')
                with open(file_name, 'rb') as fh:
                    response = HttpResponse(
                        fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + \
                        os.path.basename(file_name)
                os.remove(file_name)
                return response
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "404":
                    print("The object does not exist.")
                else:
                    raise

            return path
    raise Http404


def download_attachment(request, pk):
    attachment_obj = Attachments.objects.filter(id=pk).last()
    if attachment_obj:
        if settings.STORAGE_TYPE == "normal":
            path = attachment_obj.attachment.path
            file_path = os.path.join(settings.MEDIA_ROOT, path)
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    response = HttpResponse(
                        fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + \
                        os.path.basename(file_path)
                    return response
        else:
            file_path = attachment_obj.attachment
            file_name = attachment_obj.file_name
            # print(file_path)
            # print(file_name)
            BUCKET_NAME = "django-crm-demo"
            KEY = str(file_path)
            s3 = boto3.resource('s3')
            try:
                s3.Bucket(BUCKET_NAME).download_file(KEY, file_name)
                with open(file_name, 'rb') as fh:
                    response = HttpResponse(
                        fh.read(), content_type="application/vnd.ms-excel")
                    response['Content-Disposition'] = 'inline; filename=' + \
                        os.path.basename(file_name)
                os.remove(file_name)
                return response
            except botocore.exceptions.ClientError as e:
                if e.response['Error']['Code'] == "404":
                    print("The object does not exist.")
                else:
                    raise
            # if file_path:
            #     print('yes tus pus')
    raise Http404


def change_user_status(request, pk):
    user = get_object_or_404(User, pk=pk)
    if user.is_active:
        user.is_active = False
    else:
        user.is_active = True
    user.save()
    current_site = request.get_host()
    send_email_user_status.delay(
        pk, domain=current_site, protocol=request.scheme)
    return HttpResponseRedirect('/users/list/')


def add_comment(request):
    if request.method == "POST":
        user = get_object_or_404(User, id=request.POST.get('userid'))
        form = UserCommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.commented_by = request.user
            comment.user = user
            comment.save()
            return JsonResponse({
                "comment_id": comment.id, "comment": comment.comment,
                "commented_on": comment.commented_on,
                "commented_by": comment.commented_by.email
            })
        return JsonResponse({"error": form.errors})


def edit_comment(request, pk):
    if request.method == "POST":
        comment_obj = get_object_or_404(Comment, id=pk)
        if request.user == comment_obj.commented_by:
            form = UserCommentForm(request.POST, instance=comment_obj)
            if form.is_valid():
                comment_obj.comment = form.cleaned_data.get("comment")
                comment_obj.save(update_fields=["comment"])
                return JsonResponse({
                    "comment_id": comment_obj.id,
                    "comment": comment_obj.comment,
                })
            return JsonResponse({"error": form['comment'].errors})
        data = {'error': "You don't have permission to edit this comment."}
        return JsonResponse(data)


def remove_comment(request):
    if request.method == "POST":
        comment_obj = get_object_or_404(
            Comment, id=request.POST.get('comment_id'))
        if request.user == comment_obj.commented_by:
            comment_obj.delete()
            data = {"cid": request.POST.get("comment_id")}
            return JsonResponse(data)
        data = {'error': "You don't have permission to delete this comment."}
        return JsonResponse(data)


def api_settings(request):
    api_settings = APISettings.objects.all()
    data = {'settings': api_settings}
    return render(request, 'settings/list.html', data)


def add_api_settings(request):
    users = User.objects.filter(is_active=True).order_by('email')
    form = APISettingsForm(assign_to=users)
    assign_to_list = []
    if request.POST:
        form = APISettingsForm(request.POST, assign_to=users)
        assign_to_list = [
            int(i) for i in request.POST.getlist('lead_assigned_to', []) if i]
        if form.is_valid():
            settings_obj = form.save(commit=False)
            settings_obj.created_by = request.user
            settings_obj.save()
            if request.POST.get('tags', ''):
                tags = request.POST.get("tags")
                splitted_tags = tags.split(",")

                settings_obj.tags.add(tags)
            if assign_to_list:
                settings_obj.lead_assigned_to.add(*assign_to_list)
            success_url = reverse_lazy("common:api_settings")
            if request.POST.get("savenewform"):
                success_url = reverse_lazy("common:add_api_settings")
            data = {'success_url': success_url, 'error': False}
            return JsonResponse(data)
        return JsonResponse({'error': True, 'errors': form.errors})
    data = {'form': form, "setting": api_settings,
            'users': users, 'assign_to_list': assign_to_list}
    return render(request, 'settings/create.html', data)


def view_api_settings(request, pk):
    api_settings = APISettings.objects.filter(pk=pk).first()
    data = {"setting": api_settings}
    return render(request, 'settings/view.html', data)


def update_api_settings(request, pk):
    api_settings = APISettings.objects.filter(pk=pk).first()
    users = User.objects.filter(is_active=True).order_by('email')
    form = APISettingsForm(instance=api_settings, assign_to=users)
    assign_to_list = []
    if request.POST:
        form = APISettingsForm(
            request.POST, instance=api_settings, assign_to=users)
        assign_to_list = [
            int(i) for i in request.POST.getlist('lead_assigned_to', []) if i]
        if form.is_valid():
            settings_obj = form.save(commit=False)
            settings_obj.save()
            if request.POST.get('tags', ''):
                settings_obj.tags.clear()
                tags = request.POST.get("tags")
                splitted_tags = tags.split(",")
                for t in splitted_tags:

                    settings_obj.tags.add(t)
            if assign_to_list:
                settings_obj.lead_assigned_to.clear()
                settings_obj.lead_assigned_to.add(*assign_to_list)
            success_url = reverse_lazy("common:api_settings")
            if request.POST.get("savenewform"):
                success_url = reverse_lazy("common:add_api_settings")
            data = {'success_url': success_url, 'error': False}
            return JsonResponse(data)
        return JsonResponse({'error': True, 'errors': form.errors})
    data = {
        'form': form, "setting": api_settings,
        'users': users, 'assign_to_list': assign_to_list,
        'assigned_to_list':
        json.dumps(
            [setting.id for setting in api_settings.lead_assigned_to.all() if setting])
    }
    return render(request, 'settings/update.html', data)


def delete_api_settings(request, pk):
    api_settings = APISettings.objects.filter(pk=pk).first()
    if api_settings:
        api_settings.delete()
        data = {"error": False, "response": "Successfully Deleted!"}
    else:
        data = {"error": True,
                "response": "Object Not Found!"}
    # return JsonResponse(data)
    return redirect('common:api_settings')


def change_passsword_by_admin(request):
    if request.user.role == "ADMIN" or request.user.is_superuser:
        if request.method == "POST":
            user = get_object_or_404(User, id=request.POST.get("useer_id"))
            user.set_password(request.POST.get("new_passwoord"))
            user.save()
            mail_subject = 'LMS Account Password Changed'
            message = "<h3><b>hello</b> <i>" + user.username +\
                "</i></h3><br><h2><p> <b>Your account password has been changed !\
                 </b></p></h2>" \
                + "<br> <p><b> New Password</b> : <b><i>" + \
                request.POST.get("new_passwoord") + "</i><br></p>"
            email = EmailMessage(mail_subject, message, to=[user.email])
            email.content_subtype = "html"
            email.send()
            return HttpResponseRedirect('/users/list/')
    raise PermissionDenied


def google_login(request):
    if 'code' in request.GET:
        params = {
            'grant_type': 'authorization_code',
            'code': request.GET.get('code'),
            'redirect_uri': 'http://' +
            request.META['HTTP_HOST'] + reverse('common:google_login'),
            'client_id': settings.GP_CLIENT_ID,
            'client_secret': settings.GP_CLIENT_SECRET
        }

        info = requests.post(
            "https://accounts.google.com/o/oauth2/token", data=params)
        info = info.json()
        url = 'https://www.googleapis.com/oauth2/v1/userinfo'
        params = {'access_token': info['access_token']}
        kw = dict(params=params, headers={}, timeout=60)
        response = requests.request('GET', url, **kw)
        user_document = response.json()

        link = "https://plus.google.com/" + user_document['id']
        dob = user_document['birthday'] if 'birthday' in user_document.keys(
        ) else ""
        gender = user_document['gender'] if 'gender' in user_document.keys(
        ) else ""
        link = user_document['link'] if 'link' in user_document.keys(
        ) else link

        verified_email = user_document['verified_email'] if 'verified_email' in user_document.keys(
        ) else ''
        name = user_document['name'] if 'name' in user_document.keys(
        ) else 'name'
        first_name = user_document['given_name'] if 'given_name' in user_document.keys(
        ) else 'first_name'
        last_name = user_document['family_name'] if 'family_name' in user_document.keys(
        ) else 'last_name'
        email = user_document['email'] if 'email' in user_document.keys(
        ) else 'email@dummy.com'

        user = User.objects.filter(email=user_document['email'])

        if user:
            user = user[0]
            user.first_name = first_name
            user.last_name = last_name
        else:
            user = User.objects.create(
                username=email,
                email=email,
                first_name=first_name,
                last_name=last_name,
                role="USER"
            )

        google, _ = Google.objects.get_or_create(user=user)
        google.user = user
        google.google_url = link
        google.verified_email = verified_email
        google.google_id = user_document['id']
        google.family_name = last_name
        google.name = name
        google.given_name = first_name
        google.dob = dob
        google.email = email
        google.gender = gender
        google.save()

        user.last_login = datetime.datetime.now()
        user.save()

        login(request, user)

        if request.GET.get('state') != '1235dfghjkf123':
            return HttpResponseRedirect(request.GET.get('state'))
        return HttpResponseRedirect(reverse("common:home"))
    if request.GET.get('next'):
        next_url = request.GET.get('next')
    else:
        next_url = '1235dfghjkf123'
    rty = "https://accounts.google.com/o/oauth2/auth?client_id=" + \
        settings.GP_CLIENT_ID + "&response_type=code"
    rty += "&scope=https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email&redirect_uri=" \
        + 'http://' + request.META['HTTP_HOST'] + \
        reverse('common:google_login') + "&state=" + next_url
    return HttpResponseRedirect(rty)


