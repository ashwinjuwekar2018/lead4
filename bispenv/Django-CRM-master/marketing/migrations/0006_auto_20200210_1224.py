# Generated by Django 2.2.2 on 2020-02-10 06:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('marketing', '0005_auto_20191207_1304'),
    ]

    operations = [
        migrations.RenameField(
            model_name='failedcontact',
            old_name='contry',
            new_name='country',
        ),
    ]
