import csv
import os
import sched
import datetime as dt
from django.urls import reverse_lazy
from django_common.auth_backends import User
from geotext import GeoText
import pandas as pd
import numpy as np
from .models import *
import json
from email.mime.image import MIMEImage
from functools import lru_cache
from django.contrib.staticfiles import finders
from datetime import datetime as ddatetime
from django.core.mail import EmailMessage
import pytz
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db.models import Q
from django.http.response import (HttpResponse, HttpResponseRedirect,JsonResponse)
from django.shortcuts import get_object_or_404, redirect, render
from  common import status
from marketing.forms import (ContactForm, ContactListForm,PdfForm)
from marketing.tasks import  upload_csv_file

from pdfminer3.pdfpage import PDFPage
from pdfminer3.pdfinterp import PDFResourceManager
from pdfminer3.pdfinterp import PDFPageInterpreter
from pdfminer3.converter import PDFPageAggregator, TextConverter
from pdfminer3.layout import LAParams, LTChar
import io
from django.views import View

import re
import xlwt

from django.http import HttpResponse
import pandas as pd






TIMEZONE_CHOICES = [(tz, tz) for tz in pytz.common_timezones]


#function for exact match string
def get_exact_match(query, m2m_field, ids):
    # query = tasks_list.annotate(count=Count(m2m_field))\
    #             .filter(count=len(ids))
    query = query
    for _id in ids:
        query = query.filter(**{m2m_field: _id})
    return query

def fun_read_pdf(request):

    try:
        # managing pdf files
        obj_resource_manager = PDFResourceManager()
        # converting files in to string
        fake_file_handle = io.StringIO()
        codec = 'utf-8'
        # Set parameters for analysis.
        laparams = LAParams()
        converter = TextConverter(obj_resource_manager, fake_file_handle, codec=codec, laparams=laparams)
        page_interpreter = PDFPageInterpreter(obj_resource_manager, converter)

        for foldername, subfolders, files in os.walk(r"E:/analytyics_leads_opportunity_pdf/bispenv/Django-CRM-master/media/contacts/pdfs"):

            for one_file in files:
                fp = open('E:/analytyics_leads_opportunity_pdf/bispenv/Django-CRM-master/media/contacts/pdfs' + '/' + one_file, 'rb')
                var_name = str(fp)

                fp_bad_chars = ['<_io.BufferedReader name=', 'E:/analytyics_leads_opportunity_pdf/bispenv/Django-CRM-master/media/contacts/pdfs/',
                                '>', '.pdf', "'"]

                obj_contact = Contactlinkedin()
                obj_contact.created_by = request.user

                for page in PDFPage.get_pages(fp,
                                              caching=True,
                                              check_extractable=True):
                    page_interpreter.process_page(page)

                text = fake_file_handle.getvalue()


                for i in fp_bad_chars:
                    var_name = var_name.replace(i, '')

                    var_full_name = var_name
                    obj_contact.name = var_full_name

                country = GeoText(text)
                country = country.countries
                for country in country:
                    var_country = country
                    obj_contact.country = var_country

                print(country)
                lst = re.findall('\S+@\S+', text)
                for i in lst:
                    var_email_id = i
                    obj_contact.email = var_email_id

                bad_chars = [';', ':', '!', "*", ",", "(", ")", 'Professional Working', 'Programming Language',
                             'Native or Bilingual', '\xa0', '\x0c', 'Area', 'Home', '+91', 'Mobile', 'and', 'Work',
                             '+234',
                             '#', '+', 'Full Professional']
                #
                for i in bad_chars:
                    text = text.replace(i, '')
                text = text.split('\n')

                text = [x for x in text if x]

                for i, w in enumerate(text):

                    if w == 'Experience':
                        var_company_name = (text[i + 1])
                        var_position = (text[i + 2])
                        var_position1 = var_position.replace(" ", "")
                        bad_chars1 = ['!', '@', '#', '$','%','^','&','*','(',')','-',':',';','"',"'",'[',']','{','}','|','/','?','.',',','|']
                        for b in bad_chars1:
                            var_position1 = var_position1.replace(b, '')
                        if (var_position1.isalpha()) == True:
                            var_position2 = (text[i + 2])
                        else:
                            var_position2 = (text[i + 3])

                        obj_contact.position = var_position2
                        obj_contact.company_name = var_company_name

                    if w == 'Top Skills':
                        var_skills = (text[i + 1] + ' ' + text[i + 2] + ' ' + text[i + 3])
                        obj_contact.skills = var_skills

                    if w == 'Certifications' or w == 'Honors-Awards':
                        var_certification = (text[i + 1])
                        obj_contact.certification = var_certification
                    if w == 'Languages':
                        var_languages = (text[i + 1] + ' ' + text[i + 2])
                        obj_contact.language = var_languages

                obj_contact.save()
                f = open("sucess.txt", "a+")

                f.write("Success File Name" + var_name  + '\n')

        return render(request,'marketing/pdf.html')

    except:

        # code for application has failed
        f = open("failed.txt", "a+")

        f.write("Failed File Name" + var_name   + '\n')

        print("An run time exception occurred")
        return render(request, 'marketing/pdf.html')


#Function for dashboard
@login_required(login_url='/login')
def dashboard(request):
    print('jojobhaiya')
    todaysdate = dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    next5days = dt.date.today() + timedelta(5)
    now = dt.date.today()

    if request.user.role == 'ADMIN' or request.user.is_superuser:
        contacts = Contact.objects.all()
        contacts_list = ContactList.objects.all()
        # event_list = Event.objects.all()
        event_list = Event.objects.filter(start_time__range=(todaysdate, next5days))

        link_contacts_list =Contactlinkedin.objects.all()
        for events in event_list:
            obj_events_date = events.start_time
            var_current_date = obj_events_date.strftime('%Y-%m-%d')
            print(var_current_date)
            print('koko')

            var_today = datetime.now()
            upcoming_date = var_today.strftime('%Y-%m-%d')
            #
            # if var_current_date > upcoming_date:
            #     print('currrnt')
            #     upcoming_event_list = var_current_date.count(var_current_date)
            #     print(upcoming_event_list)

    else:
        user = request.user.id
        contacts = Contact.objects.filter(created_by=user)
        contacts_list = ContactList.objects.filter(created_by=user)
        # event_list = Event.objects.all()
        event_list = Event.objects.filter(start_time__range=(todaysdate, next5days))
        link_contacts_list = Contactlinkedin.objects.filter(created_by=user)
        for events in event_list:
            obj_events_date = events.start_time
            var_current_date = obj_events_date.strftime('%Y-%m-%d')
            print(var_current_date)
            var_today = datetime.now()
            upcoming_date = var_today.strftime('%Y-%m-%d')

    fun_readcsv_multiline = pd.read_csv(
        'E:/analytyics_leads_opportunity_pdf/bispenv/Django-CRM-master/media/csv_of_leads_final.csv')
    monthseriallist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    currentyear = pd.datetime.now().year
    lastyear = currentyear - 1

    currentyeardata = pd.pivot_table(fun_readcsv_multiline.loc[fun_readcsv_multiline['Year'] == currentyear],
                                     columns=['Month'], values='Lead', aggfunc=np.sum, fill_value=0)

    monthlist = list(currentyeardata.columns)
    unorderedmonthlist = monthlist
    orderedmonthlist = sorted(unorderedmonthlist, key=lambda x: monthseriallist.index(x))
    currentyeardata = currentyeardata[orderedmonthlist]
    currentyearleadtotallist = currentyeardata.iloc[0].tolist()

    monthlists = monthlist
    monthlist = [value for value in monthseriallist if value in monthlists]
    current_monthnotavailable = set(monthseriallist).difference(monthlist)
    for ctr in current_monthnotavailable:
        if (ctr == 'Jan'):
            monthlist.insert(0, 'Jan')
            currentyearleadtotallist.insert(0, 0)
        elif (ctr == 'Feb'):
            monthlist.insert(1, 'Feb')
            currentyearleadtotallist.insert(1, 0)
        elif (ctr == 'Mar'):
            monthlist.insert(2, 'Mar')
            currentyearleadtotallist.insert(2, 0)
        elif (ctr == 'Apr'):
            monthlist.insert(3, 'Apr')
            currentyearleadtotallist.insert(3, 0)
        elif (ctr == 'May'):
            monthlist.insert(4, 'May')
            currentyearleadtotallist.insert(4, 0)
        elif (ctr == 'Jun'):
            monthlist.insert(5, 'Jun')
            currentyearleadtotallist.insert(5, 0)
        elif (ctr == 'Jul'):
            monthlist.insert(6, 'Jul')
            currentyearleadtotallist.insert(6, 0)
        elif (ctr == 'Aug'):
            monthlist.insert(7, 'Aug')
            currentyearleadtotallist.insert(7, 0)
        elif (ctr == 'Sep'):
            monthlist.insert(8, 'Sep')
            currentyearleadtotallist.insert(8, 0)
        elif (ctr == 'Oct'):
            monthlist.insert(9, 'Oct')
            currentyearleadtotallist.insert(9, 0)
        elif (ctr == 'Nov'):
            monthlist.insert(10, 'Nov')
            currentyearleadtotallist.insert(10, 0)
        elif (ctr == 'Dec'):
            monthlist.insert(11, 'Dec')
            currentyearleadtotallist.insert(11, 0)
        else:
            j = 0
    print(currentyearleadtotallist)
    #Now here we are going to show the data of leads
    context = {
        'contacts': contacts,
        'contacts_list': contacts_list,
        'event_list': event_list,
        'link_contacts_list': link_contacts_list,
        # 'upcoming_event_list': var_current_date.count(var_current_date)
        'currenty': currentyearleadtotallist,
        'current_year': currentyear
    }


    return render(request, 'marketing/dashboard.html', context)


# class for multiple pdf uploading
class BasicUploadView(View):
    def get(self, request):
        print('this runs first')
        obj_pdf_list = Pdf.objects.all()
        return render(self.request, 'marketing/pdf.html', {'pdf': obj_pdf_list})

    def post(self, request):
        print('this runs second')
        form = PdfForm(self.request.POST, self.request.FILES)
        if form.is_valid():
            obj_pdf = form.save()
            data = {'is_valid': True, 'name': obj_pdf.file.name, 'url': obj_pdf.file.url}
        else:
            data = {'is_valid': False}
        return JsonResponse(data)

def clear_database(request):
    for obj_pdf in Pdf.objects.all():
        obj_pdf.file.delete()
        obj_pdf.delete()
    return redirect(request.POST.get('next'))

# function for read data from pdf and write in to database

# from apscheduler.schedulers.background import BackgroundScheduler
# scheduler = BackgroundScheduler()
# INTERVAL = 1
# # scheduler.add_job(fun_read_pdf, 'interval', minutes=30)
# scheduler.add_job(fun_read_pdf, 'interval', seconds=60)
# scheduler.start()

#function for contacts list
@login_required(login_url='/login')
def link_contacts_list(request):
    if (request.user.role == "ADMIN"):
        contacts =Contactlinkedin.objects.all().order_by('name')
    else:
        user = request.user.id
        contacts = Contactlinkedin.objects.filter(created_by=user)
    if request.method == 'GET':
        context = {'contacts': contacts}

        return render(request, 'marketing/linkedinlist/all.html', context)

    if request.method == 'POST':
        data = request.POST
        if data.get('contact_name'):
            contacts = contacts.filter(
                name__icontains=data.get('contact_name'))

        if data.get('contact_position'):
            contacts = contacts.filter(position__icontains=data.get('contact_position'))

        if data.get('contact_email'):
            contacts = contacts.filter(email__icontains=data.get('contact_email'))

        if data.get('contact_company'):
            contacts = contacts.filter(company_name__icontains=data.get('contact_company'))

        if data.get('country'):
            contacts = contacts.filter(country__icontains=data.get('country'))

        if data.get('skills'):
            contacts = contacts.filter(skills__icontains=data.get('skills'))

        context = {'contacts': contacts, 'users': User.objects.all()}
        print(context)
        print("LL")
        return render(request, 'marketing/linkedinlist/all.html', context)

#function for contact details
@login_required
def link_contact_detail(request, contact_id):
    contact_obj = get_object_or_404(Contactlinkedin, pk=contact_id)
    session_contact=request.session['contact_id'] = contact_obj.id
    # session_contact = request.session[contact_obj.id]
    var_stored_msg = linkMessage.objects.all()
    user_role = User.objects.all()

    if request.method == 'POST':
        if  request.POST.get('msg_content'):
            var_message = linkMessage()
            var_message.sender = request.user
            var_message.reciever =request.user
            var_message.msg_content = request.POST.get('msg_content')
            var_message.save()


    if request.method == 'POST':
        return render(request, 'marketing/linkedinlist/chat.html', {'contact_obj': contact_obj,'var_stored_msg':var_stored_msg,'user_role':user_role})
    else:
        return render(request, 'marketing/linkedinlist/chat.html',{'contact_obj': contact_obj,'var_stored_msg':var_stored_msg,'user_role':user_role})

#function for communication store
def func_link_chat_detail(request,user_id):
    user_obj = get_object_or_404(User, pk=user_id)
    session_contact1 = request.session['contact_id']

    session_linkedin1 = get_object_or_404(Contactlinkedin, pk=session_contact1)

    linkMessage.reciever = user_obj
    user_role = User.objects.all()
    obj_stored_msg = linkMessage.objects.raw(f"SELECT id, msg_content, created_at, reciever_id, sender_id, linkedin_contact_id FROM marketing_linkmessage where (linkedin_contact_id={session_contact1})"
    f"and (sender_id={request.user.id} or reciever_id={request.user.id}) and (reciever_id={user_obj.id} or sender_id={user_obj.id})")



    # var_stored_msg = Message.objects.all()

    if request.method == 'POST':
        if request.POST.get('msg_content'):
            var_message = linkMessage()
            var_message.sender = request.user
            var_message.reciever_id = user_obj.id
            var_message.linkedin_contact = session_linkedin1


            if request.POST.get('schedule_later', None) == 'true':
                schedule_date_time = request.POST.get('schedule_date_time', '')
                start_time_object = ddatetime.strptime(
                    schedule_date_time, '%Y-%m-%d %H:%M')

                var_message.schedule_date_time = start_time_object

            var_message.msg_content = request.POST.get('msg_content')
            var_message.save()

    if request.method == 'POST':
        return render(request, 'marketing/linkedinlist/userchat.html',
                      {'var_stored_msg': obj_stored_msg, 'user_role': user_role,'user_obj':user_obj,'session_linkedin1':session_linkedin1,"timezones": TIMEZONE_CHOICES, "settings_timezone": settings.TIME_ZONE,})
    else:
        # print('rocky')
        return render(request, 'marketing/linkedinlist/userchat.html',
                      { 'var_stored_msg': obj_stored_msg, 'user_role': user_role,'user_obj':user_obj,'session_linkedin1':session_linkedin1,"timezones": TIMEZONE_CHOICES, "settings_timezone": settings.TIME_ZONE,})


#function for exporting in excel
def fun_export_users_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="contacts.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('linkedin_Contacts')

    # Sheet header, first row
    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['name', 'email','position', 'company_name','certification','language','skills','country', ]

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)

    # Sheet body, remaining rows
    font_style = xlwt.XFStyle()

    rows = Contactlinkedin.objects.all().values_list('name', 'email', 'position', 'company_name','certification','language','skills','country')
    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response


#function for multiple contacts list
@login_required(login_url='/login')
def contact_lists(request):
    tags = Tag.objects.all()
    if (request.user.role == "ADMIN"):
        queryset = ContactList.objects.all().order_by('name')
    else:
        user = request.user.id
        queryset = ContactList.objects.filter(created_by=user)

    users = User.objects.filter(
        id__in=queryset.values_list('created_by_id', flat=True))
    if request.GET.get('tag'):
        queryset = queryset.filter(tags=request.GET.get('tag'))
    if request.method == 'POST':
        post_tags = request.POST.getlist('tag')

        if request.POST.get('contact_list_name'):
            queryset = queryset.filter(
                name__icontains=request.POST.get('contact_list_name'))

        if request.POST.get('created_by'):
            queryset = queryset.filter(
                created_by=request.POST.get('created_by'))

        if request.POST.get('tag'):
            queryset = queryset.filter(tags__id__in=post_tags)
            request_tags = request.POST.getlist('tag')

    data = {'contact_lists': queryset, 'tags': tags, 'users': users,
            'request_tags': request.POST.getlist('tag'), }
    return render(request, 'marketing/lists/index.html', data)


#function for contacts list
@login_required(login_url='/login')
def contacts_list(request):
    if (request.user.role == "ADMIN"):
        contacts = Contact.objects.all()
    else:
        user = request.user.id
        contacts = Contact.objects.filter(created_by=user)
    users = User.objects.filter(
        id__in=contacts.values_list('created_by_id', flat=True))

    if request.method == 'GET':
        context = {'contacts': contacts, 'users': users}
        return render(request, 'marketing/lists/all.html', context)

    if request.method == 'POST':
        data = request.POST
        if data.get('contact_name'):
            contacts = contacts.filter(
                name__icontains=data.get('contact_name'))

        if data.get('contact_company_name'):
            contacts = contacts.filter(
                company_name__icontains=data.get('contact_company_name'))

        if data.get('contact_position'):
            contacts = contacts.filter(
                position__icontains=data.get('contact_position'))

        if data.get('contact_email'):
            contacts = contacts.filter(email=data.get('contact_email'))

        if data.get('created_by'):
            contacts = contacts.filter(created_by=data.get('created_by'))

        context = {'contacts': contacts, 'users': User.objects.all()}
        return render(request, 'marketing/lists/all.html', context)


#function for create new list
@login_required(login_url='/login')
def contact_list_new(request):
    users=User.objects.all()
    if request.method == "POST":
        form = ContactListForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.created_by = request.user
            instance.save()
            tags = request.POST['tags'].split(
                ',') if request.POST['tags'] else []
            for each in tags:
                tag, _ = Tag.objects.get_or_create(
                    name=each, created_by=request.user)
                instance.tags.add(tag)
            if request.FILES.get('contacts_file'):
                #below is the code to upload the csv file data see tasks.py
                upload_csv_file(form.validated_rows, form.invalid_rows, request.user.id, [instance.id])

            return JsonResponse({'error': False,
                                 'data': form.data},
                                status=status.HTTP_201_CREATED)
        else:
            # return JsonResponse({'error': True, 'errors': form.errors},
            #                     status=status.HTTP_400_BAD_REQUEST)
            return JsonResponse(
                {'error': True, 'errors': form.errors},
                status=status.HTTP_200_OK)
    else:
        data = {'users': users}
        return render(request, 'marketing/lists/new.html', data)


#function for edit contact
@login_required(login_url='/login')
def edit_contact_list(request, pk):
    user = request.user
    try:
        contact_list = ContactList.objects.get(pk=pk)
    except ContactList.DoesNotExist:
        contact_list = ContactList.objects.none()
        return JsonResponse({}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        # contact_lists = ContactList.objects.filter(company=request.company)
        if (user.is_superuser):
            contact_lists = ContactList.objects.all()
        else:
            contact_lists = ContactList.objects.all()

        data = {'contact_list': contact_list, 'contact_lists': contact_lists}
        return render(request, 'marketing/lists/new.html', data)
    else:
        form = ContactListForm(
            request.POST, request.FILES, instance=contact_list)

        if form.is_valid():
            instance = form.save()
            instance.tags.clear()
            tags = request.POST['tags'].split(
                ',') if request.POST['tags'] else []
            for each in tags:
                tag, _ = Tag.objects.get_or_create(
                    name=each, created_by=request.user)
                instance.tags.add(tag)
            if request.FILES.get('contacts_file'):
                upload_csv_file(

                    form.validated_rows, form.invalid_rows, request.user.id, [instance.id])

            return JsonResponse({'error': False,
                                 'data': form.data}, status=status.HTTP_200_OK)
        return JsonResponse({'error': True,
                             'errors': form.errors}, status=status.HTTP_200_OK)


#function for view contact list
@login_required(login_url='/login')
def view_contact_list(request, pk):
    contact_list = get_object_or_404(ContactList, pk=pk)
    contacts = Contact.objects.all()
    if request.POST and request.POST.get("search"):
        contacts = contacts.filter(
            Q(name__icontains=request.POST['search']) |
            Q(email__icontains=request.POST['search']))

    per_page = request.GET.get("per_page", 10)
    paginator = Paginator(contacts.order_by('id'), per_page)
    page = request.GET.get('page', 1)
    try:
        contacts = paginator.page(page)
    except PageNotAnInteger:
        contacts = paginator.page(1)
    except EmptyPage:
        contacts = paginator.page(paginator.num_pages)

    data = {'contact_list': contact_list, "contacts": contacts}
    template_name = "marketing/lists/all.html"
    return render(request, template_name, data)


#function for delete contacts
@login_required(login_url='/login')
def delete_contact_list(request, pk):
    contact_list_obj = get_object_or_404(ContactList, pk=pk)
    # if not (request.user.role == 'ADMIN' or request.user.is_superuser):
    #     raise PermissionDenied

    contact_list_obj.delete()
    redirect_to = reverse('marketing:contact_lists')
    return HttpResponseRedirect(redirect_to)


#function for create new contacts
@login_required(login_url='/login')
def contacts_list_new(request):
    if request.POST:
        form = ContactForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.created_by = request.user
            instance.save()
            for each in json.loads(request.POST['contact_list']):
                instance.contact_list.add(ContactList.objects.get(id=each))
            # return JsonResponse(form.data, status=status.HTTP_201_CREATED)
            return reverse('marketing:contact_list')
        else:
            data = {'error': True, 'errors': form.errors}
    else:
        if (request.user.is_superuser):
            queryset = ContactList.objects.all()
        else:
            queryset = ContactList.objects.all()
        data = {"contact_list": queryset}
    return render(request, 'marketing/lists/cnew.html', data)

#function for edit contacts
@login_required(login_url='/login')
def edit_contact(request, pk):
    url_redirect_to = None
    contact_obj = get_object_or_404(Contact, pk=pk)
    if not (request.user.role == 'ADMIN' or request.user.is_superuser or contact_obj.created_by == request.user or
            (contact_obj.id in request.user.marketing_contactlist.all().values_list('contacts', flat=True))):
        raise PermissionDenied
    if request.method == 'GET':
        form = ContactForm(instance=contact_obj)
        return render(request, 'marketing/lists/edit_contact.html', {'form': form})

    if request.method == 'POST':
        form = ContactForm(request.POST,request.FILES,instance=contact_obj)
        if form.is_valid():
            if form.has_changed():
                if contact_obj.contact_list.count() > 1:
                    contact_list_obj = ContactList.objects.filter(
                        id=request.POST.get('from_url')).first()
                    if contact_list_obj:
                        contact_obj.contact_list.remove(contact_list_obj)
                    updated_contact = ContactForm(request.POST,request.FILES)
                    if updated_contact.is_valid():

                        updated_contact_obj = updated_contact.save()
                        updated_contact_obj.created_by = request.user
                        updated_contact_obj.save()
                        updated_contact_obj.contact_list.add(contact_list_obj)
                else:
                    contact = form.save(commit=False)
                    if form.is_valid():
                        contact.save()
                        form.save_m2m()
            else:
                contact = form.save(commit=False)
                if form.is_valid():
                    contact.save()
                    form.save_m2m()
            if request.POST.get('from_url'):
                return JsonResponse({'error': False,
                                     'success_url': reverse('marketing:contact_list_detail', args=(request.POST.get('from_url'),))})
            return JsonResponse({'error': False, 'success_url': reverse('marketing:contacts_list')})
        else:
            return JsonResponse({'error': True, 'errors': form.errors, })

#function for delete contacts
@login_required(login_url='/login')
def delete_contact(request, pk):
    contact_obj = get_object_or_404(Contact, pk=pk)
    # if not (request.user.role == 'ADMIN' or request.user.is_superuser  or
    #         (contact_obj.id in request.user.marketing_contactlist.all().values_list('contacts', flat=True))):
    #     raise PermissionDenied
    contact_obj.delete()
    if request.GET.get('from_contact'):
        return redirect(reverse('marketing:contact_list_detail', args=(request.GET.get('from_contact'),)))
    return redirect('marketing:contacts_list')


#function for contact details
@login_required(login_url='/login')
def contact_list_detail(request, pk):
    contact_list = get_object_or_404(ContactList, pk=pk)
    # if not (request.user.role == 'ADMIN' or request.user.is_superuser):
    #     raise PermissionDenied
    contacts_list = contact_list.contacts.all().order_by('name')
    # print(contacts_list)
    #here you will get the list of data from the table marketing_contact
    # Message.objects.filter(
    #     Q(reciever=request.user.id) | Q(sender=request.user.id) & Q(linkedin_contact=session_linkedin))
    if request.POST:
        if request.POST.get('name'):
            contacts_list = contacts_list.filter(
                name__icontains=request.POST.get('name'))
        if request.POST.get('email'):
            contacts_list = contacts_list.filter(
                email__icontains=request.POST.get('email'))
        if request.POST.get('company_name'):
            contacts_list = contacts_list.filter(
                company_name__icontains=request.POST.get('company_name'))
        if request.POST.get('position'):
            contacts_list = contacts_list.filter(
                position__icontains=request.POST.get('position'))
    data = {'contact_list': contact_list, "contacts_list": contacts_list}
    print("YOO")

    return render(request, 'marketing/lists/detail.html', data)


#function for failed contacts details
@login_required(login_url='/login')
def failed_contact_list_detail(request, pk):
    contact_list = get_object_or_404(ContactList, pk=pk)
    failed_contacts_list = contact_list.failed_contacts.all()
    data = {'contact_list': contact_list,
            "failed_contacts_list": failed_contacts_list}
    return render(request, 'marketing/lists/failed_detail.html', data)


#function for failed contact list
@login_required(login_url='/login')
def failed_contact_list_download_delete(request, pk):
    contact_list = get_object_or_404(ContactList, pk=pk)
    failed_contacts_list = contact_list.failed_contacts.all()
    if failed_contacts_list.count() > 0:
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="failed_contacts.csv"'
        writer = csv.writer(response)
        writer.writerow(["company name", "email", "first name",
                         "last name", "city", "state"])

        for contact in failed_contacts_list:
            writer.writerow([
                contact.company_name, contact.email, contact.name, contact.last_name, contact.city, contact.state])
        failed_contacts_list.delete()
        return response
    else:
        return HttpResponseRedirect(reverse('marketing:contact_lists', kwargs={"pk": pk}))


#function for logo image
@lru_cache()
def logo_data():
    with open(finders.find('f:/Bisp-linkedin-data/bisenv/Django-CRM-master/static/images/logo.png'), 'rb') as f:
        logo_data = f.read()
    logo = MIMEImage(logo_data)
    logo.add_header('Content-ID', '<logo>')
    return logo


#function for download demo csv
def demo_file_download(request):
    sample_data = [
        'First Name, Last Name, Email, Company, Designation, Date\n',
        'fname1, lname1, user1@email.com, cname1, dname1, date\n',
        'fname2, lname2, user2@email.com, cname2, dname2, date\n',
        'fname3, lname3, user3@email.com, cname3, dname4, date\n',
        'fname4, lname4, user4@email.com, cname4, dname4, date\n'
    ]
    response = HttpResponse(
        sample_data, content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename={}'.format(
        'sample_data.csv')
    return response

#function for contact details
@login_required
def contact_detail(request, contact_id):
    contact_obj = get_object_or_404(Contact, pk=contact_id)
    session_contact=request.session['contact_id'] = contact_obj.id
    # session_contact = request.session[contact_obj.id]
    print(session_contact)


    var_stored_msg = Message.objects.all()
    user_role = User.objects.all()

    if request.method == 'POST':
        if  request.POST.get('msg_content'):
            var_message = Message()
            var_message.sender = request.user
            var_message.reciever =request.user
            var_message.msg_content = request.POST.get('msg_content')
            var_message.save()
            # print(var_message.msg_content)



    if request.method == 'POST':
        return render(request, 'marketing/lists/chat.html', {'contact_obj': contact_obj,'var_stored_msg':var_stored_msg,'user_role':user_role})
    else:
        return render(request, 'marketing/lists/chat.html',
                      {'contact_obj': contact_obj,'var_stored_msg':var_stored_msg,'user_role':user_role})


#function for communication store
def func_chat_detail(request,user_id):
    user_obj = get_object_or_404(User, pk=user_id)
    session_contact = request.session['contact_id']

    session_linkedin = get_object_or_404(Contact, pk=session_contact)

    Message.reciever = user_obj
    user_role = User.objects.all()

    obj_stored_msg1 =  Message.objects.filter(Q(reciever=request.user.id )|Q(sender=request.user.id)&Q(linkedin_contact=session_linkedin))
    obj_stored_msg=Message.objects.raw(f"SELECT id, msg_content, created_at, reciever_id, sender_id, linkedin_contact_id FROM marketing_message where (linkedin_contact_id={session_contact}) "
    f"and (sender_id={request.user.id} or reciever_id={request.user.id}) and (reciever_id={user_obj.id} or sender_id={user_obj.id})")
    print(obj_stored_msg)

    if request.method == 'POST':
        if request.POST.get('msg_content'):
            var_message = Message()
            var_message.sender = request.user

            var_message.reciever_id = user_obj.id
            var_message.linkedin_contact= session_linkedin


            if request.POST.get('schedule_later', None) == 'true':
                schedule_date_time = request.POST.get('schedule_date_time', '')
                start_time_object = ddatetime.strptime(
                    schedule_date_time, '%Y-%m-%d %H:%M')
                print(start_time_object)


                var_message.schedule_date_time = start_time_object

            var_message.msg_content = request.POST.get('msg_content')
            var_message.save()

    if request.method == 'POST':
        return render(request, 'marketing/lists/userchat.html',
                      {'var_stored_msg': obj_stored_msg, 'user_role': user_role,'user_obj':user_obj,'session_linkedin':session_linkedin,"timezones": TIMEZONE_CHOICES, "settings_timezone": settings.TIME_ZONE,})
    else:
        return render(request, 'marketing/lists/userchat.html',
                      { 'var_stored_msg': obj_stored_msg, 'user_role': user_role,'user_obj':user_obj,'session_linkedin':session_linkedin,"timezones": TIMEZONE_CHOICES, "settings_timezone": settings.TIME_ZONE,})

#function for displaying calendar
def fun_calendar(request):
    obj_all_events = Event.objects.all()
    context = {
        "events":obj_all_events,
    }
    return render(request,'marketing/lists/calendar.html',context)


#function for add event
def fun_add_event(request):
    var_start = request.GET.get("start", None)
    var_end = request.GET.get("end", None)
    var_title = request.GET.get("title", None)
    var_description = request.GET.get("description", None)

    obj_event = Event(title=str(var_title) ,description=str(var_description),start_time=var_start, end_time=var_end,user_id=request.user.id)
    obj_event.save()
    data = {}
    return JsonResponse(data)

#function for update event
def fun_update(request):
    var_start = request.GET.get("start", None)
    var_end = request.GET.get("end", None)
    var_title = request.GET.get("title", None)
    var_id = request.GET.get("id", None)
    obj_event = Event.objects.get(id=var_id)
    obj_event.start = var_start
    obj_event.end = var_end
    obj_event.name = var_title
    obj_event.save()
    data = {}
    return JsonResponse(data)


#function for remove event
def fun_remove(request):
    var_id = request.GET.get("id", None)
    obj_event = Event.objects.get(id=var_id)
    obj_event.delete()
    data = {}
    return JsonResponse(data)



#function for mail sending
def fun_mail():
    event_obj = Event.objects.all()
    # event_obj = Event.objects.filter(end_time=datetime.now()-timedelta(days=1))
    print(event_obj)
    # date_var = event_obj.end_time - timedelta(days=1)

    # print(minus_date)
    var_today = datetime.now().strftime('%Y-%m-%d')
    print(var_today)

    for user in event_obj:
        obj_user_date = user.end_time - timedelta(days=1)
        var_current_date = obj_user_date.strftime('%Y-%m-%d')
        if var_current_date == var_today:
            print("hiiiiii")
            mail_subject = 'Remainder!!!!! followup mail'
            from_email = settings.EMAIL_HOST_USER
            email = EmailMessage(mail_subject, from_email, to=[user.user.email])
            # email.content_subtype = "html"
            email.send()

from apscheduler.schedulers.background import BackgroundScheduler

# scheduler = BackgroundScheduler()
# scheduler.add_job(fun_mail, 'interval', seconds=5)
# scheduler.start()

@login_required(login_url='/login')
def edit_failed_contact(request, pk):
    contact_obj = get_object_or_404(FailedContact, pk=pk)
    if not (request.user.role == 'ADMIN' or request.user.is_superuser or contact_obj.created_by == request.user):
        raise PermissionDenied
    if request.method == 'GET':
        form = ContactForm(instance=contact_obj)
        return render(request, 'marketing/lists/edit_contact.html', {'form': form})

    if request.method == 'POST':
        form = ContactForm(request.POST, instance=contact_obj)
        if form.is_valid():
            contact = form.save(commit=False)
            contact.save()
            form.save_m2m()
            return JsonResponse({'error': False, 'success_url': reverse('marketing:contacts_list')})
        else:
            return JsonResponse({'error': True, 'errors': form.errors, })


@login_required(login_url='/login')
def delete_failed_contact(request, pk):
    contact_obj = get_object_or_404(FailedContact, pk=pk)
    if not (request.user.role == 'ADMIN' or request.user.is_superuser or contact_obj.created_by == request.user):
        raise PermissionDenied
    if request.method == 'GET':
        contact_obj.delete()
        return redirect('marketing:contacts_list')

#######below functions written by Ashwin Juwekar

@login_required(login_url='/login')
def event_list_detail(request,pk):
    event_detail = Event.objects.get(id=pk)
    # print(event_detail.title)

    return render(request,'marketing/lists/event_detail.html',{'event_lists': event_detail})
# ,'event_complete_detail':event_complete_detail

