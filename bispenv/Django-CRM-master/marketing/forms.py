import csv
import datetime
import re
import json
import openpyxl
from django import forms
from .models import Pdf
from common.models import User
from marketing.models import (
    ContactList, Contact,
     Tag,Event
)

email_regex = '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$'

def csv_doc_validate(document):
    temp_row = []
    invalid_row = []
    reader = csv.reader((document.read().decode( encoding = "ISO-8859-1")).splitlines())


    csv_headers = ["first name","last name"]

    required_headers = ["first name"]
    for y_index, row in enumerate(reader):
        each = {}
        invalid_each = {}
        if y_index == 0:
            csv_headers = [header_name.lower()
                           for header_name in row if header_name]
            missing_headers = set(required_headers) - \
                set([r.lower() for r in row])
            if missing_headers:
                missing_headers_str = ', '.join(missing_headers)
                message = 'Missing headers: %s' % (missing_headers_str)
                return {"error": True, "message": message}
            continue
        elif not ''.join(str(x) for x in row):
            continue
        else:
            for x_index, cell_value in enumerate(row):
                try:
                    csv_headers[x_index]
                except IndexError:
                    continue
                if csv_headers[x_index] in required_headers:
                    if not cell_value:
                        invalid_each[csv_headers[x_index]] = cell_value
                    else:
                        if csv_headers[x_index] == "email":
                            if re.match(email_regex,cell_value) is None:
                                invalid_each[csv_headers[x_index]] = cell_value
                each[csv_headers[x_index]] = cell_value
        if invalid_each:
            invalid_row.append(each)
        else:
            temp_row.append(each)
    return {"error": False, "validated_rows": temp_row, "invalid_rows": invalid_row}


def get_validated_rows(wb, sheet_name, validated_rows, invalid_rows):
    headers = ["first name"]
    required_headers = ["first name"]
    work_sheet = wb.get_sheet_by_name(name=sheet_name)
    for y_index, row in enumerate(work_sheet.iter_rows()):
        if y_index == 0:
            missing_headers = set(required_headers) - \
                set([str(cell.value).lower() for cell in row])
            if missing_headers:
                missing_headers_str = ', '.join(missing_headers)
                message = "Missing headers: %s %s" % (
                    missing_headers_str, sheet_name)
                return {"error": True, "message": message}
            continue
        elif not ''.join(str(cell.value) for cell in row):
            continue
        else:
            temp_row = []
            invalid_row = []
            for x_index, cell in enumerate(row):
                try:
                    headers[x_index]
                except IndexError:
                    continue
                if headers[x_index] in required_headers:
                    if not cell.value:
                        invalid_row.append(headers[x_index])
                temp_row.append(cell.value)
            if len(invalid_row) > 0:
                invalid_rows.append(temp_row)
            else:
                if len(temp_row) >= len(required_headers):
                    validated_rows.append(temp_row)
    return validated_rows, invalid_rows


def xls_doc_validate(document):
    wb = openpyxl.load_workbook(document)
    sheets = wb.get_sheet_names()
    validated_rows = []
    invalid_rows = []
    for sheet_name in sheets:
        validated_rows, invalid_rows = get_validated_rows(
            wb, sheet_name, validated_rows, invalid_rows)
    return {"error": False, "validated_rows": validated_rows, "invalid_rows": invalid_rows}


def import_document_validator(document):
    try:
        document.seek(0, 0)
        return csv_doc_validate(document)
    except Exception as e:
        print(e)
        try:
            return xls_doc_validate(document)
        except Exception as e:
            print(e)
            return {"error": True, "message": "Not a valid CSV/XLS file"}




class PdfForm(forms.ModelForm):
    class Meta:
        model = Pdf
        fields = ('file', )


class ContactListForm(forms.ModelForm):
    tags = forms.CharField(max_length=5000, required=False)
    contacts_file = forms.FileField(required=False)

    class Meta:
        model = ContactList
        fields = ["name"]

    def __init__(self, *args, **kwargs):
        super(ContactListForm, self).__init__(*args, **kwargs)
        self.fields['contacts_file'].widget.attrs.update({
            "accept": ".csv,.xls,.xlsx,.xlsm,.xlsb,.xml",
        })
        self.fields['contacts_file'].required = True
        # if self.instance is None:
        #     self.fields['contacts_file'].required = True
        if self.data.get('contacts_file'):
            self.fields['contacts_file'].widget.attrs.update({
                "accept": ".csv,.xls,.xlsx,.xlsm,.xlsb,.xml",
            })

    def clean_contacts_file(self):
        document = self.cleaned_data.get("contacts_file")
        if document:
            data = import_document_validator(document)
            if data.get("error"):
                raise forms.ValidationError(data.get("message"))
            else:
                self.validated_rows = data.get("validated_rows", [])
                self.invalid_rows = data.get("invalid_rows", [])
                if self.invalid_rows:
                    raise forms.ValidationError('Uploaded file is not valid')
        return document

    def clean_visible_to(self):
        visible_to_data = json.loads(self.data['visible_to'])
        if len(visible_to_data) > 0:
            instance_visible_to = []
            if self.instance and self.instance.id is not None:
                instance_visible_to = list(
                    set(self.instance.visible_to.all().values_list(
                        'email', flat=True)))
            for each in visible_to_data:
                visible_to = User.objects.filter(email=each)
                if instance_visible_to:
                    visible_to = visible_to.exclude(
                        email__in=instance_visible_to)
                if visible_to:
                    raise forms.ValidationError(
                        str(each) + ' User aleady existed')
            return self.data['visible_to']
        raise forms.ValidationError("Select any of the users")

class ContactForm(forms.ModelForm):
    contact_list = forms.CharField(max_length=20000)


    def __init__(self, *args, **kwargs):
        request_user = kwargs.pop('request_user', None)
        self.obj_instance = kwargs.get('instance', None)
        super(ContactForm, self).__init__(*args, **kwargs)



        for field in self.fields.values():
            field.widget.attrs = {"class": "form-control"}

        self.fields['name'].required = True
        self.fields['last_name'].required = True
        self.fields['city'].required = True
        self.fields['state'].required = True
        self.fields['email'].required = True
        self.fields['company_name'].required = True
        self.fields['pdf_file'].widget.attrs.update({
            "accept": ".pdf",
        })
        self.fields['pdf_file'].required = True



        self.fields['contact_list'].required = False


    class Meta:
        model = Contact
        fields = ["name", "email", "contact_number",
                  "last_name", "city", "state","company_name","pdf_file"]

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if Contact.objects.filter(email=email).exclude(id=self.instance.id).exists():
            raise forms.ValidationError(
                'Contact with this Email already exists')
        return email

from django.forms import ModelForm, DateInput


class EventForm(ModelForm):
  class Meta:
    model = Event
    # datetime-local is a HTML5 input type, format to make date time show on fields
    widgets = {
      'start_time': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
      'end_time': DateInput(attrs={'type': 'datetime-local'}, format='%Y-%m-%dT%H:%M'),
    }
    fields = '__all__'

  def __init__(self, *args, **kwargs):
    super(EventForm, self).__init__(*args, **kwargs)
    # input_formats parses HTML5 datetime-local input to datetime field
    self.fields['start_time'].input_formats = ('%Y-%m-%dT%H:%M',)
    self.fields['end_time'].input_formats = ('%Y-%m-%dT%H:%M',)

class ContactsCSVUploadForm(forms.Form):
    contacts_file = forms.FileField()
    contact_list = forms.CharField(max_length=20000)

    def __init__(self, *args, **kwargs):
        super(ContactsCSVUploadForm, self).__init__(*args, **kwargs)
        self.fields['contacts_file'].widget.attrs.update({
            "accept": ".csv,.xls,.xlsx,.xlsm,.xlsb,.xml",
        })
        self.fields['contacts_file'].required = True

    def clean_contacts_file(self):
        document = self.cleaned_data.get("contacts_file")
        data = import_document_validator(document)
        if data.get("error"):
            raise forms.ValidationError(data.get("message"))
        else:
            self.validated_rows = data.get("validated_rows")
        return document

    # def clean_contact_list(self):
    #     contact_list = self.cleaned_data.get("contact_list")
    #     if not contact_list or contact_list == '[]' or\
    #             json.loads(contact_list) == []:
    #         raise forms.ValidationError(
    #             "Please choose any of the Contact List")
    #     else:
    #         for each in json.loads(contact_list):
    #             if not ContactList.objects.filter(id=each).first():
    #                 raise forms.ValidationError(
    #                     "Please choose a valid Contact List")
    #
    #     return contact_list



