import datetime
import hashlib

from celery.utils.log import get_task_logger

import requests
from celery.task import task
from django.conf import settings
from django.core.mail import EmailMessage

from marketing.models import (Contact, ContactList,
                              FailedContact)


@task
def campaign_sechedule(request):
    pass


@task
def campaign_open(request):
    pass


@task
def campaign_click(request):
    pass


@task
def upload_csv_file(data, invalid_data, user, contact_lists):
    for each in data:
        # print(each)
        # print(type(each))
        # contact = Contact.objects.filter(name=each['first name']).first()
        # if not contact:
        contact = Contact.objects.create(
                created_by_id=user,
                name=each['first name'])
        if each.get('email', None):
            contact.email = each['email']
        if each.get('last name', None):
            contact.last_name = each['last name']
        if each.get('company name', None):
            contact.company_name = each['company name']
        if each.get('city', None):
            contact.city = each['city']
        if each.get("state", None):
            contact.state = each['state']
        if each.get("date", None):
            contact.date = each['date']
        if each.get("Country", None):
            contact.country = each['Country']
        if each.get("course", None):
            contact.Course = each['course']
        if each.get("lead source", None):
            contact.Lead_Source = each['lead source']
        if each.get("phone", None):
            contact.contact_number = each['phone']
        if each.get("position", None):
            contact.position = each['position']
        contact.save()
        for contact_list in contact_lists:
            contact.contact_list.add(ContactList.objects.get(id=int(contact_list)))

    for each in invalid_data:
        contact = FailedContact.objects.filter(email=each['email']).first()
        if not contact:
            contact = FailedContact.objects.create(
                email=each['email'], created_by_id=user,
                name=each['first name'])
            if each.get('company name', None):
                contact.company_name = each['company name']
            if each.get('last name', None):
                contact.last_name = each['last name']
            if each.get('city', None):
                contact.city = each['city']
            if each.get("state", None):
                contact.state = each['state']
            if each.get("Date", None):
                contact.Date = each['Date']
            if each.get("Country", None):
                contact.country = each['Country']
            if each.get("Course", None):
                contact.Course = each['Course']
            if each.get("Lead Source", None):
                contact.Lead_Source = each['Lead Source']
            if each.get("Phone", None):
                contact.contact_number = each['Phone']
            contact.save()
        for contact_list in contact_lists:
            contact.contact_list.add(ContactList.objects.get(id=int(contact_list)))


def send_campaign_mail(subject, content, from_email, to_email, bcc, reply_to, attachments):
    msg = EmailMessage(
        subject,
        content,
        from_email,
        to_email,
        bcc,
        reply_to=reply_to,
    )
    for attachment in attachments:
        msg.attach(*attachment)
    msg.content_subtype = "html"
    res = msg.send()
    print(res)


def get_campaign_message_id(campaign):
    hash_ = hashlib.md5()
    hash_.update(
        str(str(campaign.id) + str(campaign.campaign.created_by.id)).encode('utf-8') +
        str(datetime.datetime.now()).encode('utf-8')
    )
    file_hash = hash_.hexdigest()
    return file_hash



@task
def list_all_bounces_unsubscribes():
    bounces = requests.get('https://api.sendgrid.com/api/bounces.get.json?api_user=' +
                           settings.EMAIL_HOST_USER + '&api_key=' + settings.EMAIL_HOST_PASSWORD)
    for each in bounces.json():
        if type(each) == dict:
            contact = Contact.objects.filter(email=each.get('email')).first()
            if contact:
                contact.is_bounced = True
                contact.save()

    bounces = requests.get('https://api.sendgrid.com/api/unsubscribes.get.json?api_user=' +
                           settings.EMAIL_HOST_USER + '&api_key=' + settings.EMAIL_HOST_PASSWORD)
    for each in bounces.json():
        if type(each) == dict:
            contact = Contact.objects.filter(email=each.get('email')).first()
            if contact:
                contact.is_unsubscribed = True
                contact.save()





logger = get_task_logger(__name__)






