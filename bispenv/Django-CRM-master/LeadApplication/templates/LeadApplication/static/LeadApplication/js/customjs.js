function setPreference()
        {
           if($('#newpreference').val() == '')
                {
                    if($('#preference').val() != '' )
                    {
                        var existing_pref = $('#preference').val();
                        if (confirm('you are going to update the existing preference:'+existing_pref)) {
                            // Save it!
                        } else {
                            return false;
                        }
                        var preference =  $('#preference').val();
                    }
                    else
                    {
                        $('#success_msg').addClass('alert-danger');
                        $('.success_msg_body').text('Preference should not be empty');
                        $("#success_msg").fadeIn("slow");
                        $("#success_msg").delay(5000).fadeOut("slow");
                        document.getElementById("newpreference").focus();
                        return false;
                    }

                }
           else
                {
                    var preference =  $('#newpreference').val();
                }

            var country_dropdown = $("#listcountry option:selected");
            var country_selected = "";
            country_dropdown.each(function () { country_selected += $(this).text() +"_"; });
            var technology_dropdown = $("#listtechnology option:selected");
            var technology_selected = "";
            technology_dropdown.each(function () { technology_selected += $(this).text() +"_"; });
            //var preference = document.getElementById('newpreference').value;   //$('#preference').val();
            if(country_selected == " " || technology_selected == "")
            {
                alert('Company Or Technology is null , they are necessary');
                return false;
            }

            $.ajax({
                           type:'POST',
                           url:'/Leadanalytics/fun_setPreference/',
                           data:{
                                country:    country_selected,
                                course:     technology_selected,
                                fromMonth:  $('#frommonth').val(),
                                toMonth:    $('#tomonth').val(),
                                preference : preference,

                                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                                },
                           success:function(msg)
                                   {
                                       $('.success_msg_body').text(msg);
                                       $("#success_msg").fadeIn("slow");
                                       $("#success_msg").delay(5000).fadeOut("slow");
                                   }
                      });


        }

 function searchword(char)
 {

         $.ajax({
               type:'POST',
               url:'/Leadanalytics/getRelatedWords/',
               data:{
                    char: char,
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                    },
               success:function(msg)
                       {

                           $('#freesearchdiv').html(msg);
                           $('#freesearchdiv').show();
                       }
             });

 }

 function hideFreeSearchBox()
    {
    $("#freesearchdiv").hide();
    }



function selectedWord1(arg)
 {
    $('#searchText').val(arg);
    $('#freesearchdiv').hide();
 }


////this below code is for the simple view of the analytics the above code is for the advanced analytics
// function getSelectedIntervalData(selectedValue)
//        {
//
//                    $.ajax({
//                           type:'POST',
//                           url:'/Leadanalytics/getSelectedIntervalChart/',
//                           data:{
//                                timeInterval:selectedValue,
//                                },
//                           success:function(msg)
//                               {
//
//                               if ($('#second').length > 0) {
//
//                                    document.getElementById('second').remove();
//
//                                    }
//                             $("#choiceInterval").append("<img id='second' src='data:image/png;base64,'/>");
//                            document.getElementById('second').setAttribute('src', 'data:image/png;base64,'+msg);
//
//                               }
//                      });
//        }



