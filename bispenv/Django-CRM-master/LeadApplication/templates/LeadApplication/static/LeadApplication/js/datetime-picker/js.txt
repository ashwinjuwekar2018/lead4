<script>

    $(".date-time").calentim({
        singleDate: true,
        calendarCount: 1,
        showHeader: false,
        showFooter: false,
        autoCloseOnSelect: true,
        reverseTimepickerArrows: true,
        oneCalendarWidth:310,
        startEmpty: true,
        format: "YYYY-MM-DD HH:mm",
        hourFormat: 24,
        minuteSteps: 15
    });

    $(".date").calentim({
        singleDate: true,
        calendarCount: 1,
        showHeader: false,
        showFooter: false,
        autoCloseOnSelect: true,
        oneCalendarWidth:310,
        reverseTimepickerArrows: true,
        startEmpty: true,
        showTimePickers: false,
        format: "YYYY-MM-DD"
    });
    $(".time").calentim({
        singleDate: true,
        calendarCount: 1,
        //startEmpty: true,
        showCalendars: false,
        format: "HH:mm",
        minuteSteps: 15,
        showButtons: true
    });

    $(".range-dp").calentim({
        autoCloseOnSelect: true,
        startEmpty: true,
        format: "YYYY-MM-DD HH:mm",
        hourFormat: 24,
        minuteSteps: 15
    });
    $(".range-date").calentim({
        showTimePickers: false,
        autoCloseOnSelect: true,
        startEmpty: true,
        format: "YYYY-MM-DD"
    });

</script>