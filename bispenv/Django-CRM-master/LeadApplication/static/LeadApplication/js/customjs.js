function setPreference()
        {
           if($('#newpreference').val() == '')
                {
                    if($('#preference').val() != '' )
                    {
                        var existing_pref = $('#preference').val();
                        if (confirm('you are going to update the existing preference:'+existing_pref)) {
                            // Save it!
                        } else {
                            return false;
                        }
                        var preference =  $('#preference').val();
                    }
                    else
                    {
                        $('#success_msg').addClass('alert-danger');
                        $('.success_msg_body').text('Preference should not be empty');
                        $("#success_msg").fadeIn("slow");
                        $("#success_msg").delay(5000).fadeOut("slow");
                        document.getElementById("newpreference").focus();
                        return false;
                    }

                }
           else
                {
                    var preference =  $('#newpreference').val();
                }

            var country_dropdown = $("#listcountry option:selected");
            var country_selected = "";
            country_dropdown.each(function () { country_selected += $(this).text() +"_"; });
            var technology_dropdown = $("#listtechnology option:selected");
            var technology_selected = "";
            technology_dropdown.each(function () { technology_selected += $(this).text() +"_"; });
            //var preference = document.getElementById('newpreference').value;   //$('#preference').val();
            if(country_selected == " " || technology_selected == "")
            {
                alert('Company Or Technology is null , they are necessary');
                return false;
            }

            $.ajax({
                           type:'POST',
                           url:'/Leadanalytics/fun_setPreference/',
                           data:{
                                country:    country_selected,
                                course:     technology_selected,
                                fromMonth:  $('#frommonth').val(),
                                toMonth:    $('#tomonth').val(),
                                preference : preference,

                                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                                },
                           success:function(msg)
                                   {
                                       $('.success_msg_body').text(msg);
                                       $("#success_msg").fadeIn("slow");
                                       $("#success_msg").delay(5000).fadeOut("slow");
                                   }
                      });


        }

 function searchword(char)
 {

         $.ajax({
               type:'POST',
               url:'/Leadanalytics/getRelatedWords/',
               data:{
                    char: char,
                    csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                    },
               success:function(msg)
                       {

                           $('#freesearchdiv').html(msg);
                           $('#freesearchdiv').show();
                       }
             });

 }

 function hideFreeSearchBox()
    {
    $("#freesearchdiv").hide();
    }



function selectedWord1(arg)
 {
    $('#searchText').val(arg);
    $('#freesearchdiv').hide();



 }
function getChart()
{
    var keyword = $("#searchText").val();
     $.ajax({
                           type:'POST',
                           url:'/Leadanalytics/getKeywordChart/',
                           data:{
                                keyword:keyword,
                                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
                                },
                           success:function(msg)
                                   {
                                     if(msg == '0')
                                        {
                                            $("#freesearchgraph").show();
                                            $("#graphdiv4").html('You can search by Country,Company,Location,Category and Source only');
                                        }
                                        else
                                        {
                                           $("#freesearchgraph").show();
                                           $("#graphdiv4").html(msg);
                                           $("#graphdiv4").append('<div  class="col-lg-2 col-md-2 text:center" ><a href="http://192.168.1.7:8000/Leadanalytics/download_csv/Choice" class="form-check-input  btn btn-success" >Download Excel</a></div>')
                                        }
                                     }
                      });

}

