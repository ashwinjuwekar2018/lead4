from django.apps import AppConfig


class LeadapplicationConfig(AppConfig):
    name = 'LeadApplication'
