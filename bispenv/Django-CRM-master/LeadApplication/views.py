import base64
import csv
import datetime
import random
import xml
from flask import Flask, render_template, make_response, send_file, app, Response
from bokeh.core.property.dataspec import value
from bokeh.io import show, output_file
from bokeh.layouts import column,row
from bokeh.transform import dodge, factor_cmap
from django.shortcuts import render,render_to_response
from django.http import HttpResponse, HttpResponseRedirect, request, response
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import pandas as pd
from bokeh.palettes import Dark2_5 as palette
import itertools
import numpy as np
from bokeh.models import ColumnDataSource, LabelSet, CustomJS, Row, RadioGroup, RadioButtonGroup, OpenURL, TapTool,Button
import matplotlib
from matplotlib.pyplot import figure, legend
from bokeh.models.tools import HoverTool
from bokeh.embed import components
from bokeh.plotting import figure, output_file, show, ColumnDataSource


from bokeh.models import ColumnDataSource

from django.core.files.storage import FileSystemStorage
from flask import jsonify

from io import BytesIO
import xml.etree.ElementTree as ET

from math import pi
from array import *
import cgi

# from crm import  settings
form = cgi.FieldStorage()

#global variable
global default_preference,script1,div1,var_selected_data
var_selected_data=''
default_preference=''

def fun_homePage(request):
    return render(request,'LeadApplication/homepage_old.html')




# @csrf_exempt
def fun_leadAnalytics(request,changed_prefrence):
    # default_preference = changed_prefrence  #'Preference Ten'
    # tree = ET.parse('filename4.xml')
    # root = tree.getroot()
    var_preference_list = []
    # allchoices = []
    all_country_prefered = ''
    all_technology_prefered = ''
    frommonth = 'Jan'
    tomonth = 'Dec'
    prefname=''


    # the above code is for the xml document save and display according to the preference
    var_readcsv = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\csv_of_leads_final.csv',encoding='latin1')

    var_readcsv.dropna(how="all", inplace=True)

    var_readcsv['Year'] = pd.DatetimeIndex(var_readcsv['Date']).year
    var_readcsv['MonthNumber'] = pd.DatetimeIndex(var_readcsv['Date']).month
    var_readcsv['Month'] = pd.DatetimeIndex(var_readcsv['Date']).month
    var_readcsv['Weeknumber'] = pd.DatetimeIndex(var_readcsv['Date']).week
    var_readcsv['Datenumber'] = pd.DatetimeIndex(var_readcsv['Date']).day
    var_readcsv['Lead'] = "1"
    # var_readcsv = var_readcsv.sort_values(by='MonthNumber', ascending=True)
    # print(var_readcsv)
    # print('before')
    look_up2 = {1:'Jan', 2: 'Feb', 3: 'Mar', 4: 'Apr', 5: 'May', 6: 'Jun', 7: 'Jul', 8: 'Aug', 9: 'Sep', 10: 'Oct',11:'Nov',12:'Dec'}
    var_readcsv['Month'] = var_readcsv['Month'].apply(lambda x: look_up2[x])
    updated_csv = var_readcsv.dropna(axis=0, how='any')
    updated_csv.to_csv('media/csv_of_leads_final.csv',mode='w', index=False)
    var_unique_month = updated_csv['Month'].unique()
    var_unique_country = updated_csv['Country'].unique()
    var_unique_courses = updated_csv['Technology'].unique()

    v_csvRead = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')

    # get the pivoted table
    v_pivotedTable = v_csvRead.pivot_table(index='Month', columns='Technology', values='Lead', aggfunc=np.sum,fill_value=0)
    # now get the list of the column heads i.e.courselist headings
    v_courseHeaderList = list(v_pivotedTable.head(2))
    # here the id of each columndatasource is created
    print(v_courseHeaderList)
    v_pivotedTableSource = ColumnDataSource(pd.DataFrame(v_pivotedTable))

    # here the list of months are received now we can write only column head
    monthseriallist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    monthlists = v_pivotedTableSource.data['Month'].tolist()
    print(monthlists)
    #this is the intersection we are applying below
    monthlist = [value for value in monthseriallist if value in monthlists]
    # figure is created here
    monthwise = figure(x_range=monthlist, plot_width=530, plot_height=400,tools=['save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                       toolbar_location="below")
    monthwise.xaxis.major_label_orientation = pi / 4
    courseCount = len(v_courseHeaderList)
    number_of_colors = courseCount
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    colorList = color
    # here the stacked bar is created
    v_stack = monthwise.vbar_stack(stackers=v_courseHeaderList, x='Month', color=colorList, source=v_pivotedTableSource,legend=[value(x) for x in v_courseHeaderList],
                                   name=v_courseHeaderList,
                                   width=0.3 )
    for v_counter in v_stack:
        v_technology = v_counter.name
        hover = HoverTool(tooltips=[
            ("%s " % v_technology, "@%s" % v_technology),
        ], renderers=[v_counter])
        monthwise.add_tools(hover)
    # this is the final step
    v_figureLayout = column(monthwise)
    script, div = components(v_figureLayout)
    #==========================================================
    # Country Wise Technology leads
    v_csvRead = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
    # get the pivoted table
    v_pivotedTable = v_csvRead.pivot_table(index='Country', columns='Technology', values='Lead',aggfunc=np.sum, fill_value=0)
    # now get the list of the column heads i.e.courselist headings
    v_courseHeaderList = list(v_pivotedTable.head(2))
    # here the id of each columndatasource is created
    v_pivotedTableSource = ColumnDataSource(pd.DataFrame(v_pivotedTable))
    # here the list of countries are received now we can write only column head
    v_countryList = v_pivotedTableSource.data['Country'].tolist()
    # figure is created here
    v_graphFigure = figure(x_range=v_countryList, plot_width=530, plot_height=400,
                           tools=['save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                           toolbar_location="below")
    v_graphFigure.xaxis.major_label_orientation = pi / 4
    courseCount = len(v_courseHeaderList)
    number_of_colors = courseCount
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    colorList = color
    # here the stacked bar is created
    v_stack = v_graphFigure.vbar_stack(stackers=v_courseHeaderList, x='Country', color=colorList,source=v_pivotedTableSource, legend=[value(x) for x in v_courseHeaderList],
                                       name=v_courseHeaderList,
                                       width=0.3 )
    # whatever you want to show on bar hover this is the fixed for loop format it cant be changed
    for v_counter in v_stack:
        v_technology = v_counter.name
        hover = HoverTool(tooltips=[
            ("%s " % v_technology, "@%s" % v_technology),
        ], renderers=[v_counter])
        v_graphFigure.add_tools(hover)
    # this is the final step
    v_figureLayout = column(v_graphFigure)
    script1, div1 = components(v_figureLayout)
    # Default graph that we are showing

    return render(request,'LeadApplication/lead_analytics.html',{'months':var_unique_month,'countries':var_unique_country,'all_country_prefered':all_country_prefered,'all_technology_prefered':all_technology_prefered,'frommonth':frommonth,'tomonth':tomonth,'prefname':prefname,'preference':var_preference_list,'courses':var_unique_courses,'script':script,'div':div,'script1':script1,'div1':div1})

# def fun_joy(request):
#     return HttpResponse('hello function joy')

@csrf_exempt
def fun_select_parameters(request,currentAccordId):

     # if extr=='extrastring':
     #    return HttpResponse("OKOK we received your data")

     if request.method == 'POST':
         var_country= request.POST['country']
         var_course= request.POST['course']
         var_fromMonth = request.POST['fromMonth']
         var_toMonth = request.POST['toMonth']
         var_fromYear = request.POST['fromYear']
         var_toYear = request.POST['toYear']
         var_chartoption = request.POST['chartoptions']

         #now dynamically we have to create he graph here
         countrylst = var_country.split('_')
         techlst = var_course.split('_')
         countrylst = countrylst[:-1]
         techlst = techlst[:-1]
         selectionlist =[countrylst,techlst,var_fromMonth,var_toMonth,var_fromYear,var_toYear,var_chartoption]
         if currentAccordId == 'headingEight':
             resultant_data = fun_dynamicGraph(selectionlist)
         elif currentAccordId == 'headingTwo' or currentAccordId=='barcharttimeline' or currentAccordId=='linecharttimeline':
             resultant_data = fun_dynamicGraphTimeline(selectionlist)
         else:
             resultant_data = fun_dynamicGraphSources(selectionlist)

         return HttpResponse(resultant_data)



def fun_dynamicGraph(selectionListArray):

    csv_read = pd.read_csv("E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\csv_of_leads_final.csv")

    var_nonempty_row_array = csv_read.dropna(axis=0, how='any')
    var_filtered_csv = pd.DataFrame()
    var_nonempty_array = var_nonempty_row_array
    selected_countries     = selectionListArray[0]
    selected_technologies  = selectionListArray[1]
    selected_from_month    =  selectionListArray[2]
    selected_to_month      = selectionListArray[3]
    selected_from_year    = selectionListArray[4]
    selected_to_year     = selectionListArray[5]
    var_filtered_csv       = var_nonempty_array.loc[var_nonempty_array['Country'].isin(selected_countries)]
    var_filtered_csv       = var_filtered_csv.loc[var_filtered_csv['Technology'].isin(selected_technologies)]


    fromMonthNumber = pd.to_numeric(selected_from_month)
    toMonthNumber = pd.to_numeric(selected_to_month)
    fromYear = pd.to_numeric(selected_from_year)
    toYear = pd.to_numeric(selected_to_year)

    if selectionListArray[2] != "" and selectionListArray[3] == "":
        var_filtered_csv = var_filtered_csv.loc[(var_filtered_csv['MonthNumber']==selected_from_month) & (var_filtered_csv['Year']==fromYear)]
    if selectionListArray[2] != "" and selectionListArray[3] != "":
        if fromMonthNumber == toMonthNumber and toYear >= fromYear:
            var_filtered_csv = var_filtered_csv.loc[(var_filtered_csv['Year'] >= fromYear) & (var_filtered_csv['Year'] <= toYear)]
        elif (fromMonthNumber>toMonthNumber or fromMonthNumber<toMonthNumber) and toYear > fromYear:

            totalyears =np.subtract(toYear,fromYear)
            totalyears = pd.to_numeric(totalyears)
            counter = totalyears+1
            blankDF = pd.DataFrame()
            temporaryDF = pd.DataFrame()
            for i in range(0, counter):
                if i==0:
                    temporaryDF = var_filtered_csv.loc[(var_filtered_csv['MonthNumber'] >= fromMonthNumber) & (var_filtered_csv['MonthNumber'] <= 12) & (var_filtered_csv['Year'] == fromYear)]
                    blankDF = blankDF.append(temporaryDF)
                if i>0 and i<totalyears:
                    currentYear = fromYear+i
                    temporaryDF = var_filtered_csv.loc[(var_filtered_csv['MonthNumber'] >= 1) & (var_filtered_csv['MonthNumber'] <= 12) & (var_filtered_csv['Year'] == currentYear)]
                    blankDF = blankDF.append(temporaryDF)
                if i==totalyears:
                    temporaryDF = var_filtered_csv.loc[(var_filtered_csv['MonthNumber'] >= 1) & (var_filtered_csv['MonthNumber'] <= toMonthNumber) & (var_filtered_csv['Year'] == toYear)]
                    blankDF = blankDF.append(temporaryDF)
            var_filtered_csv = blankDF


        else:
            var_filtered_csv = var_filtered_csv.loc[(var_filtered_csv['Year'] >= fromYear) & (var_filtered_csv['Year'] <= toYear)]
            var_filtered_csv = var_filtered_csv.loc[(var_filtered_csv['MonthNumber'] >= fromMonthNumber) & (var_filtered_csv['MonthNumber'] <= toMonthNumber)]

    #this is renaming of the column header
    var_filtered_csv = var_filtered_csv.rename({'Lead': 'Lead Count'}, axis='columns')
    #this was renaming of the column header real task begins now
    var_filtered_csv = var_filtered_csv.groupby(['Country', 'Technology', 'Month','Year']).agg({'Lead Count': 'sum'})
    var_dataframe = pd.DataFrame(var_filtered_csv)

    var_pivot_table = var_dataframe.pivot_table(index='Country', columns='Technology', values='Lead Count', aggfunc=np.sum,fill_value=0)        #this is the main code

    value_list = var_pivot_table.sum(axis = 1, skipna = True).tolist()

    if var_filtered_csv.empty == False:
        maxvals = max(value_list)
        if(maxvals < 8):
            maxrange = 8
        else:
            maxrange = maxvals
    else:
        maxrange=0

    max_y_range = maxrange
    #this is just the range of each row
    #from here the coloring of the technology will be done
    v_TechHeaderList = list(var_pivot_table.head(2))
    # TOOLTIPS = [("Technology", "@v_TechHeaderList")]
    source3 = ColumnDataSource(pd.DataFrame(var_pivot_table))
    var_countrylisting = source3.data['Country'].tolist()
    countrycount = len(v_TechHeaderList)
    number_of_colors = countrycount
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]

    colorList = color
    var_dynamic_fig = figure(x_range=var_countrylisting, plot_width=530, plot_height=400,tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'], toolbar_location="below")
    var_dynamic_fig.xaxis.major_label_orientation = pi / 3
    var_dynamic_fig.xaxis.axis_label = 'Countries'
    var_dynamic_fig.yaxis.axis_label = 'No. of Leads'
    var_dynamic_fig.y_range.start = 0
    var_dynamic_fig.y_range.end = max_y_range
    fromMonthName = getMonthName(fromMonthNumber)
    toMonthName = getMonthName(toMonthNumber)
    var_dynamic_fig.title.text = "Leads on the bases of Country and Technology from "+fromMonthName +' '+selected_from_year + " to " + toMonthName+' '+selected_to_year
    stacked = var_dynamic_fig.vbar_stack(stackers=v_TechHeaderList, x='Country', color=colorList, source=source3,
                            legend=[value(x) for x in v_TechHeaderList],
                            name=v_TechHeaderList,
                            width=0.3, )
    for r in stacked:
        tec = r.name
        hover = HoverTool(tooltips=[
            ("%s " % tec, "@%s" % tec),

        ], renderers=[r])
        var_dynamic_fig.add_tools(hover)

    v_figureLayout = column(var_dynamic_fig)
    script2, div2 = components(v_figureLayout)
    var_dataframe.to_csv('media/countrywise_csv.csv')
    var_dataframe = var_dataframe.sort_values(by='Year', ascending=True)

    var_tabular_format = '<div  style="height:350px; width:550px; overflow:auto;" >Number of Leads corresponding to Country and Technology\n'+var_dataframe.to_html()+'</div>'
    # global var_selected_data
    if var_filtered_csv.empty == True:
        var_selected_data = "Sorry No Leads Found"
    else:
        if len(selectionListArray[0])>5:
            var_selected_data = var_tabular_format
        else:
            var_selected_data = script2, div2,var_tabular_format

    return var_selected_data



def fun_dynamicGraphTimeline(selectionListArray):
    csv_read = pd.read_csv("E:\PYTHON\opportunityAnalytics2\media\csv_of_leads_final.csv")
    var_nonempty_row_array = csv_read.dropna(axis=0, how='any')
    var_filtered_csv = pd.DataFrame()
    var_nonempty_array = var_nonempty_row_array
    selected_countries = selectionListArray[0]
    selected_technologies = selectionListArray[1]
    selected_from_month = selectionListArray[2]
    selected_to_month = selectionListArray[3]
    selected_from_year = selectionListArray[4]
    selected_to_year = selectionListArray[5]
    chartChoice2 = selectionListArray[6]
    var_filtered_csv = var_nonempty_array.loc[var_nonempty_array['Country'].isin(selected_countries)]
    var_filtered_csv = var_filtered_csv.loc[var_filtered_csv['Technology'].isin(selected_technologies)]

    fromMonthNumber = pd.to_numeric(selected_from_month)
    toMonthNumber = pd.to_numeric(selected_to_month)
    fromYear = pd.to_numeric(selected_from_year)
    toYear = pd.to_numeric(selected_to_year)

    if selectionListArray[2] != "" and selectionListArray[3] == "":
        var_filtered_csv = var_filtered_csv.loc[
            (var_filtered_csv['MonthNumber'] == selected_from_month) & (var_filtered_csv['Year'] == fromYear)]
    if selectionListArray[2] != "" and selectionListArray[3] != "":
        if fromMonthNumber == toMonthNumber and toYear >= fromYear:
            var_filtered_csv = var_filtered_csv.loc[
                (var_filtered_csv['Year'] >= fromYear) & (var_filtered_csv['Year'] <= toYear)]
        elif (fromMonthNumber > toMonthNumber or fromMonthNumber < toMonthNumber) and toYear > fromYear:

            totalyears = np.subtract(toYear, fromYear)
            totalyears = pd.to_numeric(totalyears)
            counter = totalyears + 1
            blankDF = pd.DataFrame()
            temporaryDF = pd.DataFrame()
            for i in range(0, counter):
                if i == 0:
                    temporaryDF = var_filtered_csv.loc[(var_filtered_csv['MonthNumber'] >= fromMonthNumber) & (
                                var_filtered_csv['MonthNumber'] <= 12) & (var_filtered_csv['Year'] == fromYear)]
                    blankDF = blankDF.append(temporaryDF)
                if i > 0 and i < totalyears:
                    currentYear = fromYear + i
                    temporaryDF = var_filtered_csv.loc[
                        (var_filtered_csv['MonthNumber'] >= 1) & (var_filtered_csv['MonthNumber'] <= 12) & (
                                    var_filtered_csv['Year'] == currentYear)]
                    blankDF = blankDF.append(temporaryDF)
                if i == totalyears:
                    temporaryDF = var_filtered_csv.loc[
                        (var_filtered_csv['MonthNumber'] >= 1) & (var_filtered_csv['MonthNumber'] <= toMonthNumber) & (
                                    var_filtered_csv['Year'] == toYear)]
                    blankDF = blankDF.append(temporaryDF)
            var_filtered_csv = blankDF
        else:
            var_filtered_csv = var_filtered_csv.loc[
                (var_filtered_csv['Year'] >= fromYear) & (var_filtered_csv['Year'] <= toYear)]
            var_filtered_csv = var_filtered_csv.loc[(var_filtered_csv['MonthNumber'] >= fromMonthNumber) & (
                        var_filtered_csv['MonthNumber'] <= toMonthNumber)]


    #from here the if else condition for line and bar chart will be applied
    if chartChoice2 =='barcharttimeline':
        var_filtered_csv = var_filtered_csv.rename({'Lead': 'Lead Count'}, axis='columns')

        ###################################################
        var_filtered_csv = var_filtered_csv.groupby(['Date','Technology', 'Month','Year']).agg({'Lead Count': 'sum'})
        var_dataframe = pd.DataFrame(var_filtered_csv)
        var_pivot_table = var_dataframe.pivot_table(index='Date', columns='Technology', values='Lead Count',
                                                    aggfunc=np.sum, fill_value=0)  # this is the main code
        value_list = var_pivot_table.sum(axis=1, skipna=True).tolist()
        if var_filtered_csv.empty == False:
            maxvals = max(value_list)
            if (maxvals < 8):
                maxrange = 8
            else:
                maxrange = maxvals
        else:
            maxrange = 0

        max_y_range = maxrange
        # this is just the range of each row
        # from here the coloring of the technology will be done
        v_TechHeaderList = list(var_pivot_table.head(2))
        source3 = ColumnDataSource(pd.DataFrame(var_pivot_table))
        var_countrylisting = source3.data['Date'].tolist()
        countrycount = len(v_TechHeaderList)
        number_of_colors = countrycount
        color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
                 for i in range(number_of_colors)]
        colorList = color
        var_dynamic_fig = figure(x_range=var_countrylisting, plot_width=530, plot_height=400,
                                 tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                                 toolbar_location="below")
        var_dynamic_fig.xaxis.major_label_orientation = pi / 3
        var_dynamic_fig.xaxis.axis_label = 'Datewise(month-date-year)'
        var_dynamic_fig.yaxis.axis_label = 'No. of Leads'
        var_dynamic_fig.y_range.start = 0
        var_dynamic_fig.y_range.end = max_y_range
        fromMonthName = getMonthName(fromMonthNumber)
        toMonthName = getMonthName(toMonthNumber)
        var_dynamic_fig.title.text = "Leads on the bases of Country and Technology from " + fromMonthName + ' ' + selected_from_year + " to " + toMonthName + ' ' + selected_to_year
        stacked = var_dynamic_fig.vbar_stack(stackers=v_TechHeaderList, x='Date', color=colorList, source=source3,
                                             legend=[value(x) for x in v_TechHeaderList],
                                             name=v_TechHeaderList,
                                             width=0.3, )
        for r in stacked:
            tec = r.name
            hover = HoverTool(tooltips=[
                ("%s " % tec, "@%s" % tec),
            ], renderers=[r])
            var_dynamic_fig.add_tools(hover)
        v_figureLayout = column(var_dynamic_fig)
        script3, div3 = components(v_figureLayout)
    else:
        var_filtered_csv = var_filtered_csv.rename({'Lead': 'Lead Count'}, axis='columns')
        var_filtered_csv2 = var_filtered_csv.groupby(['Date']).agg({'Lead Count': 'sum'})
        var_dataframe = pd.DataFrame(var_filtered_csv2)
        source2 = ColumnDataSource(pd.DataFrame(var_filtered_csv2))
        Opening = source2.data['Lead Count'].tolist()
        Sources = source2.data['Date'].tolist()
        fromMonthName = getMonthName(fromMonthNumber)
        toMonthName = getMonthName(toMonthNumber)
        title = "Leads on the bases of Dates from " + fromMonthName + ' ' + selected_from_year + " to " + toMonthName + ' ' + selected_to_year
        var_lineGraph = figure(x_range=Sources, plot_width=530, plot_height=400,
                               tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                               toolbar_location="below", title=title)
        var_lineGraph.yaxis.axis_label = 'No. of Leads'
        var_lineGraph.xaxis.axis_label = 'Dates'
        var_lineGraph.xaxis.major_label_orientation = pi / 4
        # add a line renderer
        var_lineGraph.line(Sources, Opening, color='green', line_width=2)
        var_lineGraph.circle(Sources, Opening, color='orange', line_width=2)
        script3, div3 = components(var_lineGraph)
        var_dataframe = var_dataframe.sort_values(by='Year', ascending=True)
    var_tabular_format = '<div  style="height:350px; width:600px; overflow:auto;">Number of Leads by Timeline \n' + var_dataframe.to_html() + '</div>'
    # global var_selected_data
    var_dataframe.to_csv('media/stimelines.csv')
    if var_filtered_csv.empty == True:
        var_selected_data = "Sorry No Leads Found"
    else:
        if len(selectionListArray[0]) > 5:
            var_selected_data = var_tabular_format
        else:
            var_selected_data = script3, div3, var_tabular_format

    # var_selected_data = script3, div3, var_tabular_format
    return var_selected_data

def fun_dynamicGraphSources(selectionListArray):
    csv_read = pd.read_csv("E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\csv_of_leads_final.csv")
    var_nonempty_row_array = csv_read.dropna(axis=0, how='any')
    var_filtered_csv = pd.DataFrame()
    var_nonempty_array = var_nonempty_row_array
    selected_countries = selectionListArray[0]
    selected_technologies = selectionListArray[1]
    selected_from_month = selectionListArray[2]
    selected_to_month = selectionListArray[3]
    selected_from_year = selectionListArray[4]
    selected_to_year = selectionListArray[5]
    chartChoice = selectionListArray[6]

    var_filtered_csv = var_nonempty_array.loc[var_nonempty_array['Country'].isin(selected_countries)]
    var_filtered_csv = var_filtered_csv.loc[var_filtered_csv['Technology'].isin(selected_technologies)]
    fromMonthNumber = pd.to_numeric(selected_from_month)
    toMonthNumber = pd.to_numeric(selected_to_month)
    fromYear = pd.to_numeric(selected_from_year)
    toYear = pd.to_numeric(selected_to_year)
    if selectionListArray[2] != "" and selectionListArray[3] == "":
        var_filtered_csv = var_filtered_csv.loc[
            (var_filtered_csv['MonthNumber'] == selected_from_month) & (var_filtered_csv['Year'] == fromYear)]
    if selectionListArray[2] != "" and selectionListArray[3] != "":
        if fromMonthNumber == toMonthNumber and toYear >= fromYear:
            var_filtered_csv = var_filtered_csv.loc[
                (var_filtered_csv['Year'] >= fromYear) & (var_filtered_csv['Year'] <= toYear)]
        elif (fromMonthNumber > toMonthNumber or fromMonthNumber < toMonthNumber) and toYear > fromYear:
            totalyears = np.subtract(toYear, fromYear)
            totalyears = pd.to_numeric(totalyears)
            counter = totalyears + 1
            blankDF = pd.DataFrame()
            temporaryDF = pd.DataFrame()
            for i in range(0, counter):
                if i == 0:
                    temporaryDF = var_filtered_csv.loc[(var_filtered_csv['MonthNumber'] >= fromMonthNumber) & (
                            var_filtered_csv['MonthNumber'] <= 12) & (var_filtered_csv['Year'] == fromYear)]
                    blankDF = blankDF.append(temporaryDF)
                if i > 0 and i < totalyears:
                    currentYear = fromYear + i
                    temporaryDF = var_filtered_csv.loc[
                        (var_filtered_csv['MonthNumber'] >= 1) & (var_filtered_csv['MonthNumber'] <= 12) & (
                                var_filtered_csv['Year'] == currentYear)]
                    blankDF = blankDF.append(temporaryDF)
                if i == totalyears:
                    temporaryDF = var_filtered_csv.loc[
                        (var_filtered_csv['MonthNumber'] >= 1) & (var_filtered_csv['MonthNumber'] <= toMonthNumber) & (
                                var_filtered_csv['Year'] == toYear)]
                    blankDF = blankDF.append(temporaryDF)
            var_filtered_csv = blankDF
        else:
            var_filtered_csv = var_filtered_csv.loc[
                (var_filtered_csv['Year'] >= fromYear) & (var_filtered_csv['Year'] <= toYear)]
            var_filtered_csv = var_filtered_csv.loc[(var_filtered_csv['MonthNumber'] >= fromMonthNumber) & (
                    var_filtered_csv['MonthNumber'] <= toMonthNumber)]

    if chartChoice == 'barchartsource':
        #here the procedure of bar chart begins
        var_filtered_csv = var_filtered_csv.rename({'Lead': 'Lead Count'}, axis='columns')
        var_filtered_csv = var_filtered_csv.groupby(['Date','Source', 'Technology', 'Month']).agg({'Lead Count': 'sum'})
        # var_filtered_csv.to_csv('media/csv_by_lead_sources.csv')
        var_dataframe = pd.DataFrame(var_filtered_csv)
        var_pivot_table = var_dataframe.pivot_table(index='Technology', columns='Source', values='Lead Count',aggfunc=np.sum, fill_value=0)  # this is the main code
        value_list = var_pivot_table.sum(axis=1, skipna=True).tolist()
        if var_filtered_csv.empty == False:
            maxvals = max(value_list)
            if (maxvals < 8):
                maxrange = 8
            else:
                maxrange = maxvals
        else:
            maxrange = 0

        max_y_range = maxrange
        # this is just the range of each row
        # from here the coloring of the technology will be done
        v_TechHeaderList = list(var_pivot_table.head(2))
        source3 = ColumnDataSource(pd.DataFrame(var_pivot_table))
        var_countrylisting = source3.data['Technology'].tolist()
        countrycount = len(v_TechHeaderList)
        number_of_colors = countrycount
        color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
                 for i in range(number_of_colors)]
        colorList = color
        var_dynamic_fig = figure(x_range=var_countrylisting, plot_width=530, plot_height=400,
                                 tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                                 toolbar_location="below")
        var_dynamic_fig.xaxis.major_label_orientation = pi / 3
        var_dynamic_fig.xaxis.axis_label = 'Technologies'
        var_dynamic_fig.yaxis.axis_label = 'No. of Leads'
        var_dynamic_fig.y_range.start = 0
        var_dynamic_fig.y_range.end = max_y_range
        fromMonthName = getMonthName(fromMonthNumber)
        toMonthName = getMonthName(toMonthNumber)
        var_dynamic_fig.title.text = "Leads on the bases of Country and Technology from " + fromMonthName + ' ' + selected_from_year + " to " + toMonthName + ' ' + selected_to_year
        stacked = var_dynamic_fig.vbar_stack(stackers=v_TechHeaderList, x='Technology', color=colorList, source=source3,
                                             legend=[value(x) for x in v_TechHeaderList],
                                             name=v_TechHeaderList,
                                             width=0.3, )
        for r in stacked:
            tec = r.name
            hover = HoverTool(tooltips=[
                ("%s " % tec, "@%s" % tec),

            ], renderers=[r])
            var_dynamic_fig.add_tools(hover)

        v_figureLayout = column(var_dynamic_fig)
        script4, div4 = components(v_figureLayout)
        var_tabular_format = '<div style="height:350px; width:600px; overflow:auto;" >Number of Leads by Social sources and Technology\n' + var_dataframe.to_html() + '</div>'
        #here the procedure of bar chart ends
    else:
        # here we start the process of a line chart
        var_filtered_csv = var_filtered_csv.rename({'Lead': 'Lead Count'}, axis='columns')
        var_filtered_csv2 = var_filtered_csv.groupby(['Source']).agg({'Lead Count': 'sum'})
        var_dataframe = pd.DataFrame(var_filtered_csv2)
        source2 = ColumnDataSource(pd.DataFrame(var_filtered_csv2))
        Opening = source2.data['Lead Count'].tolist()
        Sources = source2.data['Source'].tolist()
        fromMonthName = getMonthName(fromMonthNumber)
        toMonthName = getMonthName(toMonthNumber)
        title = "Leads on the bases of Sources from "+fromMonthName +' '+selected_from_year + " to " + toMonthName+' '+selected_to_year
        var_lineGraph= figure(x_range=Sources, plot_width=530, plot_height=400,
              tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
              toolbar_location="below",title=title)
        var_lineGraph.yaxis.axis_label = 'No. of Leads'
        var_lineGraph.xaxis.axis_label = 'Sources'
        var_lineGraph.xaxis.major_label_orientation = pi / 4
        # add a line renderer
        var_lineGraph.line(Sources, Opening,color='green', line_width=2)
        var_lineGraph.circle(Sources,Opening,color='orange',line_width=2)

        script4, div4 = components(var_lineGraph)
        var_tabular_format = '<div id="sourcetabular"  style="height:350px; width:600px; overflow:auto;" >Number of Leads by Social sources\n'+var_dataframe.to_html()+'</div>'
        # here the process ends for the line chart
     # Feed them to the Django template.
    var_dataframe.to_csv('media/source_csv.csv')
    if var_filtered_csv.empty == True:
        var_selected_data = "Sorry No Leads Found"
    else:
        if len(selectionListArray[0])>5:
            var_selected_data = var_tabular_format
        else:
            var_selected_data = script4, div4,var_tabular_format

    # var_selected_data = script4,div4,var_tabular_format
    return var_selected_data



def getMonthName(i):
    switcher = {
        1: 'Jan',
        2: 'Feb',
        3: 'Mar',
        4: 'Apr',
        5: 'May',
        6: 'Jun',
        7: 'Jul',
        8: 'Aug',
        9: 'Sep',
        10: 'Oct',
        11: 'Nov',
        12: 'Dec',
        }
    return switcher.get(i, "Invalid day of week")

def fun_joy(request):
    return render(request,'trial.html')



def fun_setPreference(request):
    var_country = request.POST['country']
    var_course = request.POST['course']
    var_fromMonth = request.POST['fromMonth']
    var_toMonth = request.POST['toMonth']
    preference = request.POST['preference']
    # now splitting and creating an array
    countrylst = var_country.split('_')
    techlst = var_course.split('_')
    countrylst = countrylst[:-1]
    techlst = techlst[:-1]


    tree =ET.parse('filename4.xml')
    root = tree.getroot()
    i=0

    for child in root.findall('preference'):
        check_loggedin_person = child.find('loggedinperson').text
        prefer = child.find('prefer').text
        #here we are removing the node
        if child.find('prefer').text == preference:
            root.remove(child)
        # here we have removed the node

        #currently we are assuming that Chandan is the loggedin person
        if check_loggedin_person == 'Chandan':
            i = i+1

    if i<=4:
        var_preference = ET.SubElement(root, "preference")
        ET.SubElement(var_preference, "loggedinperson", name="loggedinperson").text = 'Chandan'
        ET.SubElement(var_preference, "prefer", name="prefer").text = preference
        ET.SubElement(var_preference, "country", name="country_choice").text = var_country
        ET.SubElement(var_preference, "technology", name="technology_choice").text = var_course
        ET.SubElement(var_preference, "months", name="month_range").text = var_fromMonth+'-'+var_toMonth
        tree = ET.ElementTree(root)
        tree.write("filename4.xml")
        msg = "preference saved"
    else:
        msg = "only five preference is allowed"
    return HttpResponse(msg)

def fun_upload_csv(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)

    return render(request,'homepage_old.html')

def getRelatedWords(request):
    if request.method == 'POST':
        char = request.POST['char']
    else:
        char = ""
    df = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\csv_of_leads_final.csv', nrows=1)
    a = df.columns
    columnlisting = df.columns.unique().tolist()

    blank_list = list()
    blank_list.append('<span style="float:right;" ><a href="javascript:;" onclick="hideFreeSearchBox()"><i class="fa fa-times-circle"></i></a></span>')
    lst = list()
    pf = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\salesforce_lead_analytics.csv')
    kj =''
    for j in columnlisting:
        kj = pf[pf[j].astype(str).str.match(char)== True]
        lst = kj[j].unique().tolist()
        for ii in lst:
            blank_list.append("<a href='javascript:;' onClick='selectedWord1(this.text)' >"+ii+"</a>"+"<br>")

    return HttpResponse(blank_list)

def getKeywordChart(request):
    if request.method == 'POST':
        keyword = request.POST['keyword']
    else:
        keyword = ""
    v_csvRead = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\csv_of_leads_final.csv')
    columnName = v_csvRead.columns[v_csvRead.isin([keyword]).any()]
    columnName = columnName.tolist()
    keywords = [keyword]
    var_nonempty_row_array = v_csvRead.dropna(axis=0, how='any')
    var_filtered_csv = pd.DataFrame()

    if columnName[0] == 'Country':
        selectedheader = 'Country'
    elif columnName[0] == 'Location':
        selectedheader = 'Location'
    elif columnName[0] == 'Category':
        selectedheader = 'Category'
    elif columnName[0] == 'Company':
        selectedheader = 'Company'
    elif columnName[0] == 'Source':
        selectedheader = 'Source'
    else:
        selectedheader = 'Month'

    var_filtered_csv = var_nonempty_row_array.loc[var_nonempty_row_array[columnName[0]].isin(keywords)]
    var_filtered_csv = var_filtered_csv.groupby([selectedheader, 'Technology', 'Month']).agg({'Lead': 'sum'})
    var_dataframe = pd.DataFrame(var_filtered_csv)

    var_dataframe.to_csv('media/csv_by_choice.csv')
    v_pivotedTable = var_dataframe.pivot_table(index=selectedheader, columns='Technology', values='Lead', aggfunc=np.sum,fill_value=0)
    var_tabular_format = '<div style="height:200px;  width:500px; overflow:auto; padding-left:200px;" class="col-lg-10 col-md-10 text:center"  ><span style="float:right;" ><a href="javascript:;" onclick="hideSearchDiv()"><i class="fa fa-times-circle"></i></a></span>Number of Leads according to your choice : '+columnName[0]+'\n' + var_dataframe.to_html() + '</div>'

    if selectedheader=='Month':
        var_selected_data = '0'
    else:
        var_selected_data = var_tabular_format
    return HttpResponse(var_selected_data)


def download_csv(request,choice):
    if choice=='Month':
        response = HttpResponse(content_type='text/csv')
        csv_read = pd.read_csv("E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\stimelines.csv")
        response['Content-Disposition'] = 'attachment; filename="Leads_by_Dates.csv"'
        writer = csv.writer(response)
        writer.writerow(['Date', 'Technology', 'Month', 'Year', 'Lead Count'])
        for ind in csv_read.index:
            writer.writerow([csv_read['Date'][ind], csv_read['Technology'][ind], csv_read['Month'][ind], csv_read['Year'][ind],csv_read['Lead Count'][ind]])

    if choice == 'Source':
        response = HttpResponse(content_type='text/csv')
        csv_read = pd.read_csv("E:\\analytyics_leads_opportunity_pdf\\bispenv\Django-CRM-master\media\source_csv.csv")
        response['Content-Disposition'] = 'attachment; filename="Leads_by_Sources.csv"'
        writer = csv.writer(response)
        writer.writerow(['Source', 'Lead Count'])
        for ind in csv_read.index:
            writer.writerow([csv_read['Source'][ind], csv_read['Lead Count'][ind]])
    if choice == 'Country':
        response = HttpResponse(content_type='text/csv')
        csv_read = pd.read_csv("E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\countrywise_csv.csv")
        response['Content-Disposition'] = 'attachment; filename="Leads_by_Country.csv"'
        writer = csv.writer(response)
        writer.writerow(['Country','Technology','Month', 'Lead Count'])
        for ind in csv_read.index:
            writer.writerow([csv_read['Country'][ind],csv_read['Technology'][ind],csv_read['Month'][ind], csv_read['Lead Count'][ind]])
    if choice == 'Choice':
        response = HttpResponse(content_type='text/csv')

        csv_read = pd.read_csv("E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_by_choice.csv")
        columnName = list(csv_read.columns)

        if columnName[0] == 'Country':
            selectedheader = 'Country'
        elif columnName[0] == 'Location':
            selectedheader = 'Location'
        elif columnName[0] == 'Category':
            selectedheader = 'Category'
        elif columnName[0] == 'Company':
            selectedheader = 'Company'
        elif columnName[0] == 'Source':
            selectedheader = 'Source'
        else:
            selectedheader = 'Month'

        response['Content-Disposition'] = 'attachment; filename="'+selectedheader+'_based_leads.csv"'
        writer = csv.writer(response)
        writer.writerow(columnName)
        for ind in csv_read.index:
            writer.writerow([csv_read[selectedheader][ind], csv_read['Technology'][ind],csv_read['Month'][ind],csv_read['Lead'][ind]])

    return response




