import base64
import csv
import datetime
import random
import xml
from flask import Flask, render_template, make_response, send_file, app, Response
from bokeh.core.property.dataspec import value
from bokeh.io import show, output_file
from bokeh.layouts import column,row
from bokeh.transform import dodge, factor_cmap
from django.shortcuts import render,render_to_response
from django.http import HttpResponse, HttpResponseRedirect, request, response
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
import pandas as pd
from bokeh.palettes import Dark2_5 as palette
import itertools
import numpy as np
from bokeh.models import ColumnDataSource, LabelSet, CustomJS, Row, RadioGroup, RadioButtonGroup, OpenURL, TapTool,Button
import matplotlib
from matplotlib.pyplot import figure, legend
from bokeh.models.tools import HoverTool
from bokeh.embed import components
from bokeh.plotting import figure, output_file, show, ColumnDataSource


from bokeh.models import ColumnDataSource

from django.core.files.storage import FileSystemStorage
from flask import jsonify
from datetime import date, timedelta

from io import BytesIO
import xml.etree.ElementTree as ET

from math import pi
from array import *
import cgi
import matplotlib.pyplot as plt
import datetime as dt



def fun_analytics_dashboard_old(request):
    # v_csvRead = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\salesforce_lead_analytics.csv')
    # f = v_csvRead.pivot_table(index='Country', values='Lead', aggfunc=np.sum, fill_value=0)
    # f = f.nlargest(5, 'Lead')
    # f.sort_values('Lead', inplace=True)
    # ax = f.plot.barh(y='Lead')
    # script1, div1 = components(ax)
    # return render(request,'LeadApplication/barpercentage.html')
    return render(request,'LeadApplication/multiline.html')

    # return render(request,'LeadApplication/dashboard2.html',{
    #         'labels': ['F', 'M'],
    #         'data': [52, 2],
    #         'colors': ["#FF4136", "#0074D9"]
    #          },{
    #         'labels2': ['F', 'M'],
    #         'data2': [12, 182],
    #         'colors2': ["#FF4236", "#0084D9"]
    #         }
    #             )

def fun_analytics_dashboard11(request):


    return render(request, 'LeadApplication/dashboard.html')



def fun_analytics_dashboard(request):
    fun_readcsv = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv', usecols = ["Date", "Lead"])
    last_x_days = 20
    todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
    start_date = todaysdate
    fromdate = date.today() - timedelta(last_x_days)
    previous_date = fromdate.strftime("%Y-%m-%d")
    fun_readcsv['Date'] = pd.to_datetime(fun_readcsv['Date'])

    fun_readcsv = pd.pivot_table(fun_readcsv, index=['Date'], values='Lead', aggfunc=np.sum, fill_value=0)
    fun_readcsv = fun_readcsv.reset_index()


    fun_readcsv = fun_readcsv[(fun_readcsv['Date'] > previous_date) & (fun_readcsv['Date'] <= start_date)]

    fun_readcsv['Date'] = fun_readcsv['Date'].dt.strftime('%Y-%m-%d')
    datelist = fun_readcsv['Date'].unique()

    datelist = list(datelist)
    labels = datelist

    data = fun_readcsv['Lead'].tolist()  #fun_readcsv.iloc[0].tolist()

    colorlistlength = len(labels)
    number_of_colors = colorlistlength
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    colorList = color

    #========================Now top 5 courses we have to see
    fun_readcsv2 = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
    f = fun_readcsv2.pivot_table(index='Technology', values='Lead', aggfunc=np.sum, fill_value=0)
    kf = f.nlargest(5, 'Lead')
    # kf= kf.sort_values('Lead', inplace=True)
    kf = kf.reset_index()
    labels1 = kf['Technology'].tolist()
    labels1 = list(labels1)
    data1 = kf['Lead'].tolist()
    colorlistlength1 = len(labels1)
    number_of_colors1 = colorlistlength1
    color1 = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors1)]
    colorList1 = color1
    #============Now multiline chart we need to prepare
    fun_readcsv_multiline = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
    monthseriallist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    currentyear = pd.datetime.now().year
    lastyear = currentyear-1

    currentyeardata = pd.pivot_table(fun_readcsv_multiline.loc[fun_readcsv_multiline['Year'] == currentyear], columns=['Month'], values='Lead', aggfunc=np.sum, fill_value=0)
    # here we get the monthwise
    monthlist = list(currentyeardata.columns)
    unorderedmonthlist= monthlist
    orderedmonthlist = sorted(unorderedmonthlist, key=lambda x: monthseriallist.index(x))
    currentyeardata = currentyeardata [orderedmonthlist]
    currentyearleadtotallist = currentyeardata.iloc[0].tolist()

    monthlists = monthlist
    monthlist = [value for value in monthseriallist if value in monthlists]
    current_monthnotavailable = set(monthseriallist).difference(monthlist)
    for ctr in current_monthnotavailable:
        if (ctr == 'Jan'):
            monthlist.insert(0, 'Jan')
            currentyearleadtotallist.insert(0, 0)
        elif (ctr == 'Feb'):
            monthlist.insert(1, 'Feb')
            currentyearleadtotallist.insert(1, 0)
        elif (ctr == 'Mar'):
            monthlist.insert(2, 'Mar')
            currentyearleadtotallist.insert(2, 0)
        elif (ctr == 'Apr'):
            monthlist.insert(3, 'Apr')
            currentyearleadtotallist.insert(3, 0)
        elif (ctr == 'May'):
            monthlist.insert(4, 'May')
            currentyearleadtotallist.insert(4, 0)
        elif (ctr == 'Jun'):
            monthlist.insert(5, 'Jun')
            currentyearleadtotallist.insert(5, 0)
        elif (ctr == 'Jul'):
            monthlist.insert(6, 'Jul')
            currentyearleadtotallist.insert(6, 0)
        elif (ctr == 'Aug'):
            monthlist.insert(7, 'Aug')
            currentyearleadtotallist.insert(7, 0)
        elif (ctr == 'Sep'):
            monthlist.insert(8, 'Sep')
            currentyearleadtotallist.insert(8, 0)
        elif (ctr == 'Oct'):
            monthlist.insert(9, 'Oct')
            currentyearleadtotallist.insert(9, 0)
        elif (ctr == 'Nov'):
            monthlist.insert(10, 'Nov')
            currentyearleadtotallist.insert(10, 0)
        elif (ctr == 'Dec'):
            monthlist.insert(11, 'Dec')
            currentyearleadtotallist.insert(11, 0)
        else:
            j = 0

    # print(monthlist)
    # print(currentyearleadtotallist)
    ####now for the previouse year also we need to read the csv
    lastyeardata = pd.pivot_table(fun_readcsv_multiline.loc[fun_readcsv_multiline['Year'] == lastyear],columns=['Month'], values='Lead', aggfunc=np.sum, fill_value=0)

    # here we get the monthwise
    monthlist = list(lastyeardata.columns)
    unorderedmonthlist = monthlist
    orderedmonthlist = sorted(unorderedmonthlist, key=lambda x: monthseriallist.index(x))
    lastyeardata = lastyeardata[orderedmonthlist]
    lastyearleadtotallist = lastyeardata.iloc[0].tolist()

    # monthseriallist = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    monthlists = monthlist
    monthlist = [value for value in monthseriallist if value in monthlists]
    current_monthnotavailable = set(monthseriallist).difference(monthlist)
    for ctr in current_monthnotavailable:
        if (ctr == 'Jan'):
            monthlist.insert(0, 'Jan')
            lastyearleadtotallist.insert(0, 0)
        elif (ctr == 'Feb'):
            monthlist.insert(1, 'Feb')
            lastyearleadtotallist.insert(1, 0)
        elif (ctr == 'Mar'):
            monthlist.insert(2, 'Mar')
            lastyearleadtotallist.insert(2, 0)
        elif (ctr == 'Apr'):
            monthlist.insert(3, 'Apr')
            lastyearleadtotallist.insert(3, 0)
        elif (ctr == 'May'):
            monthlist.insert(4, 'May')
            lastyearleadtotallist.insert(4, 0)
        elif (ctr == 'Jun'):
            monthlist.insert(5, 'Jun')
            lastyearleadtotallist.insert(5, 0)
        elif (ctr == 'Jul'):
            monthlist.insert(6, 'Jul')
            lastyearleadtotallist.insert(6, 0)
        elif (ctr == 'Aug'):
            monthlist.insert(7, 'Aug')
            lastyearleadtotallist.insert(7, 0)
        elif (ctr == 'Sep'):
            monthlist.insert(8, 'Sep')
            lastyearleadtotallist.insert(8, 0)
        elif (ctr == 'Oct'):
            monthlist.insert(9, 'Oct')
            lastyearleadtotallist.insert(9, 0)
        elif (ctr == 'Nov'):
            monthlist.insert(10, 'Nov')
            lastyearleadtotallist.insert(10, 0)
        elif (ctr == 'Dec'):
            monthlist.insert(11, 'Dec')
            lastyearleadtotallist.insert(11, 0)
        else:
            j = 0
    #/////////////////now we are calculating the total lead coming from which country

    fun_readcsv2 = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
    filteredcsv = fun_readcsv2.pivot_table(index='Country', values='Lead', aggfunc=np.sum, fill_value=0)
    largestfivecountry = filteredcsv.nlargest(5, 'Lead')

    largestfivecountry = largestfivecountry.reset_index()
    countrylist = largestfivecountry['Country'].tolist()
    countrylist = list(countrylist)
    data2 = largestfivecountry['Lead'].tolist()
    colorlistlength1 = len(countrylist)
    number_of_colors1 = colorlistlength1
    color2 = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
              for i in range(number_of_colors1)]
    colorList2 = color2

    #now technology by sources
    fun_readcsv3 = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
    filteredcsv3 = fun_readcsv3.pivot_table(index='Source', values='Lead', aggfunc=np.sum, fill_value=0)
    flattenedcsv = filteredcsv3.reset_index()
    sourcelist = flattenedcsv['Source'].tolist()
    # countrylist = list(countrylist)
    leadslist4 = largestfivecountry['Lead'].tolist()
    colorlistlength4 = len(sourcelist)
    number_of_colors4 = colorlistlength4
    color4 = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
              for i in range(number_of_colors4)]
    colorList4 = color4



    return render(request,'LeadApplication/fixed_lead_analytics.html',{'labels':labels,'data':data,'colors':colorList,'labels1': labels1,'data1': data1,'colors1':colorList1,'labels2': countrylist,'data2': data2,'colors2':colorList2,'labels4': sourcelist,'data4': leadslist4,'colors4':colorList4,'last_x_days':last_x_days,'previousy':lastyearleadtotallist,'currenty':currentyearleadtotallist,'current_year':currentyear,'last_year':lastyear})


# @csrf_exempt
# def fun_getchartjs(request):
#     trs = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\salesforce_lead_analyticsymd.csv')
#     start_date = 2019-1-9
#     end_date = 2019-1-11
#     trs['Date'] = pd.to_datetime(trs['Date'])
#     trs = trs[(trs['Date'] <= '2019-01-04') & (trs['Date'] > '2019-01-01')]
#     pv = pd.pivot_table(trs, columns=['Date'], values='Lead', aggfunc=np.sum, fill_value=0)
#     labelss = list(pv.head(2))
#     datas = pv.iloc[0].tolist()
#
#     labelss = ','.join(map(str, labelss))
#     datas = ','.join(map(str,datas))
#     labelss = "'Red', 'Blue', 'Yellow'"
#     datas = "12, 19, 30"
#     print(datas)
#     print("KOKO")
#     data = "{labels: ["+labelss+"], datasets: [{label: '# of Votes', data:["+datas+"],backgroundColor: ['rgba(255, 99, 132, 0.2)','rgba(54, 162, 235, 0.2)','rgba(255, 206, 86, 0.2)'],borderColor: ['rgba(255, 99, 132, 1)','rgba(54, 162, 235, 1)',   'rgba(255, 206, 86, 1)'], borderWidth: 1}]};"
#
#     return HttpResponse(data)

@csrf_exempt
def fun_past_reports(request):
    n_days1=1
    if request.method == 'POST':
        n_days1= request.POST['n_days']
    fun_readcsv = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv',usecols=["Date", "Lead"])
    last_x_days = n_days1
    last_x_days = int(last_x_days)
    todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
    start_date = todaysdate
    fromdate = date.today() - timedelta(last_x_days)

    previous_date = fromdate.strftime("%Y-%m-%d")
    fun_readcsv['Date'] = pd.to_datetime(fun_readcsv['Date'])
    fun_readcsv = pd.pivot_table(fun_readcsv, index=['Date'], values='Lead', aggfunc=np.sum, fill_value=0)
    fun_readcsv = fun_readcsv.reset_index()
    fun_readcsv = fun_readcsv[(fun_readcsv['Date'] > previous_date) & (fun_readcsv['Date'] <= start_date)]
    fun_readcsv['Date'] = fun_readcsv['Date'].dt.strftime('%Y-%m-%d')
    datelist = fun_readcsv['Date'].unique()
    datelist = list(datelist)
    labels = datelist  # list(pv.head(2))
    data = fun_readcsv['Lead'].tolist()

    colorlistlength = len(labels)
    number_of_colors = colorlistlength
    color = ["#" + ''.join([random.choice('0123456789ABCDEF') for j in range(6)])
             for i in range(number_of_colors)]
    colorList = color


    data1 = {'labels2':labels,'data2':data,'colors2':colorList,'last_x_days':last_x_days}

    return JsonResponse(data1)

def fun_past_reports_useless(request,*args,**kwargs):
    data = {
        "sales":100,
        "customers":10,
    }
    # return HttpResponse(data)
    return JsonResponse(data)