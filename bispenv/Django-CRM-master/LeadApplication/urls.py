from django.urls import path
from django.conf.urls import include,url
from LeadApplication import views
from LeadApplication import analytics
from LeadApplication import dashboard
app_name = "LeadApplication"

urlpatterns = [
    # path('handler404',views.handler404,name='handler404'),

    path('', views.fun_homePage, name='fun_homePage'),
    path('upload_csv/',views.fun_upload_csv,name='fun_upload_csv'),

    path('lead_analytics/<str:changed_prefrence>', views.fun_leadAnalytics,name='lead_analytics'),


    # path('fun_joy/', views.fun_joy,name='fun_joy'),

    path('download_csv/<str:choice>',views.download_csv,name='download_csv'),


    path('fun_select_parameters/<str:currentAccordId>', views.fun_select_parameters, name='fun_select_parameters'),
    path('fun_setPreference/',views.fun_setPreference,name = 'fun_setPreference'),
    # path('leadanalytics/<str:ch>',views.change_pref,name='change_pref'),
    path('getRelatedWords/',views.getRelatedWords,name='getRelatedWords'),
    path('getKeywordChart/',views.getKeywordChart,name='getKeywordChart'),

    #from here we are going to start the enhanced view
    #for example path('abcd/',enhancedViews.getkeyword,name='getkeyword'),
    #here we will create a new view with the name of enhancedViews

    path('analytics/',analytics.analytics,name='analytics'),
    path('getSelectedIntervalChart/',analytics.getChart,name='getSelectedIntervalChart'),
    path('CountrySpecificLeads/',analytics.getCountrySpecificChart,name='CountrySpecificLeads'),
    path('TechnologySpecificLeads/',analytics.getTechnologySpecificChart,name='TechnologySpecificLeads'),
    path('trial/',analytics.trial,name='trial'),
    path('dashboard/',dashboard.fun_analytics_dashboard,name='analytics_dashboard'),

    # path('fun_getchartjs/', dashboard.fun_getchartjs, name='fun_getchartjs'),
    path('past_reports/',dashboard.fun_past_reports,name='past_reports'),
    # path('api/data/',dashboard.fun_past_reports,name='api-data'),

]