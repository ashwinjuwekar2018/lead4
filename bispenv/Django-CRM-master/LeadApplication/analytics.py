import datetime as dt
import random
import sys
from datetime import date, timedelta
from django.shortcuts import render,render_to_response
from django.http import HttpResponse, HttpResponseRedirect, request, response
import pandas as pd
import numpy as np
import pygal
import matplotlib
import matplotlib.pyplot as plt
# matplotlib.rc('xtick', labelsize=25)
# matplotlib.rc('ytick', labelsize=25)
SMALL_SIZE = 20
matplotlib.rc('font', size=SMALL_SIZE)
matplotlib.rc('axes', titlesize=SMALL_SIZE)
from matplotlib.pyplot import figure, legend
from bokeh.embed import components
from math import pi
import cgi
from bokeh.models import ColumnDataSource
form = cgi.FieldStorage()
from bokeh.plotting import figure, output_file, show



def analytics(request):
    timeInterval = 'this_week'
    if timeInterval == 'this_week':
        todaysdate = dt.datetime.today().strftime("%Y-%m-%d")
        todate = todaysdate
        fromdate = date.today() - timedelta(7)
        fromdate = fromdate.strftime("%Y-%m-%d")

    var_readcsv = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
    var_readcsv = var_readcsv.loc[(var_readcsv['Date']>=fromdate)&(var_readcsv['Date']<todate)]
    var_readcsv = var_readcsv.sort_values(by='Date')
    var_filtered_csv2 = var_readcsv.groupby(['Date']).agg({'Lead': 'sum'})
    var_dataframe = pd.DataFrame(var_filtered_csv2)
    source2 = ColumnDataSource(pd.DataFrame(var_filtered_csv2))
    Opening = source2.data['Lead'].tolist()
    Sources = source2.data['Date'].tolist()
    title = "Data of this week from "+fromdate+" to "+todate
    var_lineGraph = figure(x_range=Sources, plot_width=1000, plot_height=400,
                           tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                           toolbar_location="below", title=title)
    #this below code is for setting the y range value that is integer because otherwise it takes the decimal value that is not acceptable
    max_y_range =var_dataframe['Lead'].max()
    if var_filtered_csv2.empty == False:
        if(max_y_range < 8):
            maxrange = 8
        else:
            maxrange = max_y_range
    else:
        maxrange=0
    #the above code is for setting the y range value
    var_lineGraph.yaxis.axis_label = 'No. of Leads'
    var_lineGraph.xaxis.axis_label = 'Dates'
    var_lineGraph.y_range.start = 0
    var_lineGraph.y_range.end = maxrange
    var_lineGraph.xaxis.major_label_orientation = pi / 4
    # add a line renderer
    var_lineGraph.line(Sources, Opening, color='green', line_width=2)
    var_lineGraph.circle(Sources, Opening, color='orange', line_width=2)
    script5, div5 = components(var_lineGraph)

    #now we are going to create a pie chart from here
    fig = plt.figure()
    ax = fig.add_axes([0, 0, 1, 1])
    ax.axis('equal')
    read_csv = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
    pivoted_csv = read_csv.pivot_table(index='Country', values='Lead', aggfunc=np.sum, fill_value=0)
    pivoted_csv_tech = read_csv.pivot_table(index='Technology',values='Lead',aggfunc=np.sum,fill_value=0)
    flattenedcsv = pivoted_csv.reset_index()
    flattenedcsv_tech = pivoted_csv_tech.reset_index()

    countrylisting = flattenedcsv['Country'].tolist()
    techlisting = flattenedcsv_tech['Technology'].tolist()
    leadlisting = flattenedcsv['Lead'].tolist()
    ax.pie(leadlisting, labels=countrylisting, autopct='%1.2f%%')
    ax.legend(
        title="Ingredients",
        loc="center right",
        bbox_to_anchor=(1, 0, 0.5, 1)
    )
    # ax.set_title("Matplotlib bakery: A pie")
    plt.rcParams['figure.figsize'] = (19,9)
    # plt.show()
    plt.legend(loc='upper right')
    plt.savefig('media/country_based_leads.png')
    return render(request, 'LeadApplication/fixed_analytics.html',{'script5': script5, 'div5': div5,'countries':countrylisting,'technologies':techlisting})


def getChart(request):
    if request.method == 'POST':
        timeInterval = request.POST['timeInterval']
    resultant_data = createDynamicChart(timeInterval)
    return HttpResponse(resultant_data)

def createDynamicChart(timeInterval):
    if timeInterval == 'last_week':
        todaysdate = dt.datetime.today().strftime('%Y-%m-%d')  # date.today() - timedelta(7)
        todate = date.today() - timedelta(7)
        todate = todate.strftime('%Y-%m-%d')
        fromdate = date.today() - timedelta(14)
        fromdate = fromdate.strftime('%Y-%m-%d')
    elif timeInterval == 'this_week':
        todaysdate = dt.datetime.today().strftime('%Y-%m-%d')
        todate = todaysdate
        fromdate = date.today() - timedelta(7)
        fromdate = fromdate.strftime('%Y-%m-%d')
    var_readcsv = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')

    mask = (var_readcsv['Date'] >= fromdate) & (var_readcsv['Date'] <= todate)

    var_readcsv = var_readcsv.loc[mask]
    print(var_readcsv)
    print('Doller bhai')
    var_readcsv = var_readcsv.sort_values(by='Date')
    var_filtered_csv2 = var_readcsv.groupby(['Date']).agg({'Lead': 'sum'})
    var_dataframe = pd.DataFrame(var_filtered_csv2)
    source2 = ColumnDataSource(pd.DataFrame(var_filtered_csv2))
    Opening = source2.data['Lead'].tolist()
    Sources = source2.data['Date'].tolist()
    title = "Data of Last week from "+fromdate+" to "+todate
    var_lineGraph = figure(x_range=Sources, plot_width=1000, plot_height=400,
                           tools=['hover', 'save', 'reset', 'zoom_out', 'zoom_in', 'pan', 'box_zoom'],
                           toolbar_location="below", title=title)
    # this below code is for setting the y range value that is integer because otherwise it takes the decimal value that is not acceptable
    max_y_range = var_dataframe['Lead'].max()
    if var_filtered_csv2.empty == False:
        if (max_y_range < 8):
            maxrange = 8
        else:
            maxrange = max_y_range
    else:
        maxrange = 0
    # the above code is for setting the y range value
    var_lineGraph.yaxis.axis_label = 'No. of Leads'
    var_lineGraph.xaxis.axis_label = 'Dates'
    var_lineGraph.y_range.start = 0
    var_lineGraph.y_range.end = maxrange
    var_lineGraph.xaxis.major_label_orientation = pi / 4
    # add a line renderer
    var_lineGraph.line(Sources, Opening, color='green', line_width=2)
    var_lineGraph.circle(Sources, Opening, color='orange', line_width=2)
    script5, div5 = components(var_lineGraph)
    var_selected_data = script5, div5
    return var_selected_data




def getCountrySpecificChart(request):
    if request.method == 'POST':
        countryname = request.POST['countryname']
    if countryname != '0':
        fig = plt.figure()
        ax = fig.add_axes([0, 0, 1, 1])
        ax.axis('equal')
        csvread = pd.read_csv('E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
        pivoted_csv = pd.pivot_table(csvread.loc[csvread['Country'] == countryname], index='Technology', values='Lead',
                                     aggfunc=np.sum, fill_value=0)

        flattenedcsv = pivoted_csv.reset_index()
        techlisting = flattenedcsv['Technology'].tolist()
        leadlisting = flattenedcsv['Lead'].tolist()
        ax.pie(leadlisting, labels=techlisting, autopct='%1.2f%%')
        ax.legend(
            title="Ingredients",
            loc="center right",
            bbox_to_anchor=(1, 0, 0.5, 1)
        )
        # ax.set_title("Matplotlib bakery: A pie")
        plt.rcParams['figure.figsize'] = (19, 9)
        # plt.show()
        plt.legend(loc='upper right')
        plt.savefig('media/'+countryname+'.png')
        # return HttpResponse(countryname)
    else:
        fig = plt.figure()
        ax = fig.add_axes([0, 0, 1, 1])
        ax.axis('equal')
        read_csv = pd.read_csv(
            'E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
        pivoted_csv = read_csv.pivot_table(index='Country', values='Lead', aggfunc=np.sum, fill_value=0)
        flattenedcsv = pivoted_csv.reset_index()
        countrylisting = flattenedcsv['Country'].tolist()
        leadlisting = flattenedcsv['Lead'].tolist()
        ax.pie(leadlisting, labels=countrylisting, autopct='%1.2f%%')
        ax.legend(
            title="Ingredients",
            loc="center right",
            bbox_to_anchor=(1, 0, 0.5, 1)
        )
        # ax.set_title("Matplotlib bakery: A pie")
        plt.rcParams['figure.figsize'] = (19, 9)
        # plt.show()
        plt.legend(loc='upper right')
        plt.savefig('media/country_based_leads.png')
        countryname='country_based_leads'
    return HttpResponse(countryname)



def getTechnologySpecificChart(request):
    if request.method == 'POST':
        techname = request.POST['techname']
    fig = plt.figure()
    ax = fig.add_axes([0, 0, 1, 1])
    ax.axis('equal')
    csvread = pd.read_csv(
        'E:\\analytyics_leads_opportunity_pdf\\bispenv\\Django-CRM-master\\media\\csv_of_leads_final.csv')
    pivoted_csv = pd.pivot_table(csvread.loc[csvread['Technology'] == techname], index='Country', values='Lead',
                                 aggfunc=np.sum, fill_value=0)
    print(techname)
    print('salman')
    flattenedcsv = pivoted_csv.reset_index()
    countrylisting = flattenedcsv['Country'].tolist()
    leadlisting = flattenedcsv['Lead'].tolist()
    ax.pie(leadlisting, labels=countrylisting, autopct='%1.2f%%')
    ax.legend(
        title="Ingredients",
        loc="center right",
        bbox_to_anchor=(1, 0, 5, 8)
    )
    # ax.set_title("Matplotlib bakery: A pie")

    plt.rcParams['figure.figsize'] = (19, 9)
    # plt.show()
    plt.legend(loc='upper right')
    plt.savefig('media/' + techname + '.png')

    return HttpResponse(techname)


def trial(request):

    return render(request,'LeadApplication/trial2.html')